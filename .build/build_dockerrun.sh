#!/bin/bash
file=".env"
GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
while IFS="=" read key val
do
    # display $line or do somthing with $line
    if [ -n "$val" ]; then
        eval "$key"=$val
    fi
done <"$file"

cat << EOF
{
  "AWSEBDockerrunVersion": 2,
  "containerDefinitions": [
    {
      "essential": true,
      "name": "application",
      "image": "$ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/$REPO/codebase:$GIT_BRANCH",
      "memory": 128
    },
    {
      "essential": true,
      "name": "php-fpm",
      "image": "$ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/$REPO/php-fpm:$GIT_BRANCH",
      "memory": 256,
      "volumesFrom": [
        {
          "sourceContainer": "application"
        }
      ],
      "mountPoints": [
        {
          "containerPath": "/usr/local/var/log",
          "sourceVolume": "awseb-logs-php-fpm"
        }
      ]
    },
    {
      "essential": true,
      "name": "php-worker",
      "image": "$ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/$REPO/php-worker:$GIT_BRANCH",
      "memory": 128,
      "volumesFrom": [
        {
          "sourceContainer": "application"
        }
      ]
    },
    {
      "essential": true,
      "name": "nginx",
      "image": "$ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/$REPO/nginx:$GIT_BRANCH",
      "links": [
        "php-fpm"
      ],
      "memory": 256,
      "volumesFrom": [
        {
          "sourceContainer": "application"
        }
      ],
      "mountPoints": [
        {
          "containerPath": "/var/log/nginx",
          "sourceVolume": "awseb-logs-nginx"
        }
      ],
      "portMappings": [
        {
          "containerPort": 80,
          "hostPort": $NGINX_HOST_HTTP_PORT
        },
        {
          "containerPort": 433,
          "hostPort": $NGINX_HOST_HTTPS_PORT
        }
      ]
    }
  ]
}
EOF


