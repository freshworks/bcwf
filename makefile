#!make
export GIT_LOCAL_BRANCH?=$(shell git rev-parse --abbrev-ref HEAD)
include .env
export $(shell sed 's/=.*//' .env)
export DEPLOY_DATE?=$(shell date '+%Y%m%d%H%M')

define deployTag
"${DOCKERPROJECT}-${GIT_LOCAL_BRANCH}-${DEPLOY_DATE}"
endef

DIR := ${CURDIR}

.PHONY: all prep build push commit clean

.PHONY: all

all:	| dev-env options build push commit clean

local:  | local-env options local-build local-run

jenkins:  | print-status jenkins-env jenkins-build jenkins-run

jenkins-deploy-dev: | jenkins-tag jenkins-push jenkins-deploy-prep jenkins-deploy-version

jenkins-deploy-staging: | jenkins-tag jenkins-push jenkins-deploy-prep jenkins-deploy-version

# ------------------------------------------------------------------------------
# Status Output
# ------------------------------------------------------------------------------

print-status:
	@echo " +---------------------------------------------------------+ "
	@echo " | Current Settings                                        | "
	@echo " +---------------------------------------------------------+ "
	@echo " | BRANCH : $(GIT_LOCAL_BRANCH) "
	@echo " | PROFILE: $(PROFILE) "
	@echo " | ACNT ID: $(ACCOUNT_ID) "
	@echo " | REGION:  $(REGION) "
	@echo " | APPLICATION:$(APPLICATION) "
	@echo " +---------------------------------------------------------+ "
	@echo " | PROJECT: $(DOCKERPROJECT) "
	@echo " | REPO:    $(REPO) "
	@echo " | CODEBASE:$(CODEBASE_TAG) "
	@echo " | NGINX:   $(NGINX_TAG) "
	@echo " | PHP-FPM: $(PHP_FPM_TAG) "
	@echo " | WORKER:  $(PHP_WORKER_TAG) "
	@echo " | POSTGRES:$(POSTGRES_TAG) "
	@echo " +---------------------------------------------------------+ "


# ------------------------------------------------------------------------------
# Account/Container Setup
# ------------------------------------------------------------------------------

create-ecr-repos:
	@echo "+\n++ Creating EC2 Container repositories ...\n+"
	@$(shell aws ecr get-login --no-include-email --profile $(PROFILE) --region $(REGION))
	@aws ecr create-repository --profile $(PROFILE) --region $(REGION) --repository-name $(REPO)/nginx || :
	@aws ecr create-repository --profile $(PROFILE) --region $(REGION) --repository-name $(REPO)/php-fpm || :
	@aws ecr create-repository --profile $(PROFILE) --region $(REGION) --repository-name $(REPO)/php-worker || :
	# @aws ecr create-repository --profile $(PROFILE) --region $(REGION) --repository-name $(REPO)/postgres || :
	@aws ecr create-repository --profile $(PROFILE) --region $(REGION) --repository-name $(REPO)/codebase || :
	@aws iam attach-role-policy --role-name aws-elasticbeanstalk-ec2-role --policy-arn arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly --profile $(PROFILE) --region $(REGION)


# ------------------------------------------------------------------------------
# Local Development
# ------------------------------------------------------------------------------

local-build:
	@echo "+\n++ Performing build of Docker images...\n+"
	@docker-compose build
	# @docker tag $(DOCKERPROJECT)_codebase:$(CODEBASE_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/codebase:$(CODEBASE_TAG)
	# @docker tag $(DOCKERPROJECT)_nginx:$(NGINX_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/nginx:$(NGINX_TAG)
	# @docker tag $(DOCKERPROJECT)_php-fpm:$(PHP_FPM_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/php-fpm:$(PHP_FPM_TAG)
	# @docker tag $(DOCKERPROJECT)_php-worker:$(PHP_WORKER_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/php-worker:$(PHP_WORKER_TAG)
	# @docker tag $(DOCKERPROJECT)_workspace:$(WORKSPACE_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/workspace:$(WORKSPACE_TAG)
	# @docker tag $(DOCKERPROJECT)_postgres:$(POSTGRES_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/postgres:$(POSTGRES_TAG)

local-stop:
	@echo "+\n++ Stopping Local Processes...\n+"
	@docker-compose down

options:
	@echo "+\n++ Generating options.config file from app/.env ...\n+"
	@.build/env_options.sh < app/.env > .ebextensions/options.config

local-env:
	@echo "+\n++ Preparing project for local development ...\n+"
	@cp .config/.env.local app/.env

local-run:
	@echo "+\n++ Running project locally via docker-compose ...\n+"
	@docker-compose up -d
	@docker exec $(DOCKERPROJECT)_workspace /home/laradock/local_update.sh

updates:
	@echo "Running local migrations and composer updates ...."
	@docker-compose exec $(DOCKERPROJECT)_workspace /home/laradock/local_update.sh

workspace:
	@echo "Shelling into local workspace ..."
	@docker-compose exec workspace bash

database:
	@echo "Shelling into local workspace ..."
	@export PGPASSWORD=$(POSTGRES_PASSWORD)
	@docker-compose exec postgres psql -U $(POSTGRES_USER) $(POSTGRES_DB)


# ------------------------------------------------------------------------------
# Jenkins Builds
# ------------------------------------------------------------------------------

jenkins-build:
	@echo "+\n++ Performing build of Docker images for Jenkins Run...\n+"
	@cp app/storage/keys/* app/storage/ || :
	@chmod 644 app/storage/oauth-p* || :
	@docker-compose -f docker-compose.deploy.yml build

jenkins-tag:
	@echo "+\n++ Tagging images for Deployment...\n+"
	@docker tag $(DOCKERPROJECT)_codebase:$(CODEBASE_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/codebase:$(CODEBASE_TAG)
	@docker tag $(DOCKERPROJECT)_nginx:$(NGINX_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/nginx:$(NGINX_TAG)
	@docker tag $(DOCKERPROJECT)_php-fpm:$(PHP_FPM_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/php-fpm:$(PHP_FPM_TAG)
	@docker tag $(DOCKERPROJECT)_php-worker:$(PHP_WORKER_TAG) $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/php-worker:$(PHP_WORKER_TAG)

jenkins-push:
	@echo "+\n++ Pushing images to Dockerhub...\n+"
	@$(shell aws ecr get-login --no-include-email --region $(REGION))
	@docker push $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/codebase:$(CODEBASE_TAG)
	@docker push $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/nginx:$(NGINX_TAG)
	@docker push $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/php-fpm:$(PHP_FPM_TAG)
	@docker push $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/php-worker:$(PHP_WORKER_TAG)
	# @docker push $(ACCOUNT_ID).dkr.ecr.$(REGION).amazonaws.com/$(REPO)/postgres:$(POSTGRES_TAG)

jenkins-env:
	@echo "+\n++ Preparing project for Jenkins Run ...\n+"
	@cp .config/.env.jenkins app/.env

jenkins-stop:
	@echo "+\n++ Stopping Jenkins Processes...\n+"
	@docker-compose -f docker-compose.deploy.yml down

jenkins-run:
	@echo "+\n++ Running project locally via docker-compose without Nginx ...\n+"
	@docker-compose -f docker-compose.deploy.yml down
	@docker-compose -f docker-compose.deploy.yml up -d applications workspace php-fpm postgres php-worker
	@docker exec $(DOCKERPROJECT)_workspace /home/laradock/local_update.sh

jenkins-local:
	@echo "+\n++ Running project locally via docker-compose without Nginx ...\n+"
	@docker-compose up -d applications workspace php-fpm postgres php-worker
	@docker exec $(DOCKERPROJECT)_workspace /home/laradock/local_update.sh

jenkins-clean:
	@echo "+\n++ Cleaning up after Jenkins tests ...\n+"
	@docker-compose down
#	@docker rm -f $(DOCKERPROJECT)_codebase $(DOCKERPROJECT)_workspace $(DOCKERPROJECT)_php-fpm $(DOCKERPROJECT)_postgres $(DOCKERPROJECT)_php-worker

jenkins-tests:
	@echo "+\n++ Building test image and running tests...\n+"
	@docker exec $(DOCKERPROJECT)_workspace /var/www/vendor/bin/phpunit \
		--coverage-clover /var/www/coverage/clover.xml \
		--log-junit /var/www/coverage/tests-report.xml

jenkins-lint:
	@echo "+\n++ Running linting... \n+"
	@docker exec $(DOCKERPROJECT)_workspace /var/www/vendor/bin/phploc /var/www/app --log-xml=complexity-report.xml
	@docker cp $(DOCKERPROJECT)_workspace:/var/www/complexity-report.xml $(PWD)/complexity-report.xml

jenkins-report:
	@echo "+\n++ Retrieving tests report...\n+"
	@docker cp $(DOCKERPROJECT)_workspace:/var/www/coverage/clover.xml $(PWD)/clover.xml
	@docker cp $(DOCKERPROJECT)_workspace:/var/www/coverage/tests-report.xml $(PWD)/tests-report.xml

jenkins-audit:
	@echo "+\n++ Running security audit... \n+"
	@docker-compose -f docker-compose.deploy.yml build audit
	@docker-compose -f docker-compose.deploy.yml up audit
	@docker cp $(DOCKERPROJECT)_audit:/var/www/audit-report.json $(PWD)/audit-report.json
	@docker cp $(DOCKERPROJECT)_audit:/var/www/audit-report.html $(PWD)/audit-report.html

# ------------------------------------------------------------------------------
# AWS Deployment
# ------------------------------------------------------------------------------

jenkins-deploy-prep:
	@echo "+\n++ Preparing for Deploying to AWS...\n+"
	@cp .config/.env.dev app/.env || :
	@cp .config/.env.$(GIT_LOCAL_BRANCH) app/.env || :
	@.build/env_options.sh < app/.env > .ebextensions/options.config
	@.build/build_dockerrun.sh > Dockerrun.aws.json

jenkins-deploy-version:
	@echo "+\n++ Deploying to AWS...\n+"
	@zip -r $(call deployTag).zip .ebextensions Dockerrun.aws.json
	@aws configure set region $(REGION)
	@echo "aws s3 cp $(call deployTag).zip s3://$(BUCKET_NAME)/$(DOCKERPROJECT)/$(call deployTag).zip"
	@aws s3 cp $(call deployTag).zip s3://$(BUCKET_NAME)/$(DOCKERPROJECT)/$(call deployTag).zip
	@aws elasticbeanstalk create-application-version --application-name $(DOCKERPROJECT) --version-label $(call deployTag) --source-bundle S3Bucket="$(BUCKET_NAME)",S3Key="$(DOCKERPROJECT)/$(call deployTag).zip"
	@aws elasticbeanstalk update-environment --application-name $(DOCKERPROJECT) --environment-name $(DOCKERPROJECT)-$(GIT_LOCAL_BRANCH) --version-label $(call deployTag)

healthcheck:
	@aws elasticbeanstalk describe-environments --application-name $(DOCKERPROJECT) --environment-name $(DOCKERPROJECT)-$(GIT_LOCAL_BRANCH) --query 'Environments[*].{Status: Status, Health: Health}'

jenkins-static:
	@echo "+\n++ Deploying Static Assets to AWS-S3 ...\n+"
	@aws configure set region $(REGION)
	@aws s3 sync ./app/public/static/email s3://static-donatelifecan/email  --acl public-read --delete

local-static:
	@echo "+\n++ Deploying Static Assets to AWS-S3 ...\n+"
	@aws configure set region $(REGION)
	@aws s3 sync ./app/public/static/email s3://static-donatelifecan/email  --acl public-read --profile voldemort --delete
