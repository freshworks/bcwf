<!DOCTYPE html>
<html>

@php($companyUrl = config('project.general.url_protocol').'://'.config('project.general.url'))

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Reset Password | Réinitialiser Le Mot De Passe</title>
</head>

<body>
    <div style="background-color: #FFFFFF; padding: 16px;">

        <!--CONTENT START-->

        <div style="font-family: sans-serif; font-size: 16px; font-weight: 300; line-height: 1.5; color: rgba(0,0,0,0.65);">

            <img src="{{config('project.general.url_cdn')}}/email/logo-canada-sm.png" alt="Donate Life Canada Logo" height="50" />

            <!--ENGLISH VERSION START-->

            <div style="margin: 40px 0;">

                <p>Hi,</p>

                <p>Your Donate Life Canada account has been updated by {{$mailData->actor->name}} ({{$mailData->actor->role}}) on {{Carbon\Carbon::now()->format('F d, Y')}}.</p>
                <p>If you have questions or need assistance, please contact the system administrator at <a>xxx@xxx.com</a>.</p>
                <p>Best wishes, <br>The Donate Life Canada Team ❤️</p>

            </div>

            <div style="font-family: sans-serif; font-size: 12px; color: rgba(0,0,0,0.25); padding: 16px 0;">

                <p>Copyright © {{Carbon\Carbon::today()->year}}  Donate Life Canada. All rights reserved.&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div>
                    <a href="{{$companyUrl}}" title="Privacy Policy" style="color: #5DD39E; text-decoration: underline;">Privacy Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Terms of Use" style="color: #5DD39E; text-decoration: underline;">Terms of Use</a>
                </div>

            </div>

            <!--ENGLISH VERSION END-->

            <hr>

            <!--FRENCH VERSION START-->

            <div style="margin: 40px 0;">

                <p>Salut,</p>

                <p>Votre compte 'Donate Life Canada' a été mis à jour par {{$mailData->actor->name}} ({{$mailData->actor->role_fr}}) le {{Carbon\Carbon::now()->format('F d, Y')}}.</p>
                <p>Si vous avez des questions ou avez besoin d’aide, veuillez contacter l’administrateur système à <a>xxx@xxx.com</a>.</p>
                <p>Meilleurs vœux, <br>L'équipe Donate Life Canada ❤️</p>

            </div>

            <div style="font-family: sans-serif; font-size: 12px; color: rgba(0,0,0,0.25); padding: 16px 0;">

                <p>Copyright © {{Carbon\Carbon::today()->year}}  Donate Life Canada. Tous les droits sont réservés.&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div>
                    <a href="{{$companyUrl}}" title="Politique de confidentialité" style="color: #5DD39E; text-decoration: underline;">Politique de confidentialité</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Conditions d'utilisation" style="color: #5DD39E; text-decoration: underline;">Conditions d'utilisation</a>
                </div>

            </div>

            <!--FRENCH VERSION END-->

        </div>

        <!--CONTENT END-->

    </div>

</body>

</html>
