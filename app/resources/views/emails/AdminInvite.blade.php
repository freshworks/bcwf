<!DOCTYPE html>
<html>

@php($companyUrl = config('project.general.url_protocol').'://'.config('project.general.url'))

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Reset Password | Réinitialiser Le Mot De Passe</title>
</head>

<body>
    <div style="background-color: #FFFFFF; padding: 16px;">

        <!--CONTENT START-->

        <div style="font-family: sans-serif; font-size: 16px; font-weight: 300; line-height: 1.5; color: rgba(0,0,0,0.65);">

            <img src="{{config('project.general.url_cdn')}}/email/logo-canada-sm.png" alt="Donate Life Canada Logo" height="50" />

            <!--ENGLISH VERSION START-->

            <div style="margin: 40px 0;">

                <p>Welcome to the Donate Life Canada family!</p>

                <p>{{ $mailData->invite->sender->name }} ({{ $mailData->invite->sender->role }}) has added you as a {{ $mailData->user->role }} to {{ $mailData->user->company_name }}.</p>
                <p>You now have the ability to search, view and save registration decisions. Click below to activate your account.</p>
                <div style="margin: 24px 0;">
                    <a href="{{$mailData->url->activate}}" title="Activate My Account" style="color: #FFFFFF; text-decoration: none; display: inline-block; padding: 8px 24px; background-color: #5DD39E; border-radius: 4px;">Activate My Account</a>
                </div>
                <p>If you have questions or need assistance, please contact the system administrator at <a>xxx@xxx.com</a>.</p>
                <p>Best wishes, <br>The Donate Life Canada Team ❤️</p>

            </div>

            <div style="font-family: sans-serif; font-size: 12px; color: rgba(0,0,0,0.25); padding: 16px 0;">

                <p>Copyright &copy; {{Carbon\Carbon::today()->year}} Donate Life Canada. All rights reserved.&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div>
                    <a href="{{$companyUrl}}" title="Privacy Policy" style="color: #5DD39E; text-decoration: underline;">Privacy Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Terms of Use" style="color: #5DD39E; text-decoration: underline;">Terms of Use</a>
                </div>

            </div>

            <!--ENGLISH VERSION END-->

            <hr>

            <!--FRENCH VERSION START-->

            <div style="margin: 40px 0;">

                <p>Bienvenue dans la famille Donate Life Canada!</p>

                <p>{{ $mailData->invite->sender->name }} ({{$mailData->invite->sender->role_fr }}) vous a ajouté en tant que {{ $mailData->user->role_fr }} de {{ $mailData->user->company_name }}.</p>
                <p>Vous avez maintenant la possibilité de rechercher, consulter et sauvegarder les décisions d'inscription. Cliquez ci-dessous pour activer votre compte.</p>
                <div style="margin: 24px 0;">
                    <a href="{{$mailData->url->activate}}" title="Activer Mon Compte" style="color: #FFFFFF; text-decoration: none; display: inline-block; padding: 8px 24px; background-color: #5DD39E; border-radius: 4px;">Activer Mon Compte</a>
                </div>
                <p>Si vous avez des questions ou avez besoin d’aide, veuillez contacter l’administrateur système à <a>xxx@xxx.com</a>.</p>
                <p>Meilleurs vœux, <br>L'équipe Donate Life Canada ❤️</p>

            </div>

            <div style="font-family: sans-serif; font-size: 12px; color: rgba(0,0,0,0.25); padding: 16px 0;">

                <p>Copyright &copy; {{Carbon\Carbon::today()->year}} Donate Life Canada. Tous les droits sont réservés.&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div>
                    <a href="{{$companyUrl}}" title="Politique de confidentialité" style="color: #5DD39E; text-decoration: underline;">Politique de confidentialité</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Conditions d'utilisation" style="color: #5DD39E; text-decoration: underline;">Conditions d'utilisation</a>
                </div>

            </div>

            <!--FRENCH VERSION END-->

        </div>

        <!--CONTENT END-->

    </div>

</body>

</html>
