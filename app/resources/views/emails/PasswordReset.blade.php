<!DOCTYPE html>
<html>

@php($companyUrl = config('project.general.url_protocol').'://'.config('project.general.url'))

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Reset Password | Réinitialiser Le Mot De Passe</title>
</head>

<body>
    <div style="background-color: #FFFFFF; padding: 16px;">

        <!--CONTENT START-->

        <div style="font-family: sans-serif; font-size: 16px; font-weight: 300; line-height: 1.5; color: rgba(0,0,0,0.65);">

            <img src="{{config('project.general.url_cdn')}}/email/logo-canada-sm.png" alt="Donate Life Canada Logo" height="50" />

            <!--ENGLISH VERSION START-->

            <div style="margin: 40px 0;">

                <p>Hi,</p>

                @if (!is_null($mailData->admin))
                    <p>A requested to reset your password was initiated by {{$mailData->admin->name}} (your {{$mailData->admin->role}}) on {{Carbon\Carbon::now()->format('F d, Y')}}. To reset your password, click the following link and follow the instructions:</p>
                @else
                    <p>You have requested to reset your password on {{Carbon\Carbon::now()->format('F d, Y')}}. To reset your password, click the following link and follow the instructions:</p>
                @endif
                <div style="margin: 24px 0;">
                    <a href="{{$mailData->url->reset}}" title="Reset Password" style="color: #FFFFFF; text-decoration: none; display: inline-block; padding: 8px 24px; background-color: #5DD39E; border-radius: 4px;">Reset Password</a>
                </div>
                <p>Please note, this link will expire in {{ $mailData->reset->expireMinutes }} minutes.</p>
                <p>If you have questions or need assistance, please contact the system administrator at <a>xxx@xxx.com</a>.</p>

                <p>Best wishes, <br>The Donate Life Canada Team ❤️</p>

            </div>

            <div style="font-family: sans-serif; font-size: 12px; color: rgba(0,0,0,0.25); padding: 16px 0;">

                <p>Copyright © {{Carbon\Carbon::today()->year}}  Donate Life Canada. All rights reserved.&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div>
                    <a href="{{$companyUrl}}" title="Privacy Policy" style="color: #5DD39E; text-decoration: underline;">Privacy Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Terms of Use" style="color: #5DD39E; text-decoration: underline;">Terms of Use</a>
                </div>

            </div>

            <!--ENGLISH VERSION END-->

            <hr>

            <!--FRENCH VERSION START-->

            <div style="margin: 40px 0;">

                <p>Salut,</p>

                @if (!is_null($mailData->admin))
                    <p>{{$mailData->admin->name}} (votre {{$mailData->admin->role_fr}}) a demandé à réinitialiser votre mot de passe le {{Carbon\Carbon::now()->format('F d, Y')}}. Pour réinitialiser votre mot de passe, cliquez sur le lien suivant et suivez les instructions:</p>
                @else
                    <p>Vous avez demandé à réinitialiser votre mot de passe le {{Carbon\Carbon::now()->format('F d, Y')}}. Pour réinitialiser votre mot de passe, cliquez sur le lien suivant et suivez les instructions:</p>
                @endif

                <p>Vous avez demandé à réinitialiser votre mot de passe le {{Carbon\Carbon::now()->format('F d, Y')}}. Pour réinitialiser votre mot de passe, cliquez sur le lien suivant et suivez les instructions:</p>

                <div style="margin: 24px 0;">
                    <a href="{{$mailData->url->reset}}" title="Reset Password" style="color: #FFFFFF; text-decoration: none; display: inline-block; padding: 8px 24px; background-color: #5DD39E; border-radius: 4px;">Réinitialiser Le Mot De Passe</a>
                </div>
                <p>Veuillez noter que ce lien expirera dans {{ $mailData->reset->expireMinutes }} minutes.</p>
                <p>Si vous avez des questions ou avez besoin d’aide, veuillez contacter l’administrateur système à <a>xxx@xxx.com</a>.</p>

                <p>Meilleurs vœux, <br>L'équipe Donate Life Canada ❤️</p>

            </div>

            <div style="font-family: sans-serif; font-size: 12px; color: rgba(0,0,0,0.25); padding: 16px 0;">

                <p>Copyright © {{Carbon\Carbon::today()->year}}  Donate Life Canada. Tous les droits sont réservés.&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div>
                    <a href="{{$companyUrl}}" title="Politique de confidentialité" style="color: #5DD39E; text-decoration: underline;">Politique de confidentialité</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Conditions d'utilisation" style="color: #5DD39E; text-decoration: underline;">Conditions d'utilisation</a>
                </div>

            </div>

            <!--FRENCH VERSION END-->

        </div>

        <!--CONTENT END-->

    </div>

</body>

</html>
