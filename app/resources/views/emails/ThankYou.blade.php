<!DOCTYPE html>
<html>

@php($companyUrl = config('project.general.url_protocol').'://'.config('project.general.url'))

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Thank you | Merci</title>
</head>

<body>
    <div style="background-color: #FFFFFF; padding: 16px;">

        <!--PREHEADER START-->

        <div style="background-color: #FFFFFF; color: #FFFFFF; font-size: 5px;">
            Thank you for registering to be an organ donor with Donate Life Canada!
        </div>

        <!--PREHEADER END-->

        <!--CONTENT START-->

        <div style="font-family: sans-serif; font-size: 16px; font-weight: 300; line-height: 1.5; color: rgba(0,0,0,0.65);">

            <img src="{{config('project.general.url_cdn')}}/email/logo-canada-sm.png" alt="Donate Life Canada Logo" height="50" />

            <!--ENGLISH VERSION START-->

            <div style="margin: 40px 0;">

                <p>Welcome to the Donate Life Canada family!</p>

                <p>Thank you so much for registering to be an official organ donor! Your generous contribution has the ability to save up to 8 lives, and benefit more than 75. You're amazing!</p>

                <p>If you want to view / update your donation decision, please click the link below -</p>

                <div style="margin: 24px 0;">
                    <a href="{{$companyUrl}}" title="View My Donation" style="color: #FFFFFF; text-decoration: none; display: inline-block; padding: 8px 24px; background-color: #5DD39E; border-radius: 4px;">View My Donation</a>
                </div>

                <p>If you have questions or need assistance, please visit <a href="{{$companyUrl}}" title="Donate Life Canada" style="color: #5DD39E; text-decoration: underline;">Donate Life Canada</a>.</p>

                <p>Best wishes, <br>The Donate Life Canada Team ❤️</p>

            </div>

            <div style="font-family: sans-serif; font-size: 12px; color: rgba(0,0,0,0.25); padding: 16px 0;">

                <p>Copyright © {{Carbon\Carbon::today()->year}} Donate Life Canada. All rights reserved.&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div>
                    <a href="{{$mailData->url->unsubscribe}}" title="Unsubscribe" style="color: #5DD39E; text-decoration: underline;">Unsubscribe</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Privacy Policy" style="color: #5DD39E; text-decoration: underline;">Privacy Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Terms of Use" style="color: #5DD39E; text-decoration: underline;">Terms of Use</a>
                </div>

            </div>

            <!--ENGLISH VERSION END-->

            <hr>

            <!--FRENCH VERSION START-->

            <div style="margin: 40px 0;">

                <p>Bienvenue dans la famille de Donate Life Canada!</p>

                <p>Merci beaucoup de vous être enregistré pour devenir un donneur d'organes officiel! Votre généreuse contribution a la capacité de sauver jusqu'à 8 vies, et de bénéficier de plus de 75. Vous êtes incroyable!</p>

                <p>Si vous voulez voir / mettre à jour votre décision de don, veuillez cliquer sur le lien ci-dessous -</p>

                <div style="margin: 24px 0;">
                    <a href="{{$companyUrl}}" title="Voir Mon Don" style="color: #FFFFFF; text-decoration: none; display: inline-block; padding: 8px 24px; background-color: #5DD39E; border-radius: 4px;">Voir Mon Don</a>
                </div>

                <p>Si vous avez des questions ou besoin d'aide, s'il vous plaît visitez <a href="{{$companyUrl}}" title="Donate Life Canada" style="color: #5DD39E; text-decoration: underline;">Donate Life Canada</a>.</p>

                <p>Meilleurs vœux, <br>The Donate Life Canada Team ❤️</p>

            </div>

            <div style="font-family: sans-serif; font-size: 12px; color: rgba(0,0,0,0.25); padding: 16px 0;">

                <p>Copyright © {{Carbon\Carbon::today()->year}} Donate Life Canada. Tous les droits sont réservés.&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <div>
                    <a href="{{$mailData->url->unsubscribe}}" title="Se désabonner" style="color: #5DD39E; text-decoration: underline;">Se désabonner</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Politique de confidentialité" style="color: #5DD39E; text-decoration: underline;">Politique de confidentialité</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{$companyUrl}}" title="Conditions d'utilisation" style="color: #5DD39E; text-decoration: underline;">Conditions d'utilisation</a>
                </div>

            </div>

            <!--FRENCH VERSION END-->

        </div>

        <!--CONTENT END-->

    </div>

</body>

</html>
