<?php
/**
 * SystemOption Translations - English
 */

return [

    'so_provinces' => [
        'ab'    => 'Alberta',
        'bc'    => 'British Columbia',
        'mb'    => 'Manitoba', 
        'nb'    => 'New Brunswick',
        'nl'    => 'Newfoundland and Labrador',
        'ns'    => 'Nova Scotia',
        'nt'    => 'Northwest Territories',
        'nu'    => 'Nunavut',
        'on'    => 'Ontario',
        'pe'    => 'Prince Edward Island',
        'qc'    => 'Quebec',
        'sk'    => 'Saskatchewan',
        'yt'    => 'Yukon'
    ],
    'so_user_types' => [
        'reg'   => 'Registrant',
        'adm'   => 'Administrator'
    ],
    'so_organ_types' => [
        'heart'         => "Heart",
        'heart_valves'  => "Heart Valves",
        'liver'         => "Liver",
        'kidneys'       => "Kidneys",
        'lungs'         => "Lungs",
        'pancreas'      => "Pancreas",
        'tissues'       => "Connective Tissues",
        'intestines'    => "Intestines",
        'bone'          => "Bone",
        'blood_vessels' => "Blood Vessels",
        'corneas'       => "Corneas",
        'skin'          => "Skin",
    ],
];  