<?php

/**
 * AccessControlLayer Translations - English
 */

return [
    'system-admin'  => "System Administrator",
    'group-admin'   => "Group Administrator",
    'staff'         => "Staff"
];
