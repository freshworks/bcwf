<?php
/**
 * Exception Translations - English
 */

return [

    'invalid_argument'   => [
        'scope_by_code' => "Failed to get :table by code: :code",
        'scope_by_uuid' => "Failed to get :table by uuid: :uuid"
    ],
    'throttle_request'   => [
        'rate_exceeded' => "Too many consecutive attempts. Try again in :retry"
    ]
];    
    
