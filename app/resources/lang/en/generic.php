<?php

/**
 * Generic Translations - English
 */

return [
    'registered'    => "Registered to be a donor",
    'all_donated'   => "All organs, eyes, and tissues are registered for donation",
    'updated_their' => "Updated their",
    'added'         => "Added",
    'previous'      => "Previous",
];
