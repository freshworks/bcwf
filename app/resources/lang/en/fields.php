<?php

/**
 * Fields Translations - English
 */

return [
    'province_id'         => "province",
    'first_name'          => "first name",
    'last_name'           => "last name",
    'sex'                 => "sex",
    'date_of_birth'       => "date of birth",
    'health_care'         => "health care number",
    'drivers_license'     => "drivers license",
    'military_services'   => "military services number",
    'city'                => "city",
    'address'             => "address",
    'postal_code'         => "postal code",
    'email'               => "email",
    'wishes'              => "wishes",
    'exclude_organ_types' => "excluded organ types",
];
