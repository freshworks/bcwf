<?php
/**
 * Authorization Translations - English
 */

return [

    'invalid_credentials'      => "The user credentials were incorrect.",
    'logged_out'               => "You are logged out.",
    'invalid_authtype'         => "Unauthorized - Invalid Authentication Type.",
    'invalid_appid'            => "Unauthorized - Invalid Application ID.",
    'missing_appid'            => "Unauthorized - Missing Application ID.",
    'invalid_rak'              => "Unauthorized - Invalid Registration Access Key.",
    'missing_rak'              => "Unauthorized - Missing Registration Access Key.",
    'expired_reset_token'      => "The reset token has expired.",
    'invalid_reset_token'      => "The reset token is no longer valid.",
    'expired_activation_token' => "The activation token has expired.",
    'invalid_activation_token' => "The activation token is no longer valid.",
    'no_registrant'            => "Registrant Not Found",
    'no_records'               => "Records Not Found",
    'no_group'                 => "Group Not Found",
    'action_unauthorized'      => "This action is unauthorized"
];
