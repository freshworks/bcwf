<?php

/**
 * Status Translations - English
 */

return [
    'active'   => "active",
    'pending'  => "pending",
    'disabled' => "inactive"
];
