<?php
/**
 * Validation Translation - French
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => "Le :attribute doit être accepté.",
    'active_url'           => "Le :attribute n'est pas une URL valide.",
    'after'                => "Le :attribute doit être une date après la :date.",
    'after_or_equal'       => "Le :attribute doit être une date après ou égale à la date.",
    'alpha'                => "Le :attribute ne peut contenir que des lettres.",
    'alpha_dash'           => "Le :attribute ne peut contenir que des lettres, des chiffres et des tirets.",
    'alpha_num'            => "Le :attribute ne peut contenir que des lettres et des chiffres.",
    'array'                => "Le :attribute doit être un tableau.",
    'before'               => "Le :attribute doit être une date avant la :date.",
    'before_or_equal'      => "Le :attribute doit être une date antérieure ou égale à la :date.",
    'between'              => [
        'numeric' => "Le :attribute doit être entre :min et :max.",
        'file'    => "Le :attribute doit être entre :min et :max kilo-octets.",
        'string'  => "Le :attribute doit être entre les caractères :min et :max.",
        'array'   => "Le :attribute doit avoir entre les éléments :min et :max."
    ],
    'boolean'              => "Le champ :attribute doit être vrai ou faux.",
    'confirmed'            => "La confirmation :attribute ne correspond pas.",
    'date'                 => "Le :attribute n'est pas une date valide.",
    'date_format'          => "Le :attribute ne correspond pas au format :format.",
    'different'            => "Le :attribute et le :other doivent être différents.",
    'digits'               => "Le :attribute doit avoir :digits chiffres.",
    'digits_between'       => "Le :attribute doit avoir entre :min et :max chiffres.",
    'dimensions'           => "Le :attribute a des dimensions d'image incorrectes.",
    'distinct'             => "Le champ :attribute a une valeur en double.",
    'email'                => "Le :attribute doit être une adresse e-mail valide.",
    'exists'               => "Le :attribute sélectionné n'est pas valide.",
    'file'                 => "Le :attribute doit être un fichier.",
    'filled'               => "Le champ :attribute est requis.",
    'image'                => "Le :attribute doit être une image.",
    'in'                   => "Le :attribute sélectionné n'est pas valide.",
    'in_array'             => "Le champ :attribute n'existe pas dans :other.",
    'integer'              => "Le :attribute doit être un nombre entier.",
    'ip'                   => "Le :attribute doit être une adresse IP valide.",
    'json'                 => "Le :attribute doit être une chaîne JSON valide.",
    'max'                  => [
        'numeric' => "Le :attribute ne doit pas être supérieur à :max.",
        'file'    => "Le :attribute ne peut pas être supérieur à :max kilo-octets.",
        'string'  => "Le :attribute ne peut pas être supérieur aux caractères :max.",
        'array'   => "Le :attribute ne doit pas contenir plus d'éléments :max.",
    ],
    'mimes'                => "Le :attribute doit être de type de fichier: :values.",
    'mimetypes'            => "Le :attribute doit être un fichier de type: :values.",
    'min'                  => [
        'numeric' => "Le :attribute doit être au moins :min.",
        'file'    => "Le :attribute doit avoir au moins :min kilo-octets.",
        'string'  => "Le :attribute doit comporter au moins :min caractères.",
        'array'   => "Le :attribute doit avoir au moins des articles :min.",
    ],
    'not_in'               => "Le :attribute sélectionné n'est pas valide.",
    'numeric'              => "Le :attribute doit être un nombre.",
    'present'              => "Le champ :attribute doit être présent.",
    'regex'                => "Le format :attribute est invalide.",
    'required'             => "Le champ :attribute est requis.",
    'required_if'          => "Le champ :attribute est requis lorsque :other est :value.",
    'required_unless'      => "Le champ :attribute est obligatoire sauf si :other est en :values.",
    'required_with'        => "Le champ :attribute est requis lorsque :values est présent.",
    'required_with_all'    => "Le champ :attribute est requis lorsque :values est présent.",
    'required_without'     => "Le champ :attribute est requis lorsque :values n'est pas présent.",
    'required_without_all' => "Le champ :attribute est requis lorsqu'aucune :values n'est présente.",
    'same'                 => "Le :attribute et le :other doivent correspondre.",
    'size'                 => [
        'numeric' => "Le :attribute doit être :size.",
        'file'    => "Le :attribute doit être de :size kilo-octets.",
        'string'  => "Le :attribute doit comporter :size caractères.",
        'array'   => "Le :attribute doit contenir :size éléments.",
    ],
    'string'               => "Le :attribute doit être une chaîne.",
    'timezone'             => "Le :attribute doit être une zone valide.",
    'unique'               => "Le :attribute a déjà été pris.",
    'uploaded'             => "Le :attribute n'a pas pu être téléchargé.",
    'url'                  => "Le format :attribute est invalide.",

     // Password strength
     'password_has_length'       => 'Le mot de passe doit comporter entre 8 et 32 ​​caractères.',
     'password_has_uppercase'    => 'Le mot de passe doit avoir au moins 1 caractère majuscule.',
     'password_has_lowercase'    => 'Le mot de passe doit avoir au moins 1 caractère minuscule.',
     'password_has_number'       => 'Le mot de passe doit avoir au moins 1 numéro.',
     'password_has_special_char' => 'Le mot de passe doit avoir au moins 1 caractère spécial.',
     'password_invalid'          => 'Format de mot de passe invalide.',

     // Enum Value
     'enum_is_invalid'           => 'La valeur que vous avez entrée est invalide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
