<?php
/**
 * Exception Translations - French
 */

return [

    'invalid_argument'   => [
        'scope_by_code' => "Impossible d'obtenir :table par le code: :code",
        'scope_by_uuid' => "Impossible d'obtenir :table par le uuid: :uuid",
    ],
    'throttle_request'   => [
        'rate_exceeded' => "Trop d'essais consécutifs. Réessayez dans :retry"
    ]
];    
    

