<?php

/**
 * Fields Translations - French
 */

return [
    'province_id'         => "province",
    'first_name'          => "prénom",
    'last_name'           => "nom de famille",
    'sex'                 => "sexe",
    'date_of_birth'       => "date de naissance",
    'health_care'         => "numéro de soins de santé",
    'drivers_license'     => "permis de conduire",
    'military_services'   => "numéro de service militaire",
    'city'                => "ville",
    'address'             => "adresse",
    'postal_code'         => "code postal",
    'email'               => "email",
    'wishes'              => "vœux",
    'exclude_organ_types' => "types d'organes exclus",
];
