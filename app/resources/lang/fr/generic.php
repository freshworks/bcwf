<?php

/**
 * Generic Translations - French
 */

return [
    'registered'    => "Enregistré pour être un donateur",
    'all_donated'   => "Tous les organes, yeux et tissus sont enregistrés pour don",
    'updated_their' => "Mis à jour leur",
    'added'         => "Ajoutée",
    'previous'      => "Précédent",
];
