<?php

/**
 * Status Translations - French
 */

return [
    'active'   => "actif",
    'pending'  => "attendre",
    'disabled' => "inactif"
];
