<?php
/**
 * Authorization Translations - Fresh
 */

return [

    'invalid_credentials'      => "Les informations d'identification de l'utilisateur sont incorrectes.",
    'logged_out'               => "Vous êtes déconnecté.",
    'invalid_authtype'         => "Non autorisé - Type d'authentification invalide.",
    'invalid_appid'            => "Non autorisé - Identifiant d'application invalide.",
    'missing_appid'            => "Non autorisé - ID d'application manquant",
    'invalid_rak'              => "Non autorisé - Clé d'accès d'enregistrement invalide",
    'missing_rak'              => "Non autorisé - clé d'accès manquante à l'inscription",
    'expired_reset_token'      => "Le jeton de réinitialisation a expiré.",
    'invalid_reset_token'      => "Le jeton de réinitialisation n'est plus valide.",
    'expired_activation_token' => "Le jeton d'activation a expiré.",
    'invalid_activation_token' => "Le jeton d'activation n'est plus valide.",
    'no_registrant'            => "Inscrit non trouvé",
    'no_records'               => "Enregistrements non trouvés",
    'no_group'                 => "Groupe non trouvés",
    'action_unauthorized'      => "Cette action est non autorisée"
];
