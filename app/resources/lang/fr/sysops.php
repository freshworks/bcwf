<?php
/**
 * SystemOption Translations - French
 */

return [

    'so_provinces' => [
        'ab'    => "L'Alberta",
        'bc'    => "La Colombie-Britannique",
        'mb'    => "Le Manitoba", 
        'nb'    => "Le Nouveau-Brunswick",
        'nl'    => "La Terre-Neuve-et-Labrador",
        'ns'    => "La Nouvelle-Écosse",
        'nt'    => "Les Territoires du Nord-Ouest",
        'nu'    => "Le Nunavut",
        'on'    => "L'Ontario",
        'pe'    => "L'île du Prince-Édouard",
        'qc'    => "Le Québec",
        'sk'    => "La Saskatchewan",
        'yt'    => "Le Yukon",
    ],
    'so_user_types' => [
        'reg'   => "Registrant",
        'adm'   => "Administrateur"
    ],
    'so_organ_types' => [
        'heart'         => "Cœur",
        'heart_valves'  => "Valves Cardiaques",
        'liver'         => "Foie",
        'kidneys'       => "Reins",
        'lungs'         => "Poumons",
        'pancreas'      => "Pancréas",
        'tissues'       => "Tissus Conjonctifs",
        'intestines'    => "Intestins",
        'bone'          => "Os",
        'blood_vessels' => "Vaisseaux Sanguins",
        'corneas'       => "Cornées",
        'skin'          => "Peau",
    ]
];
