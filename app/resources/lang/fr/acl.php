<?php

/**
 * AccessControlLayer Translations - French
 */

return [
    'system-admin'  => "Administrateur du système",
    'group-admin'   => "Administrateur de groupe",
    'staff'         => "Personnel"
];
