<?php

switch(env('APP_ENV')) {
    case "local":
    case "dev":
        $branch = [
            'web' => 'staging.web.',
            'api' => 'dev.api.',
            'admin' => 'dev.admin.',
        ];
        break;
    case "staging":
        $branch = [
            'web' => 'staging.web.',
            'api' => 'staging.api.',
            'admin' => 'staging.admin.'
        ];
        break;
    case "prod":
        $branch = [
            'web' => 'www.',
            'api' => 'api.',
            'admin' => 'admin.',
        ];
        break;
    default:
        $branch = [
            'web' => 'www.',
            'api' => 'api.',
            'admin' => 'admin.'
        ];
        break;
}

return  [

    // General Configuration Options
    'general' => [
        'company_name'        => 'Donate Life Canada',
        'url_protocol'        => env('COMPANY_PROTOCOL', 'http'),
        'url_api'             => $branch['api'].env('COMPANY_URL', 'donatelifecanada.com'),
        'url'                 => $branch['web'].env('COMPANY_URL', 'donatelifecanada.com'),
        'url_admin'           => $branch['admin'].env('COMPANY_URL', 'donatelifecanada.com'),
        'url_unsubscribe'     => '/unsubscribe',
        'url_change_password' => '/change-password',
        'url_cdn'             => env('COMPANY_PROTOCOL', 'https').'://static.voldemort.freshworks.club',
        'url_activate'        => '/activate'
    ],
    'admin' => [
        'invite_expiration' => 10,
    ],
    'paginator' => [
        'page_size'          => 24,
        'record_search_size' => 10,
    ],
    'email' => [
        'info'        => 'info@donatelifecanada.com'
    ],
    'elasticbeanstalk' => [
        'cron' => [
            'interval' => env('EB_CRON_INTERVAL', 5),
            'enable'   => env('USE_CRON', true)
        ],
        'ecr' => [
            'account_id' => env('ECR_ACCOUNT_ID'),
            'region'     => env('ECR_REGION'),
            'repo'       => env('ECR_REPO')
        ],
        'path' => env('APP_ENV') == 'local' ? '/var/www' : '/var/app/current/app'
    ],
    'localization' => [
        'default' => 'en',
        'langs' => [
            'en' => 'english',
            'fr' => 'french'
        ]
    ],
    'headers' => [
        'content-policy' => [
            "header" => "Content-Security-Policy",
            "value"  =>  "default-src 'none', connect-src 'self', 'upgrade-insecure-requests';"
            ],
        'cache-control'  => ["header" => "Cache-Control", "value" => "must-revalidate, no-cache, private"],
#        'cross-domain'   => ["header" => "X-Permitted-Cross-Domain-Policies",  "value" => "none"],
        'content-type'   => ["header" => "X-Content-Type-Options",  "value" => "nosniff"],
        'frame-options'  => ["header" => "X-Frame-Options",  "value" => "DENY"],
        'xss-protection' => ["header" => "X-XSS-Protection", "value" => "1; mode=block"],
        'transport-sec'  => ["header" => "Strict-Transport-Security",  "value" => "max-age=7776000; includeSubDomains"],
        'server'         => [
            "header" => "Server",
            "value" => sprintf('%s (%s)', env("APP_SERVER_NAME", "Donate Life Canada"), env("APP_ENV", "Production"))
            ],
        'powered-by'     => [
            "header" => "X-Powered-By",
            "value" => sprintf('%s (%s)', env("APP_POWERED_NAME", "Freshworks PowerStack"), env("APP_POWERED_VERSION", "4.2"))
            ]
        ]
];
