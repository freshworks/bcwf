<?php

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Hashing\BcryptHasher;
use App\Enums\{RolesEnum};


class DefaultUsersSeeder extends Seeder
{
    protected $groups = [
        ['name' => "St. Brian the Walrus Childrens Hospital"],
        ['name' => "Father Brendan's General Hospital"],
        ['name' => "Jon the Quality Deviner Public Hospital"]
    ];
    protected $devs = [
        ['email' => 'brian+sysadmin1@freshworks.io',   'group_id' => 1, 'role' => RolesEnum::SYSTEM_ADMIN, 'user_name' => 'brian.system-admin'],
        ['email' => 'brian+admin1@freshworks.io',      'group_id' => 2, 'role' => RolesEnum::GROUP_ADMIN,  'user_name' => 'brian.group-admin'],
        ['email' => 'brian+staff1@freshworks.io',      'group_id' => 2, 'role' => RolesEnum::STAFF,        'user_name' => 'brian.staff'],
        ['email' => 'brendan+sysadmin1@freshworks.io', 'group_id' => 1, 'role' => RolesEnum::SYSTEM_ADMIN, 'user_name' => 'brendan.system-admin'],
        ['email' => 'brendan+admin1@freshworks.io',    'group_id' => 3, 'role' => RolesEnum::GROUP_ADMIN,  'user_name' => 'brendan.group-admin'],
        ['email' => 'brendan+staff1@freshworks.io',    'group_id' => 3, 'role' => RolesEnum::STAFF,        'user_name' => 'brendan.staff'],
        ['email' => 'jon+sysadmin1@freshworks.io',     'group_id' => 1, 'role' => RolesEnum::SYSTEM_ADMIN, 'user_name' => 'jon.system-admin'],
        ['email' => 'jon+admin1@freshworks.io',        'group_id' => 4, 'role' => RolesEnum::GROUP_ADMIN,  'user_name' => 'jon.group-admin'],
        ['email' => 'jon+staff1@freshworks.io',        'group_id' => 4, 'role' => RolesEnum::STAFF,        'user_name' => 'jon.staff'],
    ];

    protected $regs = [
        ['first_name' => 'brian',   'last_name' => 'snopek', 'province_id' => 2, 'date_of_birth' => '1978-05-29'],
        ['first_name' => 'brendan', 'last_name' => 'walker', 'province_id' => 2, 'date_of_birth' => '1997-08-01'],
        ['first_name' => 'jon', 'last_name' => 'sharman', 'province_id' => 2, 'date_of_birth' => '1983-12-08'],
    ];

    /**
     * Generate the default users
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->groups AS $i => $group) {
            factory(\App\Models\Group::class)->create($group);
        }

        foreach ($this->devs as $i => $dev) {
            $admin = factory(App\Models\Admin::class)
                ->states(["{$dev['role']}"])
                ->create(['email' => $dev['email'], 'group_id' => $dev['group_id']]);
            $admin->userRecord->update(['user_name' => $dev['user_name'].".{$admin->id}"]);
        }

        foreach ($this->regs as $i => $reg) {

            $reg = factory(App\Models\RegistrantRecord::class)->states(['active'])->create([
                'first_name'    => $reg['first_name'],
                'last_name'     => $reg['last_name'],
                'province_id'   => $reg['province_id'],
                'date_of_birth' => $reg['date_of_birth'],
                "health_care"       => "1234567890ab",
                "drivers_license"   => "1234567890abcd",
                "military_services" => "1234567890ab"
                ]);

            $user = factory(App\Models\User::class)->states(['registrant', 'enabled'])->create([
                'account_id' => $reg->id
            ]);

        }



    }
}
