<?php

namespace App\Providers;

use mmghv\LumenRouteBinding\RouteBindingServiceProvider as BaseServiceProvider;

class RouteBindingServiceProvider extends BaseServiceProvider
{
    /**
     * Boot the service provider
     */
    public function boot()
    {
        // The binder instance
        $binder = $this->binder;

        // Add in the explicit wild-card bindings
//        $binder->bind('user', '\App\Models\User', function ($e) {abort(404, 'User Entity Not Found.');});
    }
}
