<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
//        'App\Events\SendInvitationEvent' => [
//            'App\Listeners\SendInvitationListener',
//        ],
//        'App\Events\PasswordResetEvent' => [
//            'App\Listeners\PasswordResetListener'
//        ]
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
    //     'App\Listeners\AuthEventSubscriber',
    ];

}
