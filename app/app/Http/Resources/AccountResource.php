<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AccountResource extends Resource
{
    /**
     * Transform the resource into an array
     */
    function toArray($request)
    {
        return [
            'id' => $this->id,
            'type_id' => $this->type_id,
        ];
    }
}
