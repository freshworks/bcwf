<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;


class AdminAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request
     */
    function authorize()
    {
        return (Auth::check() && $this->user() && $this->user()->isAdmin());
    }

    /**
     * Get the validation rules that apply to an `updateRegistrantRecord` request
     */
    function rules()
    {
        return [];
    }

}
