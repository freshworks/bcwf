<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * MiddleWare - AuthType Enforcement
 *
 * Adds the ability to restrict routes by User Account Type (registrant, admin)
 *
 * @author Freshworks <info@freshworks.io>
 */
class AuthTypeMiddleware
{

    protected $disableKey = "disable_authtype_enforcement";

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $authType
     * @return mixed
     */
    public function handle($request, Closure $next, $authType = 'any')
    {
        // This functionality can be disabled for the purpose of unit-testing
        if ($this->isDisabled() == true) {
            $response = $next($request);
            return $response;
        }


        switch($authType)
        {
            case "any":
                $valid = (Auth::check() && Auth::user());
                break;
            case "registrant":
                $valid = (Auth::check() && Auth::user()->isRegistrant());
                break;
            case "admin":
                $valid = (Auth::check() && Auth::user()->isAdmin());
                break;
        }

        if(!$valid) {
            return response()->json([
                    'error' => ['invalid_authtype'],
                    'message' => trans('auth.invalid_authtype')
                ], Response::HTTP_UNAUTHORIZED);
        }

        // Disable further validation (For chained requests)
        config(["local.{$this->disableKey}" => true]);

        $response = $next($request);
        return $response;
    }

    /**
     * Checks if we should future checks of this middleware
     *
     * @return boolean
     */
    protected function isDisabled()
    {
        return (config("local.{$this->disableKey}") == true) ? true : false;
    }

}
