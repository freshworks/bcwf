<?php

namespace App\Http\Middleware;

use App\Facades\Localization;
use Closure;

class LocalizationMiddleware
{
    protected $headerKey = "X-localization";
    
    /**
     * Pre-Request middleware to check and set localization options
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $local = ($request->hasHeader($this->headerKey)) ? strtolower($request->header($this->headerKey)) : 'en';
        Localization::setLanguage($local);
        
        $response = $next($request);

        return $response;
    }
}
