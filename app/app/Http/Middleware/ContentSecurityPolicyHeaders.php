<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Description of ContentSecurityPolicyHeaders
 *
 * @author Freshworks <info@freshworks.io>
 */
class ContentSecurityPolicyHeaders
{
    /**
     * Stronger CSP
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $list = \Config::get('project.headers');

        foreach($list AS $item) {
            $response->header($item['header'], $item['value']);
        }
//        $response->header(
//            'Content-Security-Policy',
//            "default-src 'none', connect-src 'self', 'upgrade-insecure-requests';"
//        );
//        $response->header('Cache-Control', 'no-cache, must-revalidate');
//        $response->header('X-Permitted-Cross-Domain-Policies', 'none');
//        $response->header('X-Content-Type-Options', 'nosniff');
//        $response->header('X-Frame-Options', 'DENY');
//        $response->header('X-XSS-Protection', '1; mode=block');
//        $response->header('Strict-Transport-Security', 'max-age=7776000; includeSubDomains');
//
//
//        $name    = env("APP_SERVER_NAME", "Donate Life Canada");
//        $version = env("APP_ENV", "Production");
//        $powered = env("APP_POWERED_NAME", "Freshworks PowerStack");
//        $pVers   = env("APP_POWERED_VERSION", "4.2");
//
//        $response->header('Server', sprintf('%s (%s)', $name, $version));
//        $response->header('X-Powered-By', sprintf('%s (%s)', $powered, $pVers));

        return $response;


    }
}