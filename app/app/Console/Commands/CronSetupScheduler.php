<?php

namespace App\Console\Commands;

class CronSetupScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'freshworks:cron-setup:scheduler {--overwrite : If set the CRON tab for this system will be overwritten}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure this system\'s CRON to use Laravel\'s scheduler.';

    protected $runCommand = 'schedule:run';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Initializing CRON Schedule Setup...');

        $overwrite = $this->option('overwrite');

        if (!$overwrite) {
            $output = $this->shell_exec('crontab -l');
        } else {
            $this->info('Overwriting previous CRON contents...');
            $output = null;
        }

        if ($this->isCronRunning($output, $this->runCommand)) {
            $this->info('Already found Scheduler entry! Not adding.');
        } else {
            $this->file_put_contents('/tmp/crontab.txt', $output . "* * * * * $this->artisan $this->runCommand >> /dev/null 2>&1" . PHP_EOL);
            $this->exec('crontab /tmp/crontab.txt');
        }

        $this->info('Schedule Cron Done!');
    }
    


}
