<?php

namespace App\Console\Commands;

class LeaderSelectionSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'freshworks:leader-selection:setup {--overwrite : If set the CRON tab for this system will be overwritten}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Configure this system's CRON to periodically (every 5 minutes) run leader selection.";


    private $runCommand = 'freshworks:leader-selection:run';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Initializing CRON Leader Setup...');

        $overwrite = $this->option('overwrite');

        if (!$overwrite) {
            $output = $this->shell_exec('crontab -l');
        } else {
            $this->info('Overwriting previous CRON contents...');
            $output = null;
        }
 
        if ($this->isCronRunning($output, $this->runCommand)) {
            $this->info('Already found Leader Selection entry! Not adding.');
        } else {
            $interval = \Config::get('project.elasticbeanstalk.cron.interval');

            $this->file_put_contents(
                '/tmp/crontab.txt',
                $output . "*/$interval * * * * $this->artisan $this->runCommand >> /dev/null 2>&1" . PHP_EOL
            );

            $this->exec('crontab /tmp/crontab.txt');
        }

        $this->info('Leader Selection CRON Done!');
    }
}
