<?php

namespace App\Console\Commands;

use Illuminate\Console\Command as BaseCommand;

class Command extends BaseCommand
{
    /**
     * The artisan command
     *
     * @var string
     */
    protected $artisan;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if (env('APP_ENV', 'local') === 'local') {
            $path = \Config::get('project.elasticbeanstalk.path');
            $this->artisan = "/usr/bin/php $path/artisan";
        } else {
            $this->artisan = "docker exec $(docker ps -f name=php-worker -q) php /var/www/artisan";
        }
    }

    /**
     * Check if cron job is already running
     *
     * @param string $crons
     * @return boolean
     */
    public function isCronRunning($crons, $command)
    {
        return (!is_null($crons) 
            && strpos($crons, $command) !== false);
    }


    /**
     * Wrapper function for shell_exec
     * 
     * @codeCoverageIgnore
     * @param  string $cmd
     * @return mixed
     */
    public function shell_exec($cmd)
    {
        return shell_exec($cmd);
    }

    /**
     * Wrapper function for file_put_contents
     *
     * @codeCoverageIgnore
     * @param  string $file
     * @param  string $contents
     * @return mixed
     */
    public function file_put_contents($file, $contents)
    {
        file_put_contents($file, $contents);
    }

    /**
     * Wrapper function for exec
     *
     * @codeCoverageIgnore
     * @param  string $cmd
     * @return mixed
     */
    public function exec($cmd)
    {
        echo exec($cmd);
    }

}
