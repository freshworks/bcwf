<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Facades\AmazonInstanceHelper;

class LeaderSelectionRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'freshworks:leader-selection:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Initializing Leader Selection...');
        // Only do cron setup if environment is configured to use it (This way we don't accidentally run on workers)
        if ( (boolean)\Config::get('project.elasticbeanstalk.cron.enable') ) {
            //check to see if we are in an instance
            $instanceId = AmazonInstanceHelper::getCurrentInstanceId();

            if ($instanceId) {
                $this->info('Instance ID: ' . $instanceId);

                // Get environment name
                $environmentName = AmazonInstanceHelper::getEnvName($instanceId); 
                $this->info('Environment: ' . $environmentName);
                $this->info('Getting Instances with Environment: ' . $environmentName);

                $leader = AmazonInstanceHelper::isLeaderInstance($this);
                // No leader is running so we'll setup this one as the leader
                // and create a cron entry to run the scheduler
                if ($leader) {
                    $this->info('We are the Leader! Initiating Cron Setup');
                    $this->call('freshworks:cron-setup:scheduler');
                } else {
                    // Instance was found, don't do any cron stuff
                    $this->info('We are not a leader instance :( Maybe next time...');
                }
                $this->info('Leader Selection Done!');
            } else {
                // Probably be run from your local machine
                $this->error('Did not detect an ec2 environment. Exiting.');
            }
        } else {
            $this->info('USE_CRON env var not set. Exiting.');
        }
    }
}
