<?php

namespace App\Enums;

abstract class Enum
{   
    /**
     * Constants cache
     *
     * @var array
     */
    private static $constCacheArray = [];
    
    /**
     * Get all of the constants on the class
     *
     * @return array
     */
    private static function getConstants(): array
    {
        $calledClass = get_called_class();
        
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new \ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        
        return self::$constCacheArray[$calledClass];
    }
    
    /**
     * Get all of the enum keys
     *
     * @return array
     */
    public static function getKeys(): array
    {
        return array_keys(self::getConstants());
    }
    
    /**
     * Get all of the enum values
     *
     * @return array
     */
    public static function getValues(): array
    {
        return array_values(self::getConstants());
    }
    
    /**
     * Get the key for a single enum value
     *
     * @param int|string $value
     * @return int|string
     */
    public static function getKey($value)
    {
        return array_search($value, self::getConstants(), true);
    }
    
    /**
     * Get the value for a single enum key
     *
     * @param string $key
     * @return int|string
     */
    public static function getValue(string $key)
    {
        return self::getConstants()[$key];
    }
    
    /**
     * Return the enum as an array
     *
     * @return array
     */
    public static function toArray(): array
    {
        return self::getConstants();
    }
    
    /**
     * Check that the enum contains a specific key
     *
     * @param string $key
     * @return bool
     */
    public static function hasKey(string $key): bool
    {
        $validKeys = array_map('strtolower', self::getKeys());
        $normalizedKey = strtolower($key);
        
        return in_array($normalizedKey, $validKeys, true);
    }
    
    /**
     * Check that the enum contains a specific value
     *
     * @param int|string $value
     * @param bool $strict (Optional, defaults to True)
     * @return bool
     */
    public static function hasValue($value, bool $strict = true): bool
    {
        $validValues = self::getValues();
        
        if ($strict) {
            return in_array($value, $validValues, true);
        }

        return in_array((string) $value, array_map('strval', $validValues), true);
    }
}
