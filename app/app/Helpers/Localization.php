<?php

namespace App\Helpers;
use App\Exceptions\Util\InvalidArgumentException;


/**
 * Localization Helper
 *
 * @author Freshworks <info@freshworks.io>
 */
class Localization
{
    /**
     * Gets the default language 
     * 
     * @return string
     */
    public function getDefault()
    {
        return \Config::get('project.localization.default');
    }

    /**
     * Gets the available languages
     * 
     * @return array
     */
    public function getAvailable()
    {
        return \Config::get('project.localization.langs');
    }

    /**
     * Validates that the provided langauge is an available language
     * 
     * @param string $lang
     * @return boolean
     */
    public function isValidLanguage($lang)
    {
        $available = $this->getAvailable();
        return array_key_exists(strtolower($lang), $available);
    }

    /**
     * Gets the currently set language
     * 
     * @return string
     */
    public function getLanguage()
    {
        $default = $this->getDefault();
        $current = app('translator')->getLocale();
        return ($this->isValidLanguage($current)) ? $current : $default;
    }
    
    /**
     * Sets the language after validation
     * 
     * @param string $lang
     * @return boolean
     * @throws InvalidArgumentException
     */
    public function setLanguage($lang)
    {
        if( $this->isValidLanguage($lang) ) {
            app('translator')->setLocale($lang);
            return true;
        }
        $available = Localization::getAvailableAsString();
        throw new InvalidArgumentException("Invalid Language Selection. Available Options are: {$available}");
    }
    
    /**
     * Gets the available langauges as a string (For Exceptions)
     * 
     * @return string
     */
    public function getAvailableAsString()
    {
        $available = $this->getAvailable();
        foreach($available AS $key => $name) {
            $ret[] = "'{$key}' ({$name})"; 
        }
        return implode(", ", $ret);
    }
    
    
}
