<?php

namespace App\Helpers;

use Aws\Sdk;
use App\Facades\ArrayUtil;

/**
 * Description of AmazonInstanceHelper
 *
 * @author Freshworks <info@freshworks.io>
 */
class AmazonInstanceHelper
{
    /**
     * @var Aws\ElasticBeanstalk\ElasticBeanstalkClient
     */
    protected $client = null;

    /**
     * Internal DebugMode
     * @var boolean 
     */
    public $debugMode = false;


    protected function createSDK(array $args = [])
    {
       return new Sdk($args); 
    }

    /**
     * Gets (and potentially sets) the ElasticBeanstalk Client
     * 
     * @return Aws\ElasticBeanstalk\ElasticBeanstalkClient
     */
    public function getClient()
    {
        if (is_null($this->client)) {
            $sdk = $this->createSDK([
                'region'  => env('AWS_REGION', 'us-west-2'),
                'version' => env('AWS_VERSION', 'latest'),
                'credentials' => [
                    'key'    => env('AWS_KEY'),
                    'secret' => env('AWS_SECRET'),
                ],
            ]);
            $this->client = $sdk->createEC2();
        }
        return $this->client;
    }
    
    /**
     * Gets the current aws-eb/ec2 instance-id
     * 
     * @return int
     */
    public function getCurrentInstanceId()
    {
       $ch = $this->curl_init('http://169.254.169.254/latest/meta-data/instance-id'); //magic ip from AWS
       $this->curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
       $this->curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       return $this->curl_exec($ch);
    }



    /**
     * Get the environment name for the given instance id
     * 
     * @param integer $id
     * @return string
     */
    public function getEnvName( $id )
    {
        // Get this instance metadata so we can find the environment it's running in
        $tags = $this->getClient()->describeInstances([
            'Filters' => [
                [
                    'Name'   => 'instance-id',
                    'Values' => [$id],
                ],
            ],
        ])->get('Reservations')[0]['Instances'][0]['Tags'];

        $name = array_first($tags, function ($tagArray) {
            return $tagArray['Key'] == 'elasticbeanstalk:environment-name';
        })['Value'];
        
        return $name;
    }
    
    /**
     * Get the instances in an environment by env-name
     * 
     * @param string $name
     * @return array
     */
    public function getEnvInstances( $name )
    {
        // Get instances that have this environment tagged
        $info = $this->getClient()->describeInstances([
            'Filters' => [
                [
                    'Name'   => 'tag-value',
                    'Values' => [$name],
                ],
            ],
        ]);
        $instances = ArrayUtil::map($info->get('Reservations'), function ($i) {
            return $this->current($i['Instances']);
        });


        // Only want instances that are running
        $candidateInstances = ArrayUtil::select($instances, function ($instanceMeta) {
            return $instanceMeta['State']['Code'] == 16;
        });
        
        return $candidateInstances;
    }

    /**
     * Checks if the current execution is on the leader-instance
     * 
     * @return boolean
     */
    public function isLeaderInstance( $command ) 
    {
        if (env('APP_ENV', 'dev') == 'local') {
            return true;
        }

        $currentId  = $this->getCurrentInstanceId();
        $envName    = $this->getEnvName( $currentId );
        $instances  = $this->getEnvInstances( $envName );

        $leader = false;

        if (!empty($instances)) {

            if (count($instances) > 1) {
                // if there is more than one we sort by launch time and get the oldest
                $command->info('More than one instance running, finding the oldest...');
                $oldestInstance = ArrayUtil::sort($instances, function ($left, $right) {
                    return $left['LaunchTime'] > $right['LaunchTime'];
                })[0];
            } else {
                $command->info('Only one instance running...');
                $oldestInstance = reset($instances);
            }

            if ($this->isSameInstance($oldestInstance['InstanceId'], $currentId)) {
                // if this instance is the oldest instance it's the leader
                $leader = true;
            }

        } else {
            $leader = true;
        }        

        return $leader;
        
    }
    
    /**
     * Check if two instances are same
     *
     * @param  mixed $oldInstanceId
     * @param  mixed $currentId
     * @return boolean
     */
    public function isSameInstance($oldInstanceId, $currentId)
    {
        return $oldInstanceId == $currentId;
    }
    
    /**
     * Wrapper function for curl_init
     *
     * @codeCoverageIgnore
     * @param  string $cmd
     * @return mixed
     */
    protected function curl_init($url) {
        return curl_init($url);
    }

    /**
     * Wrapper function for curl_exec
     *
     * @codeCoverageIgnore
     * @param  mixed $curl
     * @return mixed
     */
    protected function curl_exec($curl) {
        return curl_exec($curl);
    }

    /**
     * Wrapper function for curl_setopt
     *
     * @codeCoverageIgnore
     * @param  mixed $curl
     * @param  mixed $key
     * @param  mixed $value
     * @return mixed
     */
    protected function curl_setopt($curl, $key, $value) {
        return curl_setopt($curl, $key, $value);
    }

    /**
     * Wrapper function for current
     * 
     * @codeCoverageIgnore
     * @param  mixed $reservations
     * @return mixed
     */
    public function current($reservations)
    {
        return current($reservations);
    }
}
