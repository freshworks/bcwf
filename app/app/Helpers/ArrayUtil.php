<?php

namespace App\Helpers;

use \Illuminate\Pagination\Paginator;
use \Illuminate\Support\Collection;
use \Illuminate\Pagination\LengthAwarePaginator;
use App\Exceptions\Util\InvalidArgumentException;
use Traversable;

/**
 * Array Utility Helper Class
 *
 * @author Freshworks <info@freshworks.io>
 */
class ArrayUtil
{
    
    /**
     * Produces a new array of elements by mapping each element in collection through a transformation function (callback).
     * Callback arguments will be element, index, collection
     *
     * @param Traversable|array $collection
     * @param callable $callback
     * @return array
     */
    public function map($collection, callable $callback)
    {
        InvalidArgumentException::assertCollection($collection, __FUNCTION__, 1);
        $aggregation = [];
        foreach ($collection as $index => $element) {
            $aggregation[$index] = $callback($element, $index, $collection);
        }
        return $aggregation;
    }    

    /**
     * Looks through each element in the list, returning an array of all the elements that pass a truthy test (callback).
     * Opposite is Functional\reject(). Callback arguments will be element, index, collection
     *
     * @param Traversable|array $collection
     * @param callable $callback
     * @return array
     */
    function select($collection, callable $callback)
    {
        InvalidArgumentException::assertCollection($collection, __FUNCTION__, 1);
        $aggregation = [];
        foreach ($collection as $index => $element) {
            if ($callback($element, $index, $collection)) {
                $aggregation[$index] = $element;
            }
        }
        return $aggregation;
    }    

    /**
     * Sorts a collection with a user-defined function, optionally preserving array keys
     *
     * @param Traversable|array $collection
     * @param callable $callback
     * @param bool $preserveKeys
     * @return array
     */
    function sort($collection, callable $callback, $preserveKeys = false)
    {
        InvalidArgumentException::assertCollection($collection, __FUNCTION__, 1);
        InvalidArgumentException::assertBoolean($preserveKeys, __FUNCTION__, 3);
        if ($collection instanceof Traversable) {
            $array = iterator_to_array($collection);
        } else {
            $array = $collection;
        }
        $fn = $preserveKeys ? 'uasort' : 'usort';
        $fn($array, function ($left, $right) use ($callback, $collection) {
            return $callback($left, $right, $collection);
        });
        return $array;
    }    
    
    /**
     * Recursively checks the difference between two arrays
     *
     * @param array $aArray1
     * @param array $aArray2
     * @return array
     */
    public function recursiveDiff($aArray1, $aArray2) {
        $aReturn = array();
        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($aArray2[$mKey]) && is_array($mValue)) {
                    $aRecursiveDiff = $this->recursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) { 
                        $aReturn[$mKey] = $aRecursiveDiff; 
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }
        return $aReturn;
    }

    /**
     * Paginate a collection
     *
     * @param array|Collection $items
     * @param int $perPage
     * @param int $page
     * @param array $options
     *
     * @return LengthAwarePaginator
     */
    public function paginateCollection($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = ($items instanceof Collection) ? $items : Collection::make($items);
        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'data'  => $lap->values(),
            'links' => [
                'first' => $lap->url(1),
                'last'  => $lap->url($lap->lastPage()),
                'prev'  => $lap->previousPageUrl(),
                'next'  => $lap->nextPageUrl()
            ],
            'meta'  => [
                'current_page' => $lap->currentPage(),
                'from'         => $lap->firstItem(),
                'last_page'    => $lap->lastPage(),
                'path'         => $lap->getPageName(),
                'per_page'     => $lap->perPage(),
                'to'           => $lap->lastItem(),
                'total'        => $lap->total(),
            ]
        ];
    }
    
}
