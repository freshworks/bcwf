<?php

namespace App\Helpers;

use App\Exceptions\ExcessiveRetry;

/**
 * Retry Helper
 *
 * @author Freshworks <info@freshworks.io>
 */
class Retry
{
    /**
     * Calls the passed closure for R retries
     * 
     * @param callable $fn
     * @param int $retries
     * @return mixed
     * @throws ExcessiveRetry
     */
    public function start(callable $fn, $retries = 3)
    {
        try {
            return $fn();
        } catch (\Exception $e) {
            if ($retries > 0) {
                return self::start($fn, $retries-1);
            }
            throw new ExcessiveRetry('', 0, $e);
        }
    }
    
    /**
     * Calls the passed closure for R retries with D delay
     * 
     * @param callable $fn
     * @param int $retries
     * @param int $delay
     * @return mixed
     * @throws ExcessiveRetry
     */
    public function delay(callable $fn, $retries = 3, $delay  = 1)
    {
        try {
            return $fn();
        } catch (\Exception $e) {
            if ($retries > 0) {
                sleep($delay);
                return self::delay($fn, $retries-1, $delay);
            }
            throw new ExcessiveRetry('', 0, $e);
        }
    }

    /**
     * Calls the passed closure for R retries with D delay (Exponential Backoff)
     * 
     * @param callable $fn
     * @param int $retries
     * @param int $delay
     * @return mixed
     * @throws ExcessiveRetry
     */
    public function backoff(callable $fn, $retries = 3, $delay  = 1)
    {
        try {
            return $fn();
        } catch (\Exception $e) {
            if ($retries > 0) {
                if($delay < 1) {
                    $delay = $delay/$delay;
                } else if ($delay == 1) {
                    $delay = 2;
                } else {
                    $delay = $delay*$delay;
                }
                sleep($delay);
                return self::backoff($fn, $retries-1, $delay);
            }
            throw new ExcessiveRetry('', 0, $e);
        }
    }
    
}