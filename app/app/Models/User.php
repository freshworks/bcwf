<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use App\Models\UserType;
use App\Models\PasswordResets;
use Laravel\Passport\HasApiTokens;
use Ramsey\Uuid\Uuid;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Support\Facades\Hash;
use App\Enums\UserStatusEnum;

/**
 * Model - User
 *
 * @author Freshworks <info@freshworks.io>
 */
class User extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    use HasApiTokens;
    use HasRolesAndAbilities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name',
        'email',
        'password',
        'status_id',
        'type_id',
        'account_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Relation for UserTypes
     * @return User
     */
    public function userType()
    {
        return $this->belongsTo(UserType::class, 'type_id');
    }

    /**
     * Relation for PasswordResets
     *
     * @return PasswordResets
     */
    public function passwordResets()
    {
        return $this->hasMany(PasswordResets::class);
    }

    /**
    * Determine if the user is of type 'Registrant'
     *
    * @return boolean
    */
    public function isRegistrant()
    {
        return ($this->type_id == UserType::REGISTRANT_ID);
    }

    /**
    * Determine if the user is of type 'Admin'
     *
    * @return boolean
    */
    public function isAdmin()
    {
        return ($this->type_id == UserType::ADMIN_ID);
    }


    /**
     * Returns an encrypted hash of the user-id and user-name
     *
     * @return string
     */
    public function getEncryptedIdentifier()
    {
        $data = ['id' => $this->id, 'user_name' => $this->user_name];
        return encrypt($data);
    }

    /**
     * Override the default password-check functionality to add additional functionality.
     *
     * @param string $password
     * @return boolean
     */
    public function validateForPassportPasswordGrant($password)
    {
        if(Hash::check($password, $this->getAuthPassword()) && $this->status_id == UserStatusEnum::ACTIVE) {
            event(new \App\Events\LoginEvent($this));
            return true;
        }
        return false;
    }

    /**
     * References which properties should be parsed as DateTime Objects
     *
     * @inheritDoc
     * @return Array
     */
    public function getDates()
    {
        return array_merge(parent::getDates(), array('last_login_at', 'activated_at'));
    }
}
