<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * BaseModel - Custom Base model to be extended by all models
 *
 * @author Freshworks <info@freshworks.io>
 */
class BaseModel extends Model
{

    protected $casts = [
        'meta' => 'object'
    ];

    /**
     * UUID fields should always be upper case
     *
     * @param  string  $value
     * @return string
     */
    public function getUuidAttribute($value)
    {
        return strtoupper($value);
    }

    /**
     * UUID fields should always be upper case
     *
     * @param string $value
     */
    public function setUuidAttribute($value)
    {
        $this->attributes['uuid'] = strtoupper($value);
    }
}
