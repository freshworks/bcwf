<?php

namespace App\Events\Handlers;

use App\Events\LoginEvent;
use Carbon\Carbon;

/**
 * Login EventHandler
 *
 * @author Freshworks <info@freshworks.io>
 */
class LoginEventHandler
{
    /**
     * Handles the Login Event
     *
     * @param LoginEvent $event
     */
    public function handle(LoginEvent $event)
    {
        $event->user->last_login_at = Carbon::now();
        $event->user->save();

        return __METHOD__;
    }
}
