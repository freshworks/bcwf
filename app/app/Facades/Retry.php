<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Retry Facade Wrapper
 *
 * @author Freshworks <info@freshworks.io>
 */
class Retry extends Facade
{
    protected static function getFacadeAccessor() 
    { 
        return 'Retry'; 
    }
}
