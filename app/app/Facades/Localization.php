<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Localization Facade Wrapper
 *
 * @author Freshworks <info@freshworks.io>
 */
class Localization extends Facade
{
    protected static function getFacadeAccessor() 
    { 
        return 'Localization'; 
    }
}
