<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * ArrayUtil Facade Wrapper
 *
 * @author Freshworks <info@freshworks.io>
 */
class ArrayUtil extends Facade
{
    protected static function getFacadeAccessor() 
    { 
        return 'ArrayUtil'; 
    }
}
