<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;


class AdminEventSubscriber implements ShouldQueue
{
    /**
     * Register the listeners for the subscriber
     *
     * @param Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\AdminInviteEvent',
            'App\Events\Handlers\AdminInviteEventHandler@handle'
        );
        $events->listen(
            'App\Events\AdminAccountDisabledEvent',
            'App\Events\Handlers\AdminAccountDisabledEventHandler@handle'
        );
        $events->listen(
            'App\Events\AdminUpdateEvent',
            'App\Events\Handlers\AdminUpdateEventHandler@handle'
        );
     }

}
