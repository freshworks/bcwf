<?php


namespace App\Exceptions;

/**
 * Localization Helper
 *
 * @author Freshworks <info@freshworks.io>
 */
class ExcessiveRetry extends \Exception {
}
