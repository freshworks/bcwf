<?php

require_once __DIR__.'/../vendor/autoload.php';
#require_once __DIR__.'/../app/Helpers/helpers.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades(true, [
    'Illuminate\Support\Facades\Mail'   => 'Mail',
    'Illuminate\Support\Facades\Config' => 'Config',
    'Illuminate\Support\Facades\Queue'  => 'Queue',
    'Illuminate\Support\Facades\File'   => 'File',
    'Illuminate\Support\Arr'            => 'Arr',
    'Illuminate\Support\Carbon'         => 'Carbon',
    'Illuminate\Support\MessageBag'     => 'MessageBag',
    'Illuminate\Support\Pluralizer'     => 'Pluralizer',
    'Illuminate\Support\Str'            => 'Str',
    'App\Facades\ArrayUtil'             => 'ArrayUtil',
    'App\Facades\AmazonInstanceHelper'  => 'AmazonInstanceHelper',
    'App\Facades\Localization'          => 'Localization',
    'App\Facades\Retry'                 => 'Retry',
    '\Ramsey\Uuid\Uuid'                 => 'Uuid',
    '\Silber\Bouncer\BouncerFacade'     => 'Bouncer',
]);

$app->withEloquent();

$app->configure('auth');
$app->configure('project');
$app->configure('mail');

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton('ArrayUtil', function () {
    return new \App\Helpers\ArrayUtil();
});

$app->singleton('AmazonInstanceHelper', function () {
    return new \App\Helpers\AmazonInstanceHelper();
});

$app->singleton('AccountManager', function () {
    return new \App\Helpers\AccountManager();
});

$app->singleton('AccessControlManager', function () {
    return new \App\Helpers\AccessControlManager();
});

$app->singleton('Localization', function () {
    return new \App\Helpers\Localization();
});

$app->singleton('Retry', function () {
    return new \App\Helpers\Retry();
});


$app->singleton(\Barryvdh\DomPDF\ServiceProvider::class);


/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    App\Http\Middleware\CorsMiddleware::class,
    App\Http\Middleware\LocalizationMiddleware::class,
]);

$app->routeMiddleware([
    'auth' => App\Http\Middleware\Authenticate::class,
    'authtype' => App\Http\Middleware\AuthTypeMiddleware::class,
    'throttle' => App\Http\Middleware\ThrottleRequests::class,
    'clientid' => App\Http\Middleware\ClientIdEnforcement::class,
    'regkey' => App\Http\Middleware\RegistrationAccessKeyMiddleware::class,
    'content-security' => App\Http\Middleware\ContentSecurityPolicyHeaders::class
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(Silber\Bouncer\BouncerServiceProvider::class);
$app->register(App\Providers\EventServiceProvider::class);
$app->register(App\Providers\FormRequestServiceProvider::class);
$app->register(Illuminate\Mail\MailServiceProvider::class);
$app->register(Flipbox\LumenGenerator\LumenGeneratorServiceProvider::class);
$app->register(Laravel\Passport\PassportServiceProvider::class);
$app->register(Dusterio\LumenPassport\PassportServiceProvider::class);
$app->register(Barryvdh\DomPDF\ServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/api.php';
});

return $app;
