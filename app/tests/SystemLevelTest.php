<?php


class SystemLevelTest extends TestCase
{
    
    
    
    
    /**
     * Test system wide configuration before any other test is run
     * 
     * @group System
     */
    public function testSystemConfiguration()
    {
        $publicKey = __DIR__ . '/../storage/oauth-public.key';
        $privateKey = __DIR__ . '/../storage/oauth-private.key';

        $this->assertTrue(file_exists($publicKey));
        $this->assertTrue(file_exists($privateKey));

        $this->assertFileIsReadable($publicKey);
        $this->assertFileIsReadable($privateKey);
        
        $this->assertNotNull(env('APP_NAME', null));
        $this->assertNotNull(env('APP_ENV', null));
        $this->assertNotNull(env('APP_KEY', null));
        $this->assertNotNull(env('APP_URL', null));
        $this->assertNotNull(env('DB_CONNECTION', null));
        $this->assertNotNull(env('DB_HOST', null));
        $this->assertNotNull(env('DB_PORT', null));
        $this->assertNotNull(env('DB_DATABASE', null));
        $this->assertNotNull(env('DB_USERNAME', null));
        $this->assertNotNull(env('DB_PASSWORD', null));
        $this->assertNotNull(env('CLIENT_ID', null));
        $this->assertNotNull(env('CLIENT_SECRET', null));

        $this->assertNotNull($this->app->db->getPdo());
    }
    
    /**
     * Test that the oauth-keys are present
     * 
     * @group System
     * @group oAuth
     */
    public function testEncryptionKeysPresent()
    {
        $list = [
            'oauth-private.key' => '0644',
            'oauth-public.key' =>  '0644'
        ];

        foreach($list AS $fname => $fperm) {
            
            $path   = storage_path()."/{$fname}";
            $exists = File::exists($path);
            $perms  = ($exists) 
                    ? (File::chmod($path) == $fperm) 
                    : false;

            $this->assertTrue($exists, "Failed to assert that {$path} exists");
            $this->assertTrue($perms,  "Failed to assert that {$path} as permissions of {$fperm}");
            
        }
        
    }
    
    
}
