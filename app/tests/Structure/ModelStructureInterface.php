<?php

namespace Tests\Structure;

/**
 * ModelStructureInterface
 *
 * @author Freshworks <info@freshworks.io>
 */
interface ModelStructureInterface
{
    public function fetch($attr = []);
}
