<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * GroupReport Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class GroupReport implements ModelStructureInterface
{

    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'name',
            'province_id',
            'address',
            'website',
            'phone',
            'admin_count',
            'staff_count',
            'active_count',
            'inactive_count',
            'pending_count',
        ], $attr);
    }
}
