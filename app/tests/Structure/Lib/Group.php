<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Group Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Group implements ModelStructureInterface
{

    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'name',
            'province_id',
            'address',
            'website',
            'phone',
            'meta',
            'created_at',
            'updated_at',
        ], $attr);
    }
}
