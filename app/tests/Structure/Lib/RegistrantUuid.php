<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Registrant History Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class RegistrantUuid implements ModelStructureInterface
{

    public function fetch($attr = [])
    {
        return array_merge([
            'registrant_uuid',
        ], $attr);
    }
}
