<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Account Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Account implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'type_id',
        ], $attr);
    }
}
