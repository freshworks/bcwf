<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Pager Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Pager implements ModelStructureInterface
{
    
    public function fetch($attr = null)
    {
        return [
            'links' => [
                    'first',
                    'last',
                    'prev',
                    'next'
                ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total'
            ]
        ];
    }
}
