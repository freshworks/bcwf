<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * PartnerLoginAccessKey Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class PartnerLoginAccessKey implements ModelStructureInterface
{

    public function fetch($attr = [])
    {
        return array_merge([
            'access_key',
            'access_url'
        ], $attr);
    }
}
