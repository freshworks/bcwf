<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Registrant History Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class RegistrantHistory implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'created_at',
            'message',
            'difference',
        ], $attr);
    }
}
