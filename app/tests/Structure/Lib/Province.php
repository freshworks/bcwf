<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Province Model Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Province implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'code',
            'name',
        ], $attr);
    }
}
