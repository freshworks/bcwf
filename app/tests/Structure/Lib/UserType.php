<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * UserType Model Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class UserType implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'code',
            'name',
            'meta',
        ], $attr);
    }
}
