<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Admin Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Admin implements ModelStructureInterface
{

    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'first_name',
            'last_name',
            'email',
            'status',
            'group',
            'meta',
            'role',
            'abilities',
            'created_at',
            'updated_at',
            'activated_at',
            'last_login_at'
        ], $attr);
    }
}
