<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * OrganType Model Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class OrganType implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'name',
        ], $attr);
    }
}
