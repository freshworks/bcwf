<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * ClientType Model Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class ClientType implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'name',
        ], $attr);
    }
}
