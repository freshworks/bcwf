<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * User Model Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class User implements ModelStructureInterface
{
    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'user_name',
            'email',
            'status_id',
            'type_id',
            'account_id',
            'created_at',
            'updated_at',
        ], $attr);
    }
}
