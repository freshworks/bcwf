<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Registrant Model Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Registrant implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'client_type_id',
            'created_at',
            'updated_at',
        ], $attr);
    }
}
