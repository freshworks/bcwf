<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * RegistrationAccessKey Model Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class RegistrationAccessKey implements ModelStructureInterface
{

    public function fetch($attr = [])
    {
        return array_merge([
            'key',
            'status_id',
            'created_at',
            'expires_at'
        ], $attr);
    }
}
