<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Token Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Token implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'token_type',
            'expires_in',
            'access_token',
            'refresh_token'
        ], $attr);
    }
}
