<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Role Resource Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Role implements ModelStructureInterface
{

    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'name',
            'title'
        ], $attr);
    }
}
