<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * ReportOverview Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class RangeStatistics implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'monthly_average',
            'highest_month',
            'lowest_month',
            'range_total',
            'grand_total',
        ], $attr);
    }
}
