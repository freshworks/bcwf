<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * RegistrantRecord Model Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class RegistrantRecord implements ModelStructureInterface
{

    public function fetch($attr = [])
    {
        return array_merge([
            'id',
            'registrant_id',
            'province_id',
            'first_name',
            'last_name',
            'sex',
            'date_of_birth',
            'health_care',
            'drivers_license',
            'military_services',
            'city',
            'address',
            'postal_code',
            'email',
            'wishes',
            'exclude_organ_types',
            'created_at',
            'updated_at',
        ], $attr);
    }
}
