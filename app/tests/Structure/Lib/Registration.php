<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * Registration Response Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class Registration implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'token_type',
            'expires_in',
            'access_token',
            'refresh_token',
            'account_url',
        ], $attr);
    }
}
