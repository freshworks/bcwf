<?php

namespace Tests\Structure\Lib;

use Tests\Structure\ModelStructureInterface;

/**
 * ReportOverview Structure
 *
 * @author Freshworks <info@freshworks.io>
 */
class ReportOverview implements ModelStructureInterface
{
   
    public function fetch($attr = [])
    {
        return array_merge([
            'last_15_days',
            'last_30_days',
            'monthly_average',
            'year_total',
            'grand_total',
        ], $attr);
    }
}
