<?php

namespace Tests\Structure;


/**
 * StructureFactory
 * 
 * Generates an array-mock of the ResourceResponses as generated in the app.
 * Generated Structure classes are 'tightly-coupled' to ensure that changes made 
 * to the actual resources are explicitly accounted for in the unit/feature tests.
 *
 * @author Freshworks <info@freshworks.io>
 */
class StructureFactory
{
    const MODEL_PATH = __NAMESPACE__."\Lib\\";
    const MODEL_PAGER = "Pager";

    /**
     * Returns the structure for a single-record
     * 
     * @param string $model
     * @param array $attr
     * @return array
     */
    public static function resource($model, $attr = [])
    {
        return [ "data" => self::make($model, $attr) ];
    }
    
    /**
     * Returns the structure for a collection
     * 
     * @param string $model
     * @param array $attr
     * @return array
     */
    public static function collection($model, $attr = [])
    {
        return [ "data" => ["*" => self::make($model, $attr) ]];
    }

    /**
     * Returns the structure of a nested collection
     * 
     * @param string $model
     * @param array $attr
     * @return array
     */
    public static function nested($model, $attr = [])
    {
        return ["*" => self::make($model, $attr) ];
    }

    /**
     * Returns the structure for a paginated-collection
     * 
     * @param string $model
     * @param array $attr
     * @param array $pagerAttr
     * @return array
     */
    public static function pager($model, $attr = [], $pagerAttr = [])
    {
        $pager = self::make(self::MODEL_PAGER, $pagerAttr);
        return array_merge(self::collection($model, $attr), $pager);
    }

    /**
     * Fetches the expected model
     * 
     * @param string $model
     * @param array|null $attr
     * @return array
     */
    public static function make($model, $attr = [])
    {
        return self::loadModel($model)->fetch($attr);
    }    
    
    /**
     * Attempts to located and load the expected model, throwing exception on err.
     * 
     * @param string $model
     * @throws ClassNotFoundException
     * @return ModelStructureInterface
     */
    public static function loadModel($model)
    {
        try {
            $className = self::MODEL_PATH.$model;
            $class = new $className();
            return $class;
        } catch (Exception $ex) {
            throw new \Symfony\Component\Debug\Exception\ClassNotFoundException;
        }
    }
    
}
