<?php

namespace Tests\Unit\Middleware;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;

/**
 * MiddleWare Test - ContentSecurityPolicyHeadersTest
 *
 * @author Freshworks <info@freshworks.io>
 */

class ContentSecurityPolicyHeadersTest extends TestCase
{

    use DatabaseTransactions;

    protected $stack;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Test that the content-quality and content-security-policy headers are set correctly
     *
     * @group Unit
     * @group Middleware
     * @group ContentSecurityPolicyHeaders
     */
    public function testHeadersFoundAndMatch()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);

        $clientType = \App\Models\ClientType::find(\App\Models\ClientType::SYSTEM_ID);

        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'clientid:system', 'content-security']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $list = \Config::get('project.headers');
        foreach($list AS $item) {
//            dump([
//                'header' => $item['header'],
//                'expected' => $item['value'],
//                'actual' => $response->headers->get($item['header'])
//            ]);
            $this->assertTrue( $response->headers->has($item['header']), "Missing Header: {$item['header']}");
            $this->assertTrue( $response->headers->contains($item['header'], $item['value']), "Incorrect Header Value: {$item['value']}" );
        }




    }
}
