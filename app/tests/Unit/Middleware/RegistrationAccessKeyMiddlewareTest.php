<?php

namespace Tests\Unit\Middleware;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\RegistrationAccessKey;

/**
 * MiddleWare Test - RegistrationAccessKey
 *
 * @author Freshworks <info@freshworks.io>
 */

class RegistrationAccessKeyMiddlewareTest extends TestCase 
{
    
    use DatabaseTransactions;

    protected $stack;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->stack = [
           'missing_rak' => ['message' => trans('auth.missing_rak')],
           'invalid_rak' => ['message' => trans('auth.invalid_rak')]
            ];
        $this->payload = [
            'province_id' => 1,
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'sex' => 'Male',
            'date_of_birth' => $this->faker->date(),
            'health_care' => '123456789012',
            'drivers_license' => '12345678901234',
            'military_services' => '123456789012',
            'city' => $this->faker->city,
            'address' => $this->faker->address,
            'postal_code' => $this->faker->postcode,
            'email' => $this->faker->safeEmail,
            'exclude_organ_types' => null,
            'wishes' => $this->faker->sentence(12)
        ];
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }
    
    /**
     * Test that a route passes with a valid client-id
     * 
     * @group Unit
     * @group Middleware
     * @group RegistrationAccessKey
     */
    public function testRoutePassesWithValidId()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);
        
        $reg = factory(RegistrationAccessKey::class)->create();
        
        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'regkey']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Registration-Access-Key' => $reg->key]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
    }
    
    /**
     * Test that a route fails and key becomes invalidated
     * 
     * @group Unit
     * @group Middleware
     * @group RegistrationAccessKey
     */
    public function testRouteFailsAfterKeyUsed()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);
        
        $reg = factory(RegistrationAccessKey::class)->create();
        
        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'regkey']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Registration-Access-Key' => $reg->key]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $reg = RegistrationAccessKey::byKey($reg->key);
        $this->assertTrue($reg->isUsed());

        
    }
    
    /**
     * Test that the middleware catches when an invalid client-id is passed
     * 
     * @group Unit
     * @group Middleware
     * @group RegistrationAccessKey
     */
    public function testRouteFailsWithInvalidId()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);
        
        $uuid = strtoupper(\Uuid::uuid4()->toString());
        $this->missingFromDatabase('registration_access_keys', ['key' => $uuid]);

        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'regkey']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Registration-Access-Key' => $uuid]);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
        $this->seeJsonContains($this->stack['invalid_rak']);
    }
    
    /**
     * Test that the middleware catches when no header is passed
     * 
     * @group Unit
     * @group Middleware
     * @group RegistrationAccessKey
     */
    public function testRouteFailsWithoutHeader()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);

        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'regkey']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
        $this->seeJsonContains($this->stack['missing_rak']);
    }
}    