<?php

namespace Tests\Unit\Middleware;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;

/**
 * MiddleWare Test - ClientIdEnforcementTest
 *
 * @author Freshworks <info@freshworks.io>
 */

class ClientIdEnforcementTest extends TestCase 
{
    
    use DatabaseTransactions;

    protected $stack;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->stack = [
           'missing_appid' => ['message' => trans('auth.missing_appid')],
           'invalid_appid' => ['message' => trans('auth.invalid_appid')]
            ];
    }
    
    /**
     * Test that a route passes with a valid client-id
     * 
     * @group Unit
     * @group Middleware
     * @group ClientIdEnforcement
     */
    public function testRoutePassesWithValidId()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);
        
        $clientType = \App\Models\ClientType::find(\App\Models\ClientType::WEB_ID);
        
        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'clientid:web']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
    }
    
    /**
     * Test that the routes work as expected when multiple types are enabled
     * 
     * @group Unit
     * @group Middle
     * @group ClientIdEnforcement
     */
    public function testRoutePassesWithMultipleClientOptions()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);
        
        $clientType = [
            'web' => \App\Models\ClientType::find(\App\Models\ClientType::WEB_ID),
            'sys' => \App\Models\ClientType::find(\App\Models\ClientType::SYSTEM_ID),
            'apl' => \App\Models\ClientType::find(\App\Models\ClientType::APPLE_ID)
        ];
        
        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'clientid:web|system|']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Application-Id' => $clientType['web']->uuid]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        config(["local.disable_client_enforcement" => false]);
        
        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Application-Id' => $clientType['sys']->uuid]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        config(["local.disable_client_enforcement" => false]);

        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Application-Id' => $clientType['apl']->uuid]);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
        config(["local.disable_client_enforcement" => false]);
        $this->seeJsonContains($this->stack['invalid_appid']);
    }

    /**
     * Test that the middleware catches when an invalid client-id is passed
     * 
     * @group Unit
     * @group Middleware
     * @group ClientIdEnforcement
     */
    public function testRouteFailsWithInvalidId()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);
        
        $uuid = strtoupper(\Uuid::uuid4()->toString());
        $this->missingFromDatabase('client_types', ['uuid' => $uuid]);

        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'clientid:web|system|']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account', [], [], [], ['HTTP_X-Application-Id' => $uuid]);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
        $this->seeJsonContains($this->stack['invalid_appid']);
    }
    
    /**
     * Test that the middleware catches when no header is passed
     * 
     * @group Unit
     * @group Middleware
     * @group ClientIdEnforcement
     */
    public function testRouteFailsWithoutHeader()
    {
        $user = \App\Models\User::find(1);
        $this->actingAs($user);
        
        $uuid = strtoupper(\Uuid::uuid4()->toString());
        $this->missingFromDatabase('client_types', ['uuid' => $uuid]);

        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'clientid:web|system|']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
        $this->seeJsonContains($this->stack['missing_appid']);
    }
}    