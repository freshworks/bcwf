<?php

namespace Tests\Unit\Middleware;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;

/**
 * MiddleWare Test - ThrottleRequestsTest
 *
 * @author Freshworks <info@freshworks.io>
 */

class ThrottleRequestsTest extends TestCase 
{
    
    use DatabaseTransactions;
    
    /**
     * Test that the middleware-configuration works as expected
     * 
     * @group Unit
     * @group Middleware
     * @group ThrottleRequests
     */
    public function testMiddlewareConfiguration()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $this->actingAs($user);
        
        $router  = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'throttle:2,1']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });
        
        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertTrue( $response->headers->has('x-ratelimit-limit'));
        $this->assertTrue( $response->headers->contains('x-ratelimit-limit', 2) );
        $this->assertTrue( $response->headers->has('x-ratelimit-remaining'));
        $this->assertTrue( $response->headers->contains('x-ratelimit-remaining', 1) );
        
        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertTrue( $response->headers->has('x-ratelimit-limit'));
        $this->assertTrue( $response->headers->contains('x-ratelimit-limit', 2) );
        $this->assertTrue( $response->headers->has('x-ratelimit-remaining'));
        $this->assertTrue( $response->headers->contains('x-ratelimit-remaining', 0) );

        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_TOO_MANY_REQUESTS, $response->status());
        $this->assertFalse( $response->headers->has('x-ratelimit-limit'));
        $this->assertFalse( $response->headers->has('x-ratelimit-remaining'));

        $contentTest = str_before(trans('exception.throttle_request.rate_exceeded'), ':retry');
        $this->assertStringStartsWith($contentTest, $response->content());
        
    }
    
    
    
}
