<?php

namespace Tests\Unit\Middleware;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ErrorCodes;

/**
 * Test the CORS-Supporting Middleware
 */
class CorsMiddlewareTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that the OPTIONS method returns a 200 code as expected
     *
     * @group Unit
     * @group Middleware
     * @group Cors
     */
    public function testOptionsMethod()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $this->actingAs($user);
        $response = $this->call("OPTIONS", "/api/account");
        $this->assertEquals($response->status(), ErrorCodes::HTTP_OK, "Failed to assert that an OPTIONS request method returns OK");
    }
}
