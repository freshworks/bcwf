<?php

namespace Tests\Unit\Middleware;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\ {User, Admin};

/**
 * MiddleWare Test - AuthType Enforcement
 *
 * @author Freshworks <info@freshworks.io>
 */

class AuthTypeMiddlewareTest extends TestCase
{

    use DatabaseTransactions;

    protected $stack;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->stack = [
            'invalid_authtype' => ['message' => trans('auth.invalid_authtype')]
        ];
    }

    /**
     * Test that registrant restrictions pass with valid registrants
     *
     * @group Unit
     * @group Middleware
     * @group AuthType
     */
    public function testRoutePassesWithValidRegistrant()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $admin = factory(Admin::class)->states('staff')->create();
        $this->actingAs($user);

        $router = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'authtype:registrant']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
    }

    /**
     * Test that registrant restrictions fail with non-registrants
     *
     * @group Unit
     * @group Middleware
     * @group AuthType
     */
    public function testRouteFailsWithNonRegistrant()
    {
        $admin = factory(Admin::class)->states('staff')->create();
        $this->actingAs($admin->userRecord);

        $router = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'authtype:registrant']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
        $this->seeJsonContains($this->stack['invalid_authtype']);

    }
    /**
     * Test that admin restrictions pass with valid admins
     *
     * @group Unit
     * @group Middleware
     * @group AuthType
     */
    public function testRoutePassesWithValidAdmin()
    {
        $admin = factory(Admin::class)->states('staff')->create();
        $this->actingAs($admin->userRecord);

        $router = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'authtype:admin']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
    }

    /**
     * Test that admin restrictions fail with non-admins
     *
     * @group Unit
     * @group Middleware
     * @group AuthType
     */
    public function testRouteFailsWithNonAdmin()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $this->actingAs($user);

        $router = app('router');
        $router->group(['prefix' => 'test', 'middleware' => ['auth:api', 'authtype:admin']], function () use ($router) {
            $router->get('/account', '\App\Http\Controllers\AccountController@show');
        });

        $response = $this->call('GET', '/test/account');
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
        $this->seeJsonContains($this->stack['invalid_authtype']);

    }

}