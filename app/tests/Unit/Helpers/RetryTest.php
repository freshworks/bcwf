<?php

namespace Tests\Unit\Helpers;

use TestCase;
use App\Facades\Retry;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;


/**
 * Retry Helper Test Cases
 *
 * @author Freshworks <info@freshworks.io>
 */
class RetryTest extends TestCase
{
    
    use DatabaseTransactions;
    
    /**
     * Test the start method passes as expected
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testStartPasses()
    {
        $name = "Donkey";
        $expected = $name."!!";
        $func = function() use($name) {
            return $name."!!";
        };
        $actual   = Retry::start($func, 3);
        $this->assertEquals($expected, $actual);
    }
    
    /**
     * Test the start method passes when there is an exception in the closure
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testStartWithException()
    {
        $fail = true;
        $func = function() use (&$fail) {
            if ($fail) {
                $fail = false;
                throw new \Exception("Boom!");
            } else {
                return "Donkey!";
            }
        };
        $result = Retry::start($func, 5);
        $this->assertEquals("Donkey!", $result);        
    }

    /**
     * Test the start method runs the expected amount of times
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testStartRunsExpectedNumberOfRetries()
    {
        $count = 0;
        $retries = 7;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        try {
            $result = Retry::start($func, $retries);
        } catch(\Exception $e) {}
        $this->assertEquals($retries+1, $count);
    }

    /**
     * Test the start method throws the expected exception when retries are exhausted
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testStartThrowsExpectedException()
    {
        $count = 0;
        $retries = 7;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        $this->expectException(\App\Exceptions\ExcessiveRetry::class);
        $result = Retry::start($func, $retries);
    }
    
    /**
     * Test the delay method passes as expected
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testDelayPasses()
    {
        $name = "Donkey";
        $expected = $name."!!";
        $func = function() use($name) {
            return $name."!!";
        };
        $actual   = Retry::delay($func, 3, 0.25);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the delay method passes when there is an exception in the closure
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testDelayWithException()
    {
        $fail = true;
        $func = function() use (&$fail) {
            if ($fail) {
                $fail = false;
                throw new \Exception("Boom!");
            } else {
                return "Donkey!";
            }
        };
        $result = Retry::delay($func, 3, 0.25);
        $this->assertEquals("Donkey!", $result);        
    }

    /**
     * Test the delay method runs the expected amount of times
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testDelayRunsExpectedNumberOfRetries()
    {
        $count = 0;
        $retries = 2;
        $delay = 0.25;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        try {
            $result = Retry::delay($func, $retries, $delay);
        } catch(\Exception $e) {}
        $this->assertEquals($retries+1, $count);
    }

    /**
     * Test the delay method throws the expected exception when retries are exhausted
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testDelayThrowsExpectedException()
    {
        $count = 0;
        $retries = 2;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        $this->expectException(\App\Exceptions\ExcessiveRetry::class);
        $result = Retry::delay($func, $retries, 0.25);
    }

    /**
     * Test that the delay method waits the expected time between retries
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testDelayWaitsExpectedTime()
    {
        $count = 0;
        $retries = 2;
        $delay = 1;
        $time['start'] = Carbon::now()->timestamp;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        try {
            $result = Retry::delay($func, $retries, $delay);
        } catch (\Exception $ex) {}

        $this->assertEquals($retries+1, $count);
        
        $time['end'] = Carbon::now()->timestamp;
        $time['diff'] = round($time['end']-$time['start']);
        $this->assertEquals($time['diff'], $retries*$delay);
    }

    /**
     * Test the backoff method passes as expected
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testBackoffPasses()
    {
        $name = "Donkey";
        $expected = $name."!!";
        $func = function() use($name) {
            return $name."!!";
        };
        $actual   = Retry::backoff($func, 1, 0.25);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the backoff method passes when there is an exception in the closure
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testBackoffWithException()
    {
        $fail = true;
        $func = function() use (&$fail) {
            if ($fail) {
                $fail = false;
                throw new \Exception("Boom!");
            } else {
                return "Donkey!";
            }
        };
        $result = Retry::backoff($func, 1, 0.25);
        $this->assertEquals("Donkey!", $result);        
    }

    /**
     * Test the backoff method runs the expected amount of times
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testBackoffRunsExpectedNumberOfRetries()
    {
        $count = 0;
        $retries = 2;
        $delay = 0.25;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        try {
            $result = Retry::backoff($func, $retries, $delay);
        } catch(\Exception $e) {}
        $this->assertEquals($retries+1, $count);
    }

    /**
     * Test the backoff method throws the expected exception when retries are exhausted
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testBackoffThrowsExpectedException()
    {
        $count = 0;
        $retries = 2;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        $this->expectException(\App\Exceptions\ExcessiveRetry::class);
        $result = Retry::backoff($func, $retries, 0.25);
    }

    /**
     * Test that the backoff method waits the expected time between retries
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testBackoffWaitsExpectedTime()
    {
        $count = 0;
        $retries = 2;
        $delay = 1;
        $expected = 6;
        $time['start'] = Carbon::now()->timestamp;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        try {
            $result = Retry::backoff($func, $retries, $delay);
        } catch (\Exception $ex) {}

        $this->assertEquals($retries+1, $count);
        
        $time['end'] = Carbon::now()->timestamp;
        $time['diff'] = round($time['end']-$time['start']);
        $this->assertEquals($expected, $time['diff']);
    }
    
    /**
     * Test that the backoff method waits the expected time between retries with sub-zero exponents
     * 
     * @group Unit
     * @group Helpers
     * @group Retry
     */
    public function testBackoffWaitsExpectedTimeWithMicroTime()
    {
        $count = 0;
        $retries = 3;
        $delay = 0.25;
        $expected = 7;
        $time['start'] = Carbon::now()->timestamp;
        $func = function() use(&$count) {
            $count++;
            throw new \Exception("Boom!");
        };
        try {
            $result = Retry::backoff($func, $retries, $delay);
        } catch (\Exception $ex) {}

        $this->assertEquals($retries+1, $count);
        
        $time['end'] = Carbon::now()->timestamp;
        $time['diff'] = round($time['end']-$time['start']);
        $this->assertEquals($expected, $time['diff']);
    }
    
}