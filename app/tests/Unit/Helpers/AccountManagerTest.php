<?php

namespace Tests\Unit\Helpers;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use App\Exceptions\Util\InvalidArgumentException;
use App\Models\User;
use App\Models\UserType;
use App\Models\Registrant;
use App\Models\RegistrantRecord;
use App\Facades\AccountManager;

class AccountManagerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test we can create a Registrant User record
     * 
     * @group Unit
     * @group Helpers
     * @group AccountManager
     */
    public function testRegistrantUserAccountCreation()
    {
        $registrant = factory(Registrant::class)->create();
        $user = AccountManager::createRegistrantUserAccount($registrant);

        $expected = User::find($user->id);

        $this->assertEquals($expected->id, $user->id);
        $this->assertEquals($expected->account_id, $user->account_id);
        $this->assertEquals($expected->type_id, UserType::REGISTRANT_ID);
    }

    /**
     * Test an exception is thrown when the registrant ID is missing
     * 
     * @group Unit
     * @group Helpers
     * @group AccountManager
     */
    public function testEmptyRegistrantParameterThrowsException()
    {
        $registrant = new Registrant();
        $users = User::all()->count();
        
        $this->expectException(InvalidArgumentException::class);

        AccountManager::createRegistrantUserAccount($registrant);
        
        $this->assertEquals(User::all()->count(), $users);
    }
    
    /**
     * Test that the registrantExists method works with the expected params
     * 
     * @group Unit
     * @group Helpers
     * @group AccountManager
     */
    public function testRegistrantExists()
    {
        $record = factory(RegistrantRecord::class)->states(['active'])->create();

        // Check with single-params
        foreach(['health_care', 'drivers_license', 'military_services'] AS $key) {
            $checkData = ['first_name' => $record->first_name, 'last_name' => $record->last_name, $key => $record->$key];
            $actual = AccountManager::registrantExists($checkData);
            $this->assertInstanceOf(RegistrantRecord::class, $actual);
            $this->assertEquals($record->registrant_id, $actual->registrant_id);
        }
        
        // Check with multiple-params
        foreach(['health_care', 'drivers_license', 'military_services'] AS $key) {
            $checkData[$key] = $record[$key];
            $actual = AccountManager::registrantExists($checkData);
            $this->assertInstanceOf(RegistrantRecord::class, $actual);
            $this->assertEquals($record->registrant_id, $actual->registrant_id);
        }
    }

    /**
     * Test that the registrantExists method fails with incorrect params
     * 
     * @group Unit
     * @group Helpers
     * @group AccountManager
     */
    public function testRegistrantExistsFailsWithBadParams()
    {
        $record = factory(RegistrantRecord::class)->states(['active'])->create();

        // Check with single-params altered to be incorrect
        foreach(['health_care', 'drivers_license', 'military_services'] AS $key) {
            foreach(['first_name', 'last_name', $key] AS $alter) {
                $checkData = ['first_name' => $record->first_name, 'last_name' => $record->last_name, $key => $record->$key];
                $checkData[$alter] = $checkData[$alter].'mcnancypants';
                $this->missingFromDatabase('registrant_records', $checkData);
                $actual = AccountManager::registrantExists($checkData);
                $this->assertNull($actual);
            }
        }
    }
    
    /**
     * Test that the registrantDelete method works as expected
     * 
     * @group Unit
     * @group Helpers
     * @group AccountManager
     * @group RegistrantDelete
     */
    public function testRegistrantDelete()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg =  Registrant::find($user->account_id);
        $addRec = factory(RegistrantRecord::class,4)->states(['active'])->create(['registrant_id' => $reg->id]);
        $rec = $reg->activeRecord();
        
        $this->seeInDatabase('users', ['id' => $user->id]);
        $this->seeInDatabase('registrants', ['id' => $reg->id]);
        $this->seeInDatabase('registrant_records', ['registrant_id' => $reg->id]);
        $this->seeInDatabase('registrant_records', ['id' => $rec->id]);

        AccountManager::deleteRegistrant($reg);
        
        $this->missingFromDatabase('users', ['id' => $user->id]);
        $this->missingFromDatabase('registrants', ['id' => $reg->id]);
        $this->missingFromDatabase('registrant_records', ['registrant_id' => $reg->id]);
        $this->missingFromDatabase('registrant_records', ['id' => $rec->id]);
        
    }

    

    
}
