<?php

namespace Tests\Unit\Helpers;

use TestCase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Aws\Sdk;

class AmazonInstanceHelperTest extends TestCase
{
    /**
     * Test createSdk returns an instance of Sdk
     *
     * @group Unit
     * @group Helpers
     */
    public function testCreateSDK()
    {
        $mockInstanceId = "mockInstanceId";
        $mockEnvName = "mockEnvName";
        $mockInstances = [
            [
                'LaunchTime' => 999,
                'InstanceId' => 11
            ]
        ];
        $args = [];

        $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();

        $this->assertEquals($helper->createSDK($args), new Sdk($args));
    }

    /**
     * Test getClient method works as expected 
     *
     * @group Unit
     * @group Helpers
     */
    public function testGetClient()
    {
        $mockEC2Obj = "mockEC2Obj";
        $awsSdk = \Mockery::mock("\Aws\Sdk");
        $awsSdk->shouldReceive('createEC2')->once()->andReturn($mockEC2Obj);

        $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();
        $helper->shouldReceive('createSDK')->once()->with([
            'region'  => env('AWS_REGION', 'us-west-2'),
            'version' => env('AWS_VERSION', 'latest'),
            'credentials' => [
                'key'    => env('AWS_KEY'),
                'secret' => env('AWS_SECRET'),
            ],
        ])->andReturn($awsSdk);

        $this->assertEquals($mockEC2Obj, $helper->getClient());
    }

    /**
     * Test getInstanceId method works as expected 
     *
     * @group Unit
     * @group Helpers
     */
    public function testgetInstanceId()
    {
        $mockCurl = "mockCurl";
        $mockInstanceId = "mockInstanceId";

        $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();
        $helper->shouldReceive('curl_init')->with('http://169.254.169.254/latest/meta-data/instance-id')->once()->andReturn($mockCurl);
        $helper->shouldReceive('curl_setopt')->with($mockCurl, CURLOPT_CONNECTTIMEOUT, 1)->once()->andReturn($mockCurl);
        $helper->shouldReceive('curl_setopt')->with($mockCurl, CURLOPT_RETURNTRANSFER, true)->once()->andReturn($mockCurl);
        $helper->shouldReceive('curl_exec')->with($mockCurl)->once()->andReturn($mockInstanceId);

        $this->assertEquals($mockInstanceId, $helper->getCurrentInstanceId());
    }

    /**
     * Test getEnvName method works as expected 
     *
     * @group Unit
     * @group Helpers
     */
    public function testGetEnvName()
    {
        $mockInstanceId = "mockInstanceId";
        $mockEnvName = "mockEnvName";
        $mockEC2 = \Mockery::mock();
        $mockInstances = \Mockery::mock();
        $mockReservationData = [
            [
                'Instances' => [
                    [
                        'Tags' => [
                            [
                                'Key' => 'elasticbeanstalk:environment-name',
                                'Value' => $mockEnvName
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();
        $helper->shouldReceive('getClient')->once()->andReturn($mockEC2);
        $mockEC2->shouldReceive('describeInstances')->once()->with([
            'Filters' => [
                [
                    'Name'   => 'instance-id',
                    'Values' => [$mockInstanceId],
                ],
            ],
        ])->andReturn($mockInstances);
        $mockInstances->shouldReceive('get')->once()->with('Reservations')->andReturn($mockReservationData);

        $this->assertEquals($mockEnvName, $helper->getEnvName($mockInstanceId));
    }
    

    /**
     * Test getEnvInstances method works as expected 
     *
     * @group Unit
     * @group Helpers
     */
    public function testGetEnvInstances()
    {
        $mockEnvName = "mockEnvName";
        $mockEC2 = \Mockery::mock();
        $mockInstances = \Mockery::mock();
        $mockReservationData = [
            [
                'Instances' => [
                    [
                        'State' => [
                            'Code' => 16,
                            'id' => 1,
                        ]
                    ]
                ]
            ],
            [
                'Instances' => [
                    [
                        'State' => [
                            'Code' => 16,
                            'id' => 2,
                        ]
                    ]
                ]
            ],
            [
                'Instances' => [
                    [
                        'State' => [
                            'Code' => 14,
                            'id' => 3,
                        ]
                    ]
                ]
            ],
        ];

        $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();
        $helper->shouldReceive('getClient')->once()->andReturn($mockEC2);
        $mockEC2->shouldReceive('describeInstances')->once()->with([
            'Filters' => [
                [
                    'Name'   => 'tag-value',
                    'Values' => [$mockEnvName],
                ],
            ],
        ])->andReturn($mockInstances);
        $mockInstances->shouldReceive('get')->once()->with('Reservations')->andReturn($mockReservationData);

        $ret = $helper->getEnvInstances($mockEnvName);

        $this->assertNotEquals($mockReservationData, $ret);
        unset($mockReservationData[2]);
        $this->assertNotEquals($mockReservationData, $ret);
    }

    /**
     * Test isLeaderInstance method works as expected with multiple instances
     *
     * @group Unit
     * @group Helpers
     */
    public function testIsLeaderInstanceWithMultipleInstances()
    {
        $mockCommand = \Mockery::mock();
        $mockInstanceId = "mockInstanceId";
        $mockEnvName = "mockEnvName";
        $mockInstances = [
            [
                'LaunchTime' => 999,
                'InstanceId' => 11
            ],
            [
                'LaunchTime' => 123,
                'InstanceId' => 21
            ],
            [
                'LaunchTime' => 322,
                'InstanceId' => 33
            ]
        ];

        $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();
        $helper->shouldReceive('getCurrentInstanceId')->once()->andReturn($mockInstanceId);
        $helper->shouldReceive('getEnvName')->once()->with($mockInstanceId)->andReturn($mockEnvName);
        $helper->shouldReceive('getEnvInstances')->once()->with($mockEnvName)->andReturn($mockInstances);
        $mockCommand->shouldReceive('info')->once()->with('More than one instance running, finding the oldest...');
        $helper->shouldReceive('isSameInstance')->once()->with($mockInstances[1]['InstanceId'], $mockInstanceId)->andReturn(true);
        $ret = $helper->isLeaderInstance($mockCommand);

        $this->assertTrue($ret);
     }

    /**
     * Test isLeaderInstance method works as expected with once instance
     *
     * @group Unit
     * @group Helpers
     */
     public function testIsLeaderInstanceWithOneInstance()
     {
         $mockCommand = \Mockery::mock();
         $mockInstanceId = "mockInstanceId";
         $mockEnvName = "mockEnvName";
         $mockInstances = [
             [
                 'LaunchTime' => 999,
                 'InstanceId' => 11
             ]
         ];
 
         $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();
         $helper->shouldReceive('getCurrentInstanceId')->once()->andReturn($mockInstanceId);
         $helper->shouldReceive('getEnvName')->once()->with($mockInstanceId)->andReturn($mockEnvName);
         $helper->shouldReceive('getEnvInstances')->once()->with($mockEnvName)->andReturn($mockInstances);
         $mockCommand->shouldReceive('info')->once()->with('Only one instance running...');
         $helper->shouldReceive('isSameInstance')->once()->with($mockInstances[0]['InstanceId'], $mockInstanceId)->andReturn(true);
         $ret = $helper->isLeaderInstance($mockCommand);
 
         $this->assertTrue($ret);
      }

    /**
     * Test isLeaderInstance method works as expected with no instance
     *
     * @group Unit
     * @group Helpers
     */
    public function testIsLeaderInstanceWithNoInstance()
    {
        $mockCommand = \Mockery::mock();
        $mockInstanceId = "mockInstanceId";
        $mockEnvName = "mockEnvName";
        $mockInstances = [

        ];

        $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();
        $helper->shouldReceive('getCurrentInstanceId')->once()->andReturn($mockInstanceId);
        $helper->shouldReceive('getEnvName')->once()->with($mockInstanceId)->andReturn($mockEnvName);
        $helper->shouldReceive('getEnvInstances')->once()->with($mockEnvName)->andReturn($mockInstances);
        $ret = $helper->isLeaderInstance($mockCommand);

        $this->assertTrue($ret);
    }

    /**
     * Test isSameInstance method returns true when comparing two of the same instance IDs
     *
     * @group Unit
     * @group Helpers
     */
    public function testIsSameInstance()
    {
        $mockInstanceId = "mockInstanceId";
        $mockEnvName = "mockEnvName";
        $mockInstances = [
            [
                'LaunchTime' => 999,
                'InstanceId' => 11
            ]
        ];
        $oldInstanceId = 11;
        $currentId = 11;

        $helper = \Mockery::mock('\App\Helpers\AmazonInstanceHelper')->makePartial()->shouldAllowMockingProtectedMethods();
        $this->assertEquals($helper->isSameInstance($oldInstanceId, $currentId), true);
    }
}
