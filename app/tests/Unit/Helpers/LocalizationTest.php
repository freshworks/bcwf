<?php

namespace Tests\Unit\Helpers;

use TestCase;
use Illuminate\Support\Facades\{Config};
use App\Facades\Localization;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Exceptions\Util\InvalidArgumentException;

/**
 * Description of LocalizationTest
 *
 * @author Freshworks <info@freshworks.io>
 */
class LocalizationTest extends TestCase
{
    
    use DatabaseTransactions;

    /**
     * Test we can get the default language
     * 
     * @group Unit
     * @group Helpers
     * @group Localization
     */
    public function testGetDefault()
    {
        $expects = Config::get('project.localization.default');
        $actual  = Localization::getDefault();
        $this->assertEquals($expects, $actual, "Failed to get the default language.");
    }

    /**
     * Test we can get the default language
     * 
     * @group Unit
     * @group Helpers
     * @group Localization
     */
    public function testGetAvailable()
    {
        $expects = Config::get('project.localization.langs');
        $actual  = Localization::getAvailable();
        $this->assertTrue( is_array($actual) , "Failed to assert that the available languages are returned as an array");
        $this->assertEquals($expects, $actual, "Failed to get the available language.");
    }

    /**
     * Test the isValidLangauge method works as expected
     * 
     * @group Unit
     * @group Helpers
     * @group Localization
     */
    public function testIsValidLanguage()
    {
        $avail  = Localization::getAvailable();
        $invalid = ['foo', 'bar'];

        // Ensure that our forced-invalids do not exist and return false as expected
        foreach($invalid AS $key) {
            $this->assertFalse( array_key_exists($key, $avail) );
            $this->assertFalse( Localization::isValidLanguage($key) );
        }

        // Check the available ones
        foreach($avail AS $key => $name) {
            $this->assertTrue( Localization::isValidLanguage($key) );
        }
    }

    /**
     * Test the getLangauge method returns the default language when not pre-set
     * 
     * @group Unit
     * @group Helpers
     * @group Localization
     */
    public function testGetLangaugeReturnsDefaultIfNotSet()
    {
        $expected = Localization::getDefault();
        $actual   = Localization::getLanguage();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test the getLangauge method returns the selected language when changed
     * 
     * @group Unit
     * @group Helpers
     * @group Localization
     */
    public function testGetLangaugeReturnsSelectedLanguage()
    {
        $default = Localization::getDefault();
        $available = Localization::getAvailable();
        foreach($available AS $key => $name) {
            if( $key != $default) {
                $expected = $key;
                break;
            }
        }
        $this->assertTrue( ($expected != $default) );
        $this->assertEquals($default, Localization::getLanguage());
        
        app('translator')->setLocale($expected);
        $this->assertEquals($expected, Localization::getLanguage());

    }
    
    /**
     * Test that the setLanguage method works as expected
     * 
     * @group Unit
     * @group Helpers
     * @group Localization
     */
    public function testSetLangauge()
    {
        $default = Localization::getDefault();
        $this->assertEquals($default, Localization::getLanguage());
        
        $avail = Localization::getAvailable();
        foreach($avail AS $key => $name) {
            $this->assertTrue( Localization::setLanguage($key) );
            $this->assertEquals($key,Localization::getLanguage());
        }
    }

    /**
     * Test that the setLanguage method works as expected
     * 
     * @group Unit
     * @group Helpers
     * @group Localization
     */
    public function testSetLangaugeThrowsExceptionWithInvalidLangauge()
    {
        $invalid = 'foobar';
        $this->assertFalse(Localization::isValidLanguage($invalid));

        $this->expectException(InvalidArgumentException::class);
        Localization::setLanguage($invalid);
    }
   
    /**
     * Test that the getAvailableAsString returns as expected
     * 
     * @group Unit
     * @group Helpers
     * @group Localization
     */
    public function testGetAvailableAsString()
    {
        $expected = Localization::getAvailableAsString();
        $this->assertTrue( is_string($expected) );
        $this->assertTrue( strlen($expected) > 2 );
    }
    
}
