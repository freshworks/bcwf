<?php

namespace Tests\Unit\Helpers;

use TestCase;
use App\Exceptions\Util\InvalidArgumentException;
use App\Facades\ArrayUtil;


class ArrayUtilTest extends TestCase
{

    /**
     * Test that recursiveDiff returns an array of differences between two multi-dimensional arrays
     *
     * @group Helper
     * @group ArrayUtils
     */
    public function testRecursiveDiff()
    {
        $arr1 = [
            'a' => 1,
            'b' => 2,
            'c' => [1, 2, 3],
        ];
        $arr2 = [
            'a' => 1,
            'b' => 3,
            'c' => null,
        ];
        $arr3 = [
            'a' => 1,
            'b' => 2,
            'c' => [
                1,
                2,
                3 => [1, 2],
            ],
        ];
        $expectedArr1Arr2 = [
            'b' => 2,
            'c' => [1, 2, 3],
        ];
        $expectedArr3Arr1 = [
            'c' => [3 => [1, 2]],
        ];
        $expectedArr3Arr2 = [
            'b' => 2,
            'c' => [
                1,
                2,
                3 => [1, 2],
            ],
        ];

        $this->assertEquals($expectedArr1Arr2, ArrayUtil::recursiveDiff($arr1, $arr2));
        $this->assertEquals($expectedArr3Arr1, ArrayUtil::recursiveDiff($arr3, $arr1));
        $this->assertEquals($expectedArr3Arr2, ArrayUtil::recursiveDiff($arr3, $arr2));
    }

     /**
     * Test that paginateCollection returns a paginated array
     *
     * @group Helper
     * @group ArrayUtils
     */
    public function testPaginateCollection()
    {
        $arr = [
            [0 => 'a'],
            [1 => 'b'],
            [2 => 'c'],
            [3 => 'd'],
            [4 => 'e'],
            [5 => 'f'],
            [6 => 'g'],
        ];
        
        $perPage = 3;
        $page = 1;
        $expected = array_slice($arr, 0, 3);
        $result = ArrayUtil::paginateCollection(collect($arr), $perPage, $page);
        
        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('links', $result);
        $this->assertArrayHasKey('meta', $result);
        $this->assertEquals($expected, $result['data']->toArray());

        $page = 2;
        $expected = array_slice($arr, 3, 3);
        $result = ArrayUtil::paginateCollection(collect($arr), $perPage, $page);

        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('links', $result);
        $this->assertArrayHasKey('meta', $result);
        $this->assertEquals($expected, $result['data']->toArray());

        $arr = [];
        $page = 1;
        $expected = [];
        $result = ArrayUtil::paginateCollection(collect($arr), $perPage, $page);

        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('links', $result);
        $this->assertArrayHasKey('meta', $result);
        $this->assertEquals($expected, $result['data']->toArray());
    }
    
}
