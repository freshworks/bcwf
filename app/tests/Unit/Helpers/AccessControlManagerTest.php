<?php

namespace Tests\Unit\Helpers;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use App\Exceptions\Util\InvalidArgumentException;
use App\Models\Admin;
use App\Facades\AccessControlManager;
use App\Enums\{RolesEnum, AbilitiesEnum};


class AccessControlManagerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test roles method returns an array of roles
     *
     * @group Unit
     * @group Helpers
     * @group AccessControlManager
     */
    public function testGettingRoles()
    {
        $roles = AccessControlManager::roles();
        $this->assertTrue(is_array($roles));
        $this->assertEquals(count($roles), count(RolesEnum::getValues()));
    }

    /**
     * Test abilities method returns an array of abilities
     *
     * @group Unit
     * @group Helpers
     * @group AccessControlManager
     */
    public function testGettingAbilities()
    {
        $abilities = AccessControlManager::abilities();
        $this->assertTrue(is_array($abilities));
        $this->assertEquals(count($abilities), count(AbilitiesEnum::getValues()));
    }

    /**
     * Test currentAbilities method returns an array of abilities
     *
     * @group Unit
     * @group Helpers
     * @group AccessControlManager
     */
    public function testGettingCurrentAbilities()
    {
        $group = factory(Admin::class)->states([RolesEnum::GROUP_ADMIN])->create();
        $abilities = AccessControlManager::currentAbilities($group);

        $this->assertTrue(is_array($abilities));
        $this->assertTrue(in_array(false, $abilities));
        $this->assertEquals(count($abilities), count(AbilitiesEnum::getValues()));

        $admin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create();
        $abilities = AccessControlManager::currentAbilities($admin);

        $this->assertTrue(is_array($abilities));
        $this->assertFalse(in_array(false, $abilities));
        $this->assertEquals(count($abilities), count(AbilitiesEnum::getValues()));


    }

    /**
     * Test setRole method returns an admin with a new role
     *
     * @group Unit
     * @group Helpers
     * @group AccessControlManager
     */
    public function testSettingARole()
    {
        $staff = factory(Admin::class)->states([RolesEnum::STAFF])->create();
        $ret = AccessControlManager::setRole($staff, RolesEnum::GROUP_ADMIN);

        $this->assertEquals(RolesEnum::GROUP_ADMIN, $ret->meta->role);
        $this->assertTrue($ret->isA(RolesEnum::GROUP_ADMIN));
        $this->assertFalse($ret->isA(RolesEnum::STAFF));

        $group = factory(Admin::class)->states([RolesEnum::GROUP_ADMIN])->create();
        $ret = AccessControlManager::setRole($group, RolesEnum::SYSTEM_ADMIN);

        $this->assertEquals(RolesEnum::SYSTEM_ADMIN, $ret->meta->role);
        $this->assertTrue($ret->isA(RolesEnum::SYSTEM_ADMIN));
        $this->assertFalse($ret->isA(RolesEnum::GROUP_ADMIN));

        $sysAdmin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create();
        $ret = AccessControlManager::setRole($sysAdmin, RolesEnum::STAFF);

        $this->assertEquals(RolesEnum::STAFF, $ret->meta->role);
        $this->assertTrue($ret->isA(RolesEnum::STAFF));
        $this->assertFalse($ret->isA(RolesEnum::SYSTEM_ADMIN));
    }

}
