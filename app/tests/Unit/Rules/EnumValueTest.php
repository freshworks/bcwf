<?php

use App\Rules\EnumValue;
use Tests\Unit\Enums\{FooEnum, BarEnum};


class EnumValueTest extends TestCase
{
    /**
     * Test that a new EnumValue rule passes validation correctly
     *
     * @group Unit
     * @group EnumValue
     */
    public function testValidationPasses()
    {
        $passes1 = (new EnumValue(FooEnum::class))->passes(null, 1);
        $passes2 = (new EnumValue(BarEnum::class))->passes(null, 'bar-one');
        
        $this->assertTrue($passes1);
        $this->assertTrue($passes2);
    }

    /**
     * Test that a new EnumValue rule fails validation correctly
     *
     * @group Unit
     * @group EnumValue
     */
    public function testValidationFails()
    {
        $fails1 = (new EnumValue(FooEnum::class))->passes(null, 'bar');
        $fails2 = (new EnumValue(BarEnum::class))->passes(null, 'foo');
        
        $this->assertFalse($fails1);
        $this->assertFalse($fails2);
    }

    /**
     * Test that a new EnumValue rule returns a message
     *
     * @group Unit
     * @group EnumValue
     */
    public function testMessage()
    {
        $this->assertEquals(trans('validation.enum_is_invalid'), (new EnumValue(FooEnum::class))->message());
    }
}
