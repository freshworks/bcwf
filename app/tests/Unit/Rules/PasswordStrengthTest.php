<?php

use App\Rules\PasswordStrength;
use Illuminate\Validation\Validator;


class PasswordStrengthTest extends TestCase
{
    /**
     * Test valid password passes validation.
     *
     * @group Unit
     * @group Rules
     * @group PasswordStrength
     */
    public function testValidPasswordFormatPassValidation()
    {
        $data = 'Password1!';
        $v = new PasswordStrength();

        $this->assertTrue($v->passes(null, $data));
    }
    
    /**
     * Test invalid password formats fails validation and returns various error messages.
     * 
     * @group Unit
     * @group Rules
     * @group PasswordStrength
     */
    public function testIncorrectPasswordFormatsFailValidation()
    {
        $data = $this->invalidPasswordStrengthDataProvider();
        $v = new PasswordStrength();

        foreach($data AS $label => $src) {
            $this->assertFalse($v->passes(null, $label));
            $this->assertEquals($v->message(), $src['error_message']);
        }
    }

    /**
     * Data provider for invalid password formats.
     * 
     * @return array
     */
    public function invalidPasswordStrengthDataProvider()
    {
        // Password has insufficient length
        $ret['Passw!1'] = [
            'error_message' => trans('validation.password_has_length') . "\n",
        ];

        // Password does not contain an uppercase character
        $ret['password1!'] = [
            'error_message' => trans('validation.password_has_uppercase') . "\n",
        ];

        // Password does not contain an lowercase character
        $ret['PASSWORD1!'] = [
            'error_message' => trans('validation.password_has_lowercase') . "\n",
        ];

        // Password does not contain a number
        $ret['Password!'] = [
            'error_message' => trans('validation.password_has_number') . "\n",
        ];

        // Password does not contain a special character
        $ret['Password1'] = [
            'error_message' => trans('validation.password_has_special_char') . "\n",
        ];

        // Password that does not pass any validation
        $ret['~'] = [
            'error_message' => $ret['Passw!1']['error_message'] . 
                               $ret['password1!']['error_message'] . 
                               $ret['PASSWORD1!']['error_message'] . 
                               $ret['Password!']['error_message'] . 
                               $ret['Password1']['error_message'],
        ];
        
        return $ret;
    }
}
