<?php

namespace Tests\Unit\Listeners;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Events\Dispatcher;
use App\Mail\ThankYouEmail;
use App\Models\Registrant;
use App\Models\RegistrantRecord;
use App\Enums\RegistrantRecordStatusEnum;

class RegistrantEventSubscriberTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true,
                'local.disable_client_enforcement' => true,
                'local.disable_rak_enforcement'  => true]);
    }

    /**
     * Test the onRegistration event handler
     *
     * @group Unit
     * @group Listeners
     */
    public function testOnRegistration()
    {
        \Mail::fake();
        $registrant = factory(Registrant::class)->create(['subscriber' => true]);
        $user = factory(\App\Models\User::class)->states('enabled')->create(['account_id' => $registrant->id]);
        factory(RegistrantRecord::class)->create([
            'registrant_id' => $registrant->id,
            'status_id' => RegistrantRecordStatusEnum::ACTIVE,
        ]);
        $handler = new \App\Events\Handlers\RegistrationEventHandler();
        $event = new \App\Events\RegistrationEvent($registrant);

        $handler->handle($event);

        \Mail::assertSent(ThankYouEmail::class, 1);
    }

     /**
     * Test the onRegistration event handler and the registrant is unsubscribed
     *
     * @group Unit
     * @group Listeners
     */
    public function testOnRegistrationWhenRegistrantIsUnsubscribed()
    {
        \Mail::fake();
        $registrant = factory(Registrant::class)->create(['subscriber' => false]);
        $user = factory(\App\Models\User::class)->states('enabled')->create(['account_id' => $registrant->id]);
        factory(RegistrantRecord::class)->create([
            'registrant_id' => $registrant->id,
            'status_id' => RegistrantRecordStatusEnum::ACTIVE,
        ]);
        $handler = new \App\Events\Handlers\RegistrationEventHandler();
        $event = new \App\Events\RegistrationEvent($registrant);

        $handler->handle($event);

        \Mail::assertSent(ThankYouEmail::class, 0);
    }

    /**
     * Test the onRegistration event handler will be fired when registering
     *
     * @group Unit
     * @group Listeners
     */
    public function testOnRegistrationEventFired()
    {
        $this->expectsEvents('App\Events\RegistrationEvent');
        $payload = [
            'province_id' => 1,
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'sex' => 'Male',
            'date_of_birth' => $this->faker->date(),
            'health_care' => '123456789012',
            'drivers_license' => '12345678901234',
            'military_services' => '123456789012',
            'city' => $this->faker->city,
            'address' => $this->faker->address,
            'postal_code' => $this->faker->postcode,
            'email' => $this->faker->safeEmail,
            'wishes' => $this->faker->sentence(12),
            'exclude_organ_types' => null,
            'status_id' => 1
        ];
        $response = $this->call('POST', '/api/register', $payload);
    }



}
