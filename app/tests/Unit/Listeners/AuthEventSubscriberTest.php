<?php

namespace Tests\Unit\Listeners;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Events\Dispatcher;
use App\Mail\ThankYouEmail;
use App\Models\Registrant;
use App\Models\RegistrantRecord;


class AuthEventSubscriberTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 
                'local.disable_client_enforcement' => true,
                'local.disable_rak_enforcement'  => true]);
    }
    
    /**
     * Test the onRegistrantLogin event handler
     * 
     * @group Unit
     * @group Listeners
     */
    public function testOnRegistrantLogin()
    {
        $registrant = factory(\App\Models\Registrant::class)->states(['subscriber'])->create();
        $handler = new \App\Events\Handlers\RegistrantLoginEventHandler();
        $event = new \App\Events\RegistrantLoginEvent($registrant);
        $this->assertEquals($handler->handle($event), get_class($handler)."::handle");
    }
    
    /**
     * Test the onLogin event handler
     * 
     * @group Unit
     * @group Listeners
     */
    public function testOnLogin()
    {
        $user = factory(\App\Models\User::class)->states(['admin', 'enabled'])->create();
        $handler = new \App\Events\Handlers\LoginEventHandler();
        $event = new \App\Events\LoginEvent($user);
        $this->assertEquals($handler->handle($event), get_class($handler)."::handle");
    }

    /**
     * Test the onLogout event handler
     * 
     * @group Unit
     * @group Listeners
     */
    public function testOnLogout()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $handler = new \App\Events\Handlers\LogoutEventHandler();
        $event = new \App\Events\LogoutEvent($user);
        $this->assertEquals($handler->handle($event), get_class($handler)."::handle");
    }

    /**
     * Test the onRegistrationAccessKey event handler
     * 
     * @group Unit
     * @group Listeners
     */
    public function testOnRegistrationAccessKey()
    {
        $key = factory(\App\Models\RegistrationAccessKey::class)->create();
        $handler = new \App\Events\Handlers\RegistrationAccessKeyEventHandler();
        $event = new \App\Events\RegistrationAccessKeyEvent($key);
        $this->assertEquals($handler->handle($event), get_class($handler)."::handle");
    }

    /**
     * Test that the proper event is fired when generating a RegistrationAccessKey
     * 
     * @group Unit
     * @group Listeners
     * @group Events
     * @group RegistrationAccessKey
     */
    public function testRegistrationAccessKeyEventIsFired()
    {
        $this->expectsEvents('App\Events\RegistrationAccessKeyEvent');
        $this->call('GET', '/api/registration-access-key');
        
    }
    
}
