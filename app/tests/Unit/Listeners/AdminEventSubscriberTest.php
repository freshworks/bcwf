<?php

namespace Tests\Unit\Listeners;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Events\Dispatcher;
use App\Mail\ {AdminInviteEmail, AdminUpdateEmail, AdminAccountDisabledEmail};
use App\Models\ {Admin, User};
use App\Enums\ {AbilitiesEnum, RolesEnum, UserStatusEnum};
use Symfony\Component\HttpFoundation\Response as ResponseCode;


class AdminEventSubscriberTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config([
            'local.disable_throttle' => true,
            'local.disable_client_enforcement' => true,
            'local.disable_rak_enforcement' => true
        ]);
    }

    /**
     * Test the onInvite event handler
     *
     * @group Unit
     * @group Listeners
     */
    public function testOnInviteHandler()
    {

        \Mail::fake();
        $this->expectsEvents('App\Events\AdminInviteEvent');

        $admin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create();
        $this->actingAs($admin->userRecord);

        $input = [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'group_id' => 1,
            'role' => RolesEnum::STAFF,
        ];

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);

        $newAdmin = Admin::where('email', '=', $input['email'])->first();
        $handler = new \App\Events\Handlers\AdminInviteEventHandler();
        $event = new \App\Events\AdminInviteEvent($newAdmin, $admin);
        $handler->handle($event);

        \Mail::assertSent(AdminInviteEmail::class, 1);

    }

    /**
     * Test the onUpdate event handler
     *
     * @group Unit
     * @group Listeners
     */
    public function testOnUpdateHandler()
    {

        \Mail::fake();
        $this->expectsEvents('App\Events\AdminUpdateEvent');

        $actor = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create();

        $this->actingAs($actor->userRecord);

        $input = [
            'admin_id' => $admin->id,
            'first_name' => "Foo",
            'last_name' => "Bar",
            'role'  => RolesEnum::STAFF,
            'group_id' => $admin->group->id
        ];

        $response = $this->call('POST', '/api/admin/edit', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);

        $handler = new \App\Events\Handlers\AdminUpdateEventHandler();
        $event = new \App\Events\AdminUpdateEvent($admin, $actor);
        $handler->handle($event);

        \Mail::assertSent(AdminUpdateEmail::class, 1);

    }

    /**
     * Test the onDisabled event handler
     *
     * @group Unit
     * @group Listeners
     */
    public function testOnAccountDisabledHandler()
    {

        \Mail::fake();
        $this->expectsEvents('App\Events\AdminAccountDisabledEvent');

        $actor = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create();

        $this->actingAs($actor->userRecord);

        $input = [
            'admin_id' => $admin->id,
            'status_id' => UserStatusEnum::DISABLED
        ];

        $response = $this->call('POST', '/api/admin/change-status', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);

        $handler = new \App\Events\Handlers\AdminAccountDisabledEventHandler();
        $event = new \App\Events\AdminAccountDisabledEvent($admin, $actor);
        $handler->handle($event);

        \Mail::assertSent(AdminAccountDisabledEmail::class, 1);

    }




}
