<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\{ Admin, Group, User, UserType };
use Silber\Bouncer\BouncerFacade;

class AdminTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we get a Admin record correctly
     *
     * @group Unit
     * @group Models
     * @group Admin
     */
    public function testGetsAdmin()
    {
        $admin = factory(Admin::class)->states(['staff'])->create();
        $expected = Admin::find($admin->id);
        $this->assertEquals($expected->id, $admin->id, "Failed to assert that Admin Models are able to fetch results");
    }

    /**
     * Test that we can get the UserRecord relation correctly
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group User
     */
    public function testUserRecordRelation()
    {
        $admin = factory(Admin::class)->states(['staff'])->create();
        $res = User::where(['account_id' => $admin->id, 'type_id' => UserType::ADMIN_ID]);
        $this->assertEquals($res->count(), 1);
        $user = $res->first();
        $this->assertEquals($admin->userRecord->type_id, UserType::ADMIN_ID);
        $this->assertEquals($admin->userRecord->account_id, $admin->id);
        $this->assertInstanceOf(User::class, $admin->userRecord);
    }

    /**
     * Test that we can get the Group relation correctly
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group Group
     */
    public function testGroupRelation()
    {
        $admin = factory(Admin::class)->states(['staff'])->create();
        $group = App\Models\Group::find($admin->group_id);
        $this->assertEquals($admin->group->id, $group->id);
        $this->assertEquals($admin->group()->count(), 1);
        $this->assertInstanceOf(Group::class, $admin->group);
    }

    /**
     * Test that the setRole method correctly sets the json property
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group AccessControl
     */
    public function testGetRole()
    {
        $admin = factory(Admin::class)->states('staff')->create();
        foreach(['system-admin', 'group-admin', 'staff'] AS $role) {
            tap($admin)->update(['meta' => ['role' => $role]]);
            $this->assertEquals($admin->getRole(), $admin->meta->role);
        }
    }

    /**
     * Test that the setRole method correctly sets the json property
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group AccessControl
     */
    public function testSetRole()
    {
        $admin = factory(Admin::class)->states('staff')->create();
        foreach (['system-admin', 'group-admin', 'staff'] as $role) {
            $admin->setRole($role);
            $this->assertEquals($admin->getRole(), $admin->meta->role);
        }
    }

    /**
     * Test that the Bouncer-Aliases work correctly for 'isA'
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group AccessControl
     */
    public function testIsA()
    {
        $roles = ['system-admin', 'group-admin', 'staff'] ;
        $check = $roles;
        array_unshift($check, array_pop($check));
        foreach ($roles AS $i => $role) {
            $admin = factory(Admin::class)->states($role)->create();
            $this->assertTrue($admin->isA($role));
            $this->assertFalse($admin->isA($check[$i]));
        }
    }

    /**
     * Test that the Bouncer-Aliases work correctly for 'isAn'
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group AccessControl
     */
    public function testIsAn()
    {
        $roles = ['system-admin', 'group-admin', 'staff'];
        $check = $roles;
        array_unshift($check, array_pop($check));
        foreach ($roles as $i => $role) {
            $admin = factory(Admin::class)->states($role)->create();
            $this->assertTrue($admin->isAn($role));
            $this->assertFalse($admin->isAn($check[$i]));
        }
    }

    /**
     * Test that the Bouncer-Aliases work correctly for 'getAbilities'
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group AccessControl
     */
    public function testGetAbilities()
    {
        $test = BouncerFacade::role()->firstOrCreate([
            'name' => 'testrole',
            'title' => 'Test Role'
        ]);
        for ($i=0; $i<=5; $i++) {
            $role[$i] = BouncerFacade::ability()->firstOrCreate([
                'name'  => "ability{$i}",
                'title' => "Ability {$i}"
            ]);
            BouncerFacade::allow($test)->to($role[$i]);
        }
        $admin = factory(Admin::class)->create();
        $admin->userRecord->retract('staff');
        $admin->userRecord->assign('testrole');
        $abilities = $admin->getAbilities();

        $this->assertEquals($admin->getAbilities(), $admin->userRecord->getAbilities());
        $this->assertEquals(count($role), count($abilities));

        foreach($role AS $i => $d) {
            $this->assertNotNull( $abilities->first( function($item) use ($i) {
                return $item->name == "ability{$i}";
            }));
        }
    }

    /**
     * Test that the 'can' method correctly maps to the users-ability check
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group AccessControl
     */
    public function testCan()
    {

        $test = BouncerFacade::role()->firstOrCreate([
            'name' => 'testrole',
            'title' => 'Test Role'
        ]);
        for ($i = 0; $i <= 5; $i++) {
            $role[$i] = BouncerFacade::ability()->firstOrCreate([
                'name' => "ability{$i}",
                'title' => "Ability {$i}"
            ]);
            BouncerFacade::allow($test)->to($role[$i]);
        }
        $admin = factory(Admin::class)->create();
        $admin->userRecord->retract('staff');
        $admin->userRecord->assign('testrole');

        $staff = factory(Admin::class)->create();

        foreach($role AS $i => $r) {
            $this->assertTrue($admin->userRecord->can($r->name));
            $this->assertTrue($admin->can($r->name));
            $this->assertFalse($staff->userRecord->can($r->name));
            $this->assertFalse($staff->can($r->name));
        }

    }




}
