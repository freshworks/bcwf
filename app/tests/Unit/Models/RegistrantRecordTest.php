<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\Registrant;
use App\Models\RegistrantRecord;
use App\Models\Province;
use App\Enums\RegistrantRecordStatusEnum;

class RegistrantRecordTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we get a RegistrantRecord correctly
     *
     * @group Unit
     * @group Models
     * @group RegistrantRecord
     */
    public function testGetsRegistrantRecord()
    {
        $registrantRecord = factory(RegistrantRecord::class)->create();
        $expected = RegistrantRecord::find($registrantRecord->id);

        $this->assertEquals($expected->id, $registrantRecord->id);
    }

    /**
     * Test that we can get the Registrant relation correctly
     *
     * @group Unit
     * @group Models
     * @group RegistrantRecord
     */
    public function testRegistrantRelation()
    {
        $registrant = factory(Registrant::class)->create();
        $registrantRecord = factory(RegistrantRecord::class)->create([
            'registrant_id' => $registrant->id,
        ]);

        factory(Registrant::class, 5)->create();

        $this->assertInstanceOf(Registrant::class, $registrantRecord->registrant);
        $this->assertEquals($registrantRecord->registrant->id, $registrant->id);
    }

    /**
     * Test that we can get the Province relation correctly
     *
     * @group Unit
     * @group Models
     * @group RegistrantRecord
     */
    public function testProvinceRelation()
    {
        $registrantRecord = factory(RegistrantRecord::class)->create();
        $province = Province::find($registrantRecord->province_id);

        $this->assertInstanceOf(Province::class, $registrantRecord->province);
        $this->assertEquals($registrantRecord->province->id, $province->id);
    }

    /**
     * Test that the RegistrantRecord DB Trigger works as expected
     *
     * @group Unit
     * @group Models
     * @group RegistrantRecord
     * @group Database
     */
    public function testTrigger()
    {
        $record = factory(App\Models\RegistrantRecord::class)->states(['active'])->create();
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords);
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords->where('status_id', RegistrantRecordStatusEnum::ACTIVE));

        factory(App\Models\RegistrantRecord::class,3)->states(['active'])->create(['registrant_id' => $record->registrant_id]);
        $this->assertCount(4, Registrant::find($record->registrant_id)->registrantRecords);
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords->where('status_id', RegistrantRecordStatusEnum::ACTIVE));
    }

    /**
     * Test that the scopeActive method filters as expected
     *
     * @group Unit
     * @group Models
     * @group RegistrantRecord
     */
    public function testScopeActive()
    {
        $record = factory(App\Models\RegistrantRecord::class)->states(['active'])->create();
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords()->active()->get());

        factory(App\Models\RegistrantRecord::class,3)->states(['active'])->create(['registrant_id' => $record->registrant_id]);
        $this->assertCount(4, Registrant::find($record->registrant_id)->registrantRecords);
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords()->active()->get());

    }

    /**
     * Test that the scopeArchived method filters as expected
     *
     * @group Unit
     * @group Models
     * @group RegistrantRecord
     */
    public function testScopeArchived()
    {
        $record = factory(App\Models\RegistrantRecord::class)->states(['active'])->create();
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords()->active()->get());
        $this->assertCount(0, Registrant::find($record->registrant_id)->registrantRecords()->archived()->get());

        factory(App\Models\RegistrantRecord::class,3)->states(['active'])->create(['registrant_id' => $record->registrant_id]);
        $this->assertCount(4, Registrant::find($record->registrant_id)->registrantRecords);
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords()->active()->get());
        $this->assertCount(3, Registrant::find($record->registrant_id)->registrantRecords()->archived()->get());

    }




}
