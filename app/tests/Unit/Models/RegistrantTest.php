<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\Registrant;
use App\Models\RegistrantRecord;
use App\Models\ClientType;
use App\Models\User;
use App\Models\UserType;

class RegistrantTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we can get the UserRecord relation correctly
     *
     * @group Unit
     * @group Models
     * @group Admin
     * @group User
     */
    public function testUserRecordRelation()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg = Registrant::find($user->account_id);

        $this->assertEquals($reg->userRecord->type_id, UserType::REGISTRANT_ID);
        $this->assertEquals($reg->userRecord->id, $user->id);
        $this->assertInstanceOf(User::class, $reg->userRecord);
    }

    /**
     * Test that we get a Registrant correctly
     *
     * @group Unit
     * @group Models
     * @group Registrant
     */
    public function testGetsRegistrant()
    {
        $registrant = factory(Registrant::class)->create();
        $expected = Registrant::find($registrant->id);

        $this->assertEquals($expected->id, $registrant->id);
    }

    /**
     * Test that we can get the RegistrantRecord relation correctly
     *
     * @group Unit
     * @group Models
     * @group Registrant
     */
    public function testRegistrantRecordRelation()
    {
        $registrant = factory(Registrant::class)->create();
        $registrantRecord = factory(RegistrantRecord::class)->create([
            'registrant_id' => $registrant->id,
        ]);

        factory(RegistrantRecord::class, 5)->create();

        $this->assertEquals($registrant->registrantRecords->count(), 1);
        $this->assertEquals($registrant->registrantRecords[0]->id, $registrantRecord->id);
        $this->assertEquals($registrantRecord->registrant_id, $registrant->id);
    }

    /**
     * Test that we can get the ClientType relation correctly
     *
     * @group Unit
     * @group Models
     * @group Registrant
     */
    public function testClientTypeRelation()
    {
        $registrant = factory(Registrant::class)->create();
        $clientType = ClientType::find($registrant->client_type_id);

        $this->assertEquals($registrant->clientType->id, $clientType->id);
        $this->assertInstanceOf(ClientType::class, $registrant->clientType);
    }

    /**
     * Test that the activeRecord method returns only the active record
     *
     * @group Unit
     * @group Models
     * @group Registrant
     * @group RegistrantRecord
     */
    public function testActiveRecord()
    {
        $record = factory(App\Models\RegistrantRecord::class)->states(['active'])->create();
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords()->active()->get());
        $this->assertCount(0, Registrant::find($record->registrant_id)->registrantRecords()->archived()->get());

        factory(App\Models\RegistrantRecord::class,3)->states(['active'])->create(['registrant_id' => $record->registrant_id]);
        $this->assertCount(4, Registrant::find($record->registrant_id)->registrantRecords);
        $this->assertCount(1, Registrant::find($record->registrant_id)->registrantRecords()->active()->get());
        $this->assertCount(3, Registrant::find($record->registrant_id)->registrantRecords()->archived()->get());

        $reg = Registrant::find($record->registrant_id);
        $actual = $reg->activeRecord();
        $this->assertInstanceOf(App\Models\RegistrantRecord::class, $actual);

    }

    /**
     * Test that the isSubscriber method works as expected
     *
     * @group Unit
     * @group Models
     * @group Registrant
     */
    public function testIsSubscriber()
    {
        $actual = [
            1 => factory(Registrant::class)->states('subscriber')->create(),
            2 => factory(Registrant::class)->create(['subscriber' => false])
        ];
        $this->assertTrue($actual[1]->isSubscriber());
        $this->assertFalse($actual[2]->isSubscriber());
    }




}
