<?php

namespace Tests\Unit\Models;

use TestCase;
use App\Models\ {PasswordResets, User, Admin};
use Illuminate\Support\Carbon;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Enums\PasswordResetStatusEnum;

class PasswordResetsTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we can get the User relation correctly
     *
     * @group Unit
     * @group Models
     * @group PasswordResets
     */
    public function testUserRelation()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        $reset = PasswordResets::create($data);

        $this->assertInstanceOf(User::class, $reset->user);
        $this->assertEquals($reset->user_id, $user->id);
    }

    /**
     * Test that we can get the Admin relation correctly
     *
     * @group Unit
     * @group Models
     * @group PasswordResets
     */
    public function testAdminRelation()
    {
        $user   = factory(User::class)->create();
        $admin  = factory(Admin::class)->create();
        $data   = [
            'user_id' => $user->id,
            'admin_id' => $admin->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        $reset = PasswordResets::create($data);

        $this->assertInstanceOf(Admin::class, $reset->admin);
        $this->assertEquals($reset->admin_id, $admin->id);
    }


    /**
     * Test that we can the default expire minutes of reset-record
     *
     * @group Unit
     * @group Models
     * @group PasswordResets
     */
    public function testSetExpireMinutes()
    {
        PasswordResets::expireMinutes(33);
        $this->assertEquals(33, PasswordResets::$expireMinutes);
        PasswordResets::expireMinutes(25);
        $this->assertEquals(25, PasswordResets::$expireMinutes);
    }

    /**
     * Test that we can create reset record with the default expire minutes
     *
     * @group Unit
     * @group Models
     * @group PasswordResets
     */
    public function testCreateWithExpireMinutes()
    {
        $user = factory(User::class)->create();

        $now = Carbon::now();
        Carbon::setTestNow($now);

        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
            'status_id' => PasswordResetStatusEnum::PENDING,
        ];

        PasswordResets::expireMinutes(22);
        $reset = PasswordResets::create($data);
        $expected = Carbon::now()->addMinutes(22)->toDateTimeString();
        $this->assertEquals($expected, Carbon::parse($reset->expires_at)->toDateTimeString());
    }

    /**
     * Test that the isExpired method works as expected
     *
     * @group Unit
     * @group Models
     * @group PasswordResets
     */
    public function testIsExpired()
    {
        $user = factory(User::class)->create();

        $now = Carbon::now();
        Carbon::setTestNow($now);

        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
            'status_id' => PasswordResetStatusEnum::PENDING,
        ];
        PasswordResets::expireMinutes(22);
        $reset = PasswordResets::create($data);
        $this->assertFalse($reset->isExpired());

        PasswordResets::expireMinutes(-30);
        $reset = PasswordResets::create($data);
        $this->assertTrue($reset->isExpired());
    }
}
