<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\ {UserType, ClientType, User};
use Ramsey\Uuid\Uuid;

class UserTypeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we get all UserTypes correctly
     *
     * @group Unit
     * @group Models
     * @group UserType
     * @group SystemOption
     */
    public function testGetsUserType()
    {
        $userTypes = UserType::all();
        $this->assertInstanceOf(UserType::class, $userTypes->first());
    }

    /**
     * Test that we can get the User relation correctly
     *
     * @group Unit
     * @group Models
     * @group UserType     
     * @group SystemOption
     */
    public function testUserRelation()
    {
        $uuid = Uuid::uuid4()->toString();
        $users = [
            'reg' => factory(User::class)->states(['registrant', 'enabled'])->create(['email' => $uuid.'-reg@foobar.io']),
            'adm' => factory(User::class)->states(['registrant', 'enabled'])->create([
                'email' => $uuid.'-adm@foobar.io',
                'type_id' => UserType::byCode(UserType::ADMIN_CODE)->id
                ]),
        ];
        $types   = [
            'reg' => UserType::where('code', UserType::REGISTRANT_CODE)->first(),
            'adm' => UserType::where('code', UserType::ADMIN_CODE)->first(),
        ];

        foreach($types AS $key => $type) {
            $actual = $type->users->where('email', '=', $users[$key]->email);
            $this->assertCount(1, $actual);
            $this->assertInstanceOf(User::class, $actual->first());
        }
    }
    
    
    /**
     * Tests that the query scope 'byCode' returns the expected value
     * 
     * @group Unit
     * @group Models
     * @group UserType
     * @group SystemOption
     */
    public function testScopeByCode()
    {
        $expected = [
            1 => UserType::find(1),
            2 => UserType::find(2)
        ];
        
        foreach($expected AS $id => $type) {
            $actual = UserType::byCode($type->code);
            $this->assertInstanceOf(UserType::class, $actual, "Failed to assert returned type is of expected class: ". UserType::class);
            $this->assertEquals($type->code, $actual->code, "Failed to assert that returned property: 'code' matches expected value for id: {$id}");
            $this->assertEquals($type->name, $actual->name, "Failed to assert that returned property: 'name' matches expected value for id {$id}");
        }
    }
    
    /**
     * Test that the query scope 'byCode' fails as expected when provided a non-existent type-code.
     * 
     * @group Unit
     * @group Models
     * @group UserType
     * @group SystemOption
     */
    public function testScopeByCodeFailure()
    {
        $badCode = "foobar";
        $this->missingFromDatabase('so_user_types', ['code' => $badCode]);
        $this->expectException(InvalidArgumentException::class);
        UserType::byCode($badCode);
    }

    /**
     * Test that the SystemOptionBase::getNameAttribute translates as expected.
     * 
     * @group Unit
     * @group Models
     * @group UserType
     * @group SystemOption
     * @group Localization
     */
    public function testSystemOptionBaseTranslation()
    {
        foreach(App\Facades\Localization::getAvailable() AS $key => $lang) {
            App\Facades\Localization::setLanguage($key);
            $type = UserType::all()->random();
            $expected = trans("sysops.so_user_types.".$type->code);
            $actual   = UserType::byCode($type->code)->name;
            $this->assertEquals($expected, $actual, "Failed to assert that UserType::setNameAttribute translated correctly to {$key}:{$lang}");
        }
    }
   
}
