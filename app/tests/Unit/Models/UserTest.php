<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\Registrant;
use App\Models\RegistrantRecord;
use App\Models\User;
use App\Models\UserType;
use App\Models\ClientType;
use Ramsey\Uuid\Uuid;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we get a User correctly
     *
     * @group Unit
     * @group Models
     * @group User
     */
    public function testGetUser()
    {
        $user = factory(App\Models\User::class)->states(['enabled', 'registrant'])->create();
        $expected = User::find($user->id);
        $this->assertEquals($expected->id, $user->id);
    }


    /**
     * Test that we can get the UserType relation correctly
     *
     * @group Unit
     * @group Models
     * @group User
     * @group UserType
     */
    public function testUserTypeRelation()
    {
        $list = [
            1 => factory(User::class)->states(['registrant', 'enabled'])->create(),
            2 => factory(User::class)->states(['registrant', 'enabled'])->create([
                    'type_id' => UserType::byCode(UserType::ADMIN_CODE)->id
                ]),
        ];

        foreach($list AS $expected => $user) {
            $actual = $user->userType;
            $this->assertInstanceOf(UserType::class, $actual);
            $this->assertEquals($expected, $actual->id);
        }
    }

    /**
     * Test the isRegistrant method works as expected
     *
     * @group Unit
     * @group Models
     * @group User
     * @group UserType
     */
    public function testIsRegistrant()
    {
        $list = [
            UserType::REGISTRANT_CODE   => factory(User::class)->states(['registrant', 'enabled'])->create(),
            UserType::ADMIN_CODE        => factory(User::class)->states(['registrant', 'enabled'])->create([
                    'type_id' => UserType::byCode(UserType::ADMIN_CODE)->id
                ]),
        ];

        $this->assertTrue($list[UserType::REGISTRANT_CODE]->isRegistrant());
        $this->assertFalse($list[UserType::ADMIN_CODE]->isRegistrant());
    }

    /**
     * Test the getEncryptedIdentifier method works as expected
     *
     * @group Unit
     * @group Models
     * @group User
     * @group Encryption
     */
    public function testGetEncryptedIdentifier()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $expected = [
            'id' => $user->id,
            'user_name' => $user->user_name
        ];
        $dec = decrypt($user->getEncryptedIdentifier());
        $this->assertEquals($dec, $expected);
    }


    /**
     * Test that the admin relation works correctly
     *
     * @group Unit
     * @group Models
     * @group User
     * @group Admin
     */
    public function testIsAdmin()
    {
        $list = [
            0 => factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create(),
            1 => factory(\App\Models\Admin::class)->states('staff')->create()->userRecord
        ];
        foreach($list AS $i => $user) {
            $this->assertEquals((bool)$i, $user->isAdmin());
        }
    }

    /**
     * Test that the base-model setUuidAttribute attribute works as expected
     *
     * @group Unit
     * @group Models
     * @group User
     */
    public function testSetUuidAttribute()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $uuid = strtolower(Uuid::uuid4()->toString());
        $user->uuid = $uuid;
        $this->assertNotEquals($user->uuid, $uuid);
        $this->assertEquals($user->uuid, strtoupper($uuid));
    }

}
