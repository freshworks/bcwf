<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\OrganType;

class OrganTypeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we get an OrganType correctly
     *
     * @group Unit
     * @group Models
     * @group OrganType
     * @group SystemOption
     */
    public function testGetsOrganType()
    {
        $organType = OrganType::find(1);
        $this->assertEquals($organType->name, 'Blood Vessels');
    }

    /**
     * Tests that the query scope 'byCode' returns the expected value
     * 
     * @group Unit
     * @group Models
     * @group OrganType
     * @group SystemOption
     */
    public function testScopeByCode()
    {
        $expected = [
            1 => OrganType::find(1),
            2 => OrganType::find(2)
        ];
        
        foreach($expected AS $id => $type) {
            $actual = OrganType::byCode($type->code);
            $this->assertInstanceOf(OrganType::class, $actual, "Failed to assert returned type is of expected class: ". OrganType::class);
            $this->assertEquals($type->code, $actual->code, "Failed to assert that returned property: 'code' matches expected value for id: {$id}");
            $this->assertEquals($type->name, $actual->name, "Failed to assert that returned property: 'name' matches expected value for id {$id}");
        }
    }
    
    /**
     * Test that the query scope 'byCode' fails as expected when provided a non-existent type-code.
     * 
     * @group Unit
     * @group Models
     * @group OrganType
     * @group SystemOption
     */
    public function testScopeByCodeFailure()
    {
        $badCode = "foobar";
        $this->missingFromDatabase('so_organ_types', ['code' => $badCode]);
        $this->expectException(InvalidArgumentException::class);
        OrganType::byCode($badCode);
    }
    
    /**
     * Test that the SystemOptionBase::getNameAttribute translates as expected.
     * 
     * @group Unit
     * @group Models
     * @group OrganType
     * @group SystemOption
     * @group Localization
     */
    public function testSystemOptionBaseTranslation()
    {
        foreach(App\Facades\Localization::getAvailable() AS $key => $lang) {
            App\Facades\Localization::setLanguage($key);
            $type = OrganType::all()->random();
            $expected = trans("sysops.so_organ_types.".$type->code);
            $actual   = OrganType::byCode($type->code)->name;
            $this->assertEquals($expected, $actual, "Failed to assert that OrganType::setNameAttribute translated correctly to {$key}:{$lang}");
        }
    }
    
}