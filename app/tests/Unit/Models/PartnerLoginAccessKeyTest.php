<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\ {PartnerLoginAccessKey, Registrant};
use Illuminate\Support\Carbon;
use App\Enums\AccessKeyEnum;

class PartnerLoginAccessKeyTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that the registrant relation works as expected
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     * @group Registrant
     */
    public function testRegistrantRelation()
    {
        $record = factory(PartnerLoginAccessKey::class)->states('active')->create();
        $this->assertInstanceOf(Registrant::class, $record->registrant);
    }

    /**
     * Test that the scopeByKey method works as expected
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testScopeByKey()
    {
        $rec = factory(PartnerLoginAccessKey::class)->create();
        $record = PartnerLoginAccessKey::byKey($rec->key);
        $this->assertEquals($record->id, $rec->id);
    }

    /**
     * Test that the scopeByKey fails with the correct exception
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testScopeByKeyFails()
    {
        $key = strtoupper(\Uuid::uuid4()->toString());
        $this->missingFromDatabase('partner_login_access_keys', ['key' => $key]);
        $this->expectException(\InvalidArgumentException::class);
        $record = PartnerLoginAccessKey::byKey($key);
    }

    /**
     * Test that the scopeByValidKey works as expected
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testScopeByValidKey()
    {
        $rec = factory(PartnerLoginAccessKey::class)->create();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $record = PartnerLoginAccessKey::byKey($rec->key);
        $this->assertEquals($record->id, $rec->id);
    }

    /**
     * Test that the scopeByValidKey fails with the correct exception
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testScopeByValidKeyFails()
    {
        $key = strtoupper(\Uuid::uuid4()->toString());
        $this->missingFromDatabase('partner_login_access_keys', ['key' => $key]);
        $record = PartnerLoginAccessKey::byValidKey($key);
        $this->assertEquals($record->count(), 0);

        $rec = factory(PartnerLoginAccessKey::class)->states('used')->create();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_USED);
        $record = PartnerLoginAccessKey::byValidKey($rec->key);
        $this->assertEquals($record->count(), 0);
    }

    /**
     * Test that the tap() method correctly sets the status of the record
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testTap()
    {
        $rec = factory(PartnerLoginAccessKey::class)->create();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_ACTIVE);

        $rec->tap();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_USED);
        $rec = PartnerLoginAccessKey::byKey($rec->key);
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_USED);
    }

    /**
     * Test the isActive method correctly identifies active records
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testIsActive()
    {
        $rec = factory(PartnerLoginAccessKey::class)->create();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $this->assertTrue($rec->isActive());

        $rec->tap();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_USED);
        $this->assertFalse($rec->isActive());
    }

    /**
     * Test the isUsed method correctly identifies used records
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testIsUsed()
    {
        $rec = factory(PartnerLoginAccessKey::class)->create();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $this->assertFalse($rec->isUsed());

        $rec->status_id = AccessKeyEnum::STATUS_USED;
        $rec->save();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_USED);
        $this->assertTrue($rec->isUsed());
    }

    /**
     * Test the isExpired method correctly identifies expired records
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testIsExpired()
    {
        $rec = factory(PartnerLoginAccessKey::class)->create();
        $this->assertFalse(Carbon::parse($rec->created_at)->addMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertFalse($rec->isExpired());

        $rec->created_at = Carbon::parse($rec->created_at)->subMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES + 10)->timestamp;
        $rec->save();
        $this->assertTrue(Carbon::parse($rec->created_at)->addMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertTrue($rec->isExpired());
    }

    /**
     * Test the isValid method correctly identifies valid records
     *
     * @group Unit
     * @group Models
     * @group PartnerLoginAccessKey
     */
    public function testIsValid()
    {
        $rec = factory(PartnerLoginAccessKey::class)->create();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $this->assertFalse(Carbon::parse($rec->created_at)->addMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertTrue($rec->isValid());

        $rec = factory(PartnerLoginAccessKey::class)->create(['status_id' => AccessKeyEnum::STATUS_USED]);
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_USED);
        $this->assertFalse(Carbon::parse($rec->created_at)->addMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertFalse($rec->isValid());

        $rec->created_at = Carbon::parse($rec->created_at)->subMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES + 10)->timestamp;
        $rec->save();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_USED);
        $this->assertTrue(Carbon::parse($rec->created_at)->addMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertFalse($rec->isValid());

        $rec->status_id = AccessKeyEnum::STATUS_ACTIVE;
        $rec->save();
        $this->assertEquals($rec->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $this->assertTrue(Carbon::parse($rec->created_at)->addMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertFalse($rec->isValid());

    }

}