<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\ {Admin, User, Group, Province};

class GroupTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we get a Group record correctly
     *
     * @group Unit
     * @group Models
     * @group Group
     */
    public function testGetsGroup()
    {
        $group = factory(Group::class)->create();
        $expected = Group::find($group->id);
        $this->assertEquals($expected->id, $group->id, "Failed to assert that Group Models are able to fetch results");
    }

    /**
     * Test that the province relation works correctly
     *
     * @group Unit
     * @group Models
     * @group Group
     */
    public function testProvinceRelation()
    {
        $province = Province::all()->random();
        $group = factory(Group::class)->create(['province_id' => $province->id]);
        $this->assertEquals($group->province->id, $province->id);
        $this->assertInstanceOf(Province::class, $group->province);
    }

    /**
     * Test that the admins releation works correctly
     *
     * @group Unit
     * @group Models
     * @group Unit
     * @group Admin
     */
    public function testAdminRelation()
    {
        $createCount = rand(2,9);
        $group = factory(Group::class)->create();
        $admins = factory(Admin::class, $createCount)->states('staff')->create(['group_id' => $group->id]);
        $this->assertCount($createCount, $group->admins);
        $this->assertInstanceOf(Admin::class, $group->admins()->first());
    }
}
