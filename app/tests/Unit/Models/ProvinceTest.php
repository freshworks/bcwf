<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\Province;
use App\Models\RegistrantRecord;

class ProvinceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we get all Provinces correctly
     *
     * @group Unit
     * @group Models
     * @group Province
     * @group SystemOption
     */
    public function testGetsProvinces()
    {
        $provinces = Province::all();
        $this->assertEquals($provinces->count(), 13);
    }

    /**
     * Test that we can get the RegistrantRecord relation correctly
     *
     * @group Unit
     * @group Models
     * @group Province
     * @group SystemOption
     */
    public function testRegistrantRecordRelation()
    {
        $registrantRecord = factory(RegistrantRecord::class)->create([
            'province_id' => 1,
        ]);
        $province = Province::find($registrantRecord->province_id);

        $this->assertEquals($province->registrantRecords->first()->province_id, $registrantRecord->province_id);
        $this->assertInstanceOf(RegistrantRecord::class, $province->registrantRecords->first());
    }

    /**
     * Tests that the query scope 'byCode' returns the expected value
     * 
     * @group Unit
     * @group Models
     * @group Province
     * @group SystemOption
     */
    public function testScopeByCode()
    {
        $expected = [
            1 => Province::find(1),
            2 => Province::find(2)
        ];
        
        foreach($expected AS $id => $type) {
            $actual = Province::byCode($type->code);
            $this->assertInstanceOf(Province::class, $actual, "Failed to assert returned type is of expected class: ". Province::class);
            $this->assertEquals($type->code, $actual->code, "Failed to assert that returned property: 'code' matches expected value for id: {$id}");
            $this->assertEquals($type->name, $actual->name, "Failed to assert that returned property: 'name' matches expected value for id {$id}");
        }
    }
    
    /**
     * Test that the query scope 'byCode' fails as expected when provided a non-existent type-code.
     * 
     * @group Unit
     * @group Models
     * @group Province
     * @group SystemOption
     */
    public function testScopeByCodeFailure()
    {
        $badCode = "foobar";
        $this->missingFromDatabase('so_provinces', ['code' => $badCode]);
        $this->expectException(InvalidArgumentException::class);
        Province::byCode($badCode);
    }
    
    /**
     * Test that the SystemOptionBase::getNameAttribute translates as expected.
     * 
     * @group Unit
     * @group Models
     * @group Province
     * @group SystemOption
     * @group Localization
     */
    public function testSystemOptionBaseTranslation()
    {
        foreach(App\Facades\Localization::getAvailable() AS $key => $lang) {
            App\Facades\Localization::setLanguage($key);
            $type = Province::all()->random();
            $expected = trans("sysops.so_provinces.".$type->code);
            $actual   = Province::byCode($type->code)->name;
            $this->assertEquals($expected, $actual, "Failed to assert that Province::setNameAttribute translated correctly to {$key}:{$lang}");
        }
    }
   
}
