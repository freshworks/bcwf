<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\ClientType;
use App\Models\Registrant;
use Illuminate\Support\Carbon;

class ClientTypeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that we get all ClientTypes correctly
     *
     * @group Unit
     * @group Models
     * @group ClientType
     */
    public function testGetsClientType()
    {
        $clientTypes = ClientType::all();
        $this->assertEquals($clientTypes->count(), 3);
    }

    /**
     * Test that we can get the Registrant relation correctly
     *
     * @group Unit
     * @group Models
     * @group ClientType
     */
    public function testRegistrantRelation()
    {
        $now = Carbon::now();
        Carbon::setTestNow($now);

        $reg = [
            1 => factory(Registrant::class, 1)->create(['client_type_id' => 1]),
            2 => factory(Registrant::class, 5)->create(['client_type_id' => 2]),
        ];
        $types = [
            1 => ['obj' => ClientType::find(1), 'cnt' => 1],
            2 => ['obj' => ClientType::find(2), 'cnt' => 5]
        ];

        foreach($types AS $key => $obj) {
            $actual = $obj['obj']->registrants->where('created_at', '=', $now);
            $this->assertCount($obj['cnt'], $actual, "Failed asserting registrant relation count on ClientType model.");
            $this->assertInstanceOf(Registrant::class, $actual->first(), "Failed asserting registrant relation return type on ClientType model.");
            $this->assertEquals($actual->first()->client_type_id, $key, "Failed asserting registrant relation return value on ClientType model.");
        }

    }
}
