<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\RegistrationAccessKey;
use Illuminate\Support\Carbon;
use App\Enums\AccessKeyEnum;

class RegistrationAccessKeyTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that the scopeByKey method works as expected
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testScopeByKey()
    {
        $reg = factory(RegistrationAccessKey::class)->create();
        $record = RegistrationAccessKey::byKey($reg->key);
        $this->assertEquals($record->id, $reg->id);
    }

    /**
     * Test that the scopeByKey fails with the correct exception
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testScopeByKeyFails()
    {
        $key = strtoupper(\Uuid::uuid4()->toString());
        $this->missingFromDatabase('registration_access_keys', ['key' => $key]);
        $this->expectException(\InvalidArgumentException::class);
        $record = RegistrationAccessKey::byKey($key);
    }

    /**
     * Test that the scopeByValidKey works as expected
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testScopeByValidKey()
    {
        $reg = factory(RegistrationAccessKey::class)->create();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $record = RegistrationAccessKey::byKey($reg->key);
        $this->assertEquals($record->id, $reg->id);
    }

    /**
     * Test that the scopeByValidKey fails with the correct exception
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testScopeByValidKeyFails()
    {
        $key = strtoupper(\Uuid::uuid4()->toString());
        $this->missingFromDatabase('registration_access_keys', ['key' => $key]);
        $record = RegistrationAccessKey::byValidKey($key);
        $this->assertEquals($record->count(), 0);

        $reg = factory(RegistrationAccessKey::class)->states('used')->create();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_USED);
        $record = RegistrationAccessKey::byValidKey($reg->key);
        $this->assertEquals($record->count(), 0);
    }

    /**
     * Test that the tap() method correctly sets the status of the record
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testTap()
    {
        $reg = factory(RegistrationAccessKey::class)->create();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_ACTIVE);

        $reg->tap();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_USED);
        $reg = RegistrationAccessKey::byKey($reg->key);
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_USED);
    }

    /**
     * Test the isActive method correctly identifies active records
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testIsActive()
    {
        $reg = factory(RegistrationAccessKey::class)->create();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $this->assertTrue($reg->isActive());

        $reg->tap();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_USED);
        $this->assertFalse($reg->isActive());
    }

    /**
     * Test the isUsed method correctly identifies used records
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testIsUsed()
    {
        $reg = factory(RegistrationAccessKey::class)->create();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $this->assertFalse($reg->isUsed());

        $reg->status_id = AccessKeyEnum::STATUS_USED;
        $reg->save();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_USED);
        $this->assertTrue($reg->isUsed());
    }

    /**
     * Test the isExpired method correctly identifies expired records
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testIsExpired()
    {
        $reg = factory(RegistrationAccessKey::class)->create();
        $this->assertFalse( Carbon::parse($reg->created_at)->addMinutes(AccessKeyEnum::REGISTRATION_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertFalse( $reg->isExpired() );

        $reg->created_at = Carbon::parse($reg->created_at)->subMinutes(AccessKeyEnum::REGISTRATION_ACCESS_EXPIRES_MINUTES+10)->timestamp;
        $reg->save();
        $this->assertTrue( Carbon::parse($reg->created_at)->addMinutes(AccessKeyEnum::REGISTRATION_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertTrue( $reg->isExpired() );
    }

    /**
     * Test the isValid method correctly identifies valid records
     *
     * @group Unit
     * @group Models
     * @group RegistrationAccessKey
     */
    public function testIsValid()
    {
        $reg = factory(RegistrationAccessKey::class)->create();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $this->assertFalse( Carbon::parse($reg->created_at)->addMinutes(AccessKeyEnum::REGISTRATION_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertTrue( $reg->isValid() );

        $reg = factory(RegistrationAccessKey::class)->create(['status_id' => AccessKeyEnum::STATUS_USED]);
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_USED);
        $this->assertFalse( Carbon::parse($reg->created_at)->addMinutes(AccessKeyEnum::REGISTRATION_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertFalse( $reg->isValid() );

        $reg->created_at = Carbon::parse($reg->created_at)->subMinutes(AccessKeyEnum::REGISTRATION_ACCESS_EXPIRES_MINUTES+10)->timestamp;
        $reg->save();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_USED);
        $this->assertTrue( Carbon::parse($reg->created_at)->addMinutes(AccessKeyEnum::REGISTRATION_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertFalse( $reg->isValid() );

        $reg->status_id = AccessKeyEnum::STATUS_ACTIVE;
        $reg->save();
        $this->assertEquals($reg->status_id, AccessKeyEnum::STATUS_ACTIVE);
        $this->assertTrue( Carbon::parse($reg->created_at)->addMinutes(AccessKeyEnum::REGISTRATION_ACCESS_EXPIRES_MINUTES)->isPast());
        $this->assertFalse( $reg->isValid() );

    }

}