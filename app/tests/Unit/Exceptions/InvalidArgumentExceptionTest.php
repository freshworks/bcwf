<?php

use App\Exceptions\Util\InvalidArgumentException;

class InvalidArgumentExceptionTest extends TestCase
{
    /**
     * Test that an exception is thrown when calling assertCallback with an invalid $callback type
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertCallbackIsNotAValidType()
    {
        $callback = 123;
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expected parameter %d to be a valid callback, no array, string, closure or functor given',
            $callee,
            $parameterPosition
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertCallback($callback, $callee, $parameterPosition);
    }

     /**
     * Test that an exception is thrown when calling assertCallback with an array $callback containing an object
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertCallbackIsAnArray()
    {
        $callback = [
            new InvalidArgumentException()
        ];
        $callee = 'test';
        $parameterPosition = 1;
        $type = 'method';
        $callbackCopy = array_values(array_replace([], $callback));
        $callbackCopy[0] = get_class($callbackCopy[0]);
        $expectedCallback = join($callbackCopy, '->');
        $expected = sprintf(
            "%s() expects parameter %d to be a valid callback, %s '%s' not found or invalid %s name",
            $callee,
            $parameterPosition,
            $type,
            $expectedCallback,
            $type
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertCallback($callback, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertCallback with a string $callback
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertCallbackIsAString()
    {
        $callback = 'example';
        $callee = 'test';
        $parameterPosition = 1;
        $type = 'function';
        $expected = sprintf(
            "%s() expects parameter %d to be a valid callback, %s '%s' not found or invalid %s name",
            $callee,
            $parameterPosition,
            $type,
            $callback,
            $type
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertCallback($callback, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertMethodName with an invalid $methodName type
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertMethodName()
    {
        $methodName = 123;
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be string, %s given',
            $callee,
            $parameterPosition,
            gettype($methodName)
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertMethodName($methodName, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertPropertyName with an invalid $propertyName type
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertPropertyName()
    {
        $propertyName = [];
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be a valid property name or array index, %s given',
            $callee,
            $parameterPosition,
            gettype($propertyName)
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertPropertyName($propertyName, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertPositiveInteger with a negative integer
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertPositiveInteger()
    {
        $value = -1;
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be positive integer, %s given',
            $callee,
            $parameterPosition,
            'negative integer'
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertPositiveInteger($value, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertArrayKeyExists with an invalid array $key type
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertValidArrayKey()
    {
        $key = [];
        $callee = 'test';
        $keyTypes = ['NULL', 'string', 'integer', 'double', 'boolean'];
        $keyType = gettype($key);
        $expected = sprintf(
            '%s(): callback returned invalid array key of type "%s". Expected %4$s or %3$s',
            $callee,
            $keyType,
            array_pop($keyTypes),
            join(', ', $keyTypes)
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertValidArrayKey($key, $callee);
    }

    /**
     * Test that an exception is thrown when calling assertArrayKeyExists with a non-existent $key in $collection
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertArrayKeyExists()
    {
        $collection = ['example'];
        $key = 'not_example';
        $callee = 'test';
        $expected = sprintf(
            '%s(): unknown key "%s"',
            $callee,
            $key
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertArrayKeyExists($collection, $key, $callee);
    }

    /**
     * Test that an exception is thrown when calling assertBoolean and $value is not a boolean
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertBoolean()
    {
        $value = 'Not a boolean';
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be boolean',
            $callee,
            $parameterPosition
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertBoolean($value, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertInteger and $value is not an integer
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertInteger()
    {
        $value = '1';
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be integer',
            $callee,
            $parameterPosition
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertInteger($value, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertIntegerGreaterThanOrEqual with a $value that is less than $limit
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertIntegerGreaterThanOrEqual()
    {
        $value = 1;
        $limit = 2;
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be an integer greater than or equal to %d',
            $callee,
            $parameterPosition,
            $limit
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertIntegerGreaterThanOrEqual($value, $limit, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertIntegerGreaterThanOrEqual and $value is not an integer
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertIntegerGreaterThanOrEqualAndValueIsNotAnInteger()
    {
        $value = '1';
        $limit = 2;
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be an integer greater than or equal to %d',
            $callee,
            $parameterPosition,
            $limit
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertIntegerGreaterThanOrEqual($value, $limit, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertIntegerLessThanOrEqual with a $value that is greater than $limit
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertIntegerLessThanOrEqual()
    {
        $value = 2;
        $limit = 1;
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be an integer less than or equal to %d',
            $callee,
            $parameterPosition,
            $limit
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertIntegerLessThanOrEqual($value, $limit, $callee, $parameterPosition);
    }

    /**
     * Test that an exception is thrown when calling assertIntegerLessThanOrEqual and $value is not an integer
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertIntegerLessThanOrEqualAndValueIsNotAnInteger()
    {
        $value = '2';
        $limit = 1;
        $callee = 'test';
        $parameterPosition = 1;
        $expected = sprintf(
            '%s() expects parameter %d to be an integer less than or equal to %d',
            $callee,
            $parameterPosition,
            $limit
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertIntegerLessThanOrEqual($value, $limit, $callee, $parameterPosition);
    }

     /**
     * Test that an exception is thrown when calling assertResolvablePlaceholder with an empty $args array
     *
     * @group Unit
     * @group Exceptions
     */
    public function testAssertResolvablePlaceholder()
    {
        $args = [];
        $position = 1;
        $expected = sprintf('Cannot resolve parameter placeholder at position %d. Parameter stack is empty.', $position);

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($expected);

        InvalidArgumentException::assertResolvablePlaceholder($args, $position);
    }
}
