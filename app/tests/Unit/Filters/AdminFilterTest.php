<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;
use App\Models\ {Admin, Group};
use App\Filters\AdminFilter;
use App\Enums\UserStatusEnum;


class AdminFilterTest extends TestCase
{
    use DatabaseTransactions;

    private $filter;

    /**
     * @inheritdoc
     */
    public function setUp() {
        parent::setUp();

        $request = $this->createMock(Request::class);

        $filter = new AdminFilter($request);

        $queryBuilder = new QueryBuilder($this->app->make('db')->connection());
        $builder = new Builder($queryBuilder);
        $builder->setModel(new Admin);

        $property = (new \ReflectionClass(AdminFilter::class))->getProperty('builder');
        $property->setAccessible(true);
        $property->setValue($filter, $builder);

        $this->filter = $filter;
    }

    /**
     * Test that the byGroupId method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByGroupId()
    {
        $group = factory(Group::class)->create();
        $admin = factory(Admin::class)->create(['group_id' => $group->id]);

        $result = $this->filter->byGroupId($group->id)->first();

        $this->assertEquals($admin->first_name, $result->first_name);
        $this->assertEquals($admin->id, $result->id);
        $this->assertEquals($group->id, $result->group_id);
    }

    /**
     * Test that the bySearchTerm method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testBySearchTerm()
    {
        $group = factory(Group::class)->create();
        $admin = factory(Admin::class)->create(['group_id' => $group->id]);

        $searchTerm = $admin->first_name;
        $searchTerm2 = $admin->last_name;
        $searchTerm3 = $admin->first_name . ' ' . $admin->last_name;
        $searchTerm4 = $admin->email;
        $searchTerm5 = $group->name;

        $result = $this->filter->bySearchTerm($searchTerm)->first();
        $result2 = $this->filter->bySearchTerm($searchTerm2)->first();
        $result3 = $this->filter->bySearchTerm($searchTerm3)->first();
        $result4 = $this->filter->bySearchTerm($searchTerm4)->first();
        $result5 = $this->filter->bySearchTerm($searchTerm5)->first();

        $this->assertEquals($admin->first_name, $result->first_name);
        $this->assertEquals($admin->last_name, $result2->last_name);
        $this->assertEquals(
            $admin->first_name . ' ' . $admin->last_name, 
            $result3->first_name . ' ' . $result3->last_name
        );
        $this->assertEquals($admin->email, $result4->email);
        $this->assertEquals($group->id, $result5->group_id);
    }

    /**
     * Test passing false into byShowAll method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByShowAllIsFalse()
    {
        $group = factory(Group::class)->create();
        $activeAdmins = factory(Admin::class, rand(1, 3))->create(['group_id' => $group->id]);
        $pendingAdmins = factory(Admin::class, rand(1, 3))->create(['group_id' => $group->id]);
        $inactiveAdmins = factory(Admin::class, rand(1, 3))->create(['group_id' => $group->id]);
        
        // Statuses not updated..
        foreach ($pendingAdmins as $admin) {
            $admin->userRecord->status_id = UserStatusEnum::PENDING;
            $admin->userRecord->save();
        }
        foreach ($inactiveAdmins as $admin) {
            $admin->userRecord->status_id = UserStatusEnum::DISABLED;
            $admin->userRecord->save();
        }

        $showAll  = false;
        $expected = count($activeAdmins);
        $result   = $this->filter->byShowAll($showAll)->where('group_id', '=', $group->id)->get()->toArray();

        $this->assertCount($expected, $result);
    }

    /**
     * Test passing true into byShowAll method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testShowAllIsTrue()
    {
        $group = factory(Group::class)->create();
        $activeAdmins = factory(Admin::class, rand(1, 3))->create(['group_id' => $group->id]);
        $pendingAdmins = factory(Admin::class, rand(1, 3))->create(['group_id' => $group->id]);
        $inactiveAdmins = factory(Admin::class, rand(1, 3))->create(['group_id' => $group->id]);
        
        // Statuses not updated..
        foreach ($pendingAdmins as $admin) {
            $admin->userRecord->status_id = UserStatusEnum::PENDING;
            $admin->userRecord->save();
        }
        foreach ($inactiveAdmins as $admin) {
            $admin->userRecord->status_id = UserStatusEnum::DISABLED;
            $admin->userRecord->save();
        }

        $showAll  = true;
        $expected = count($activeAdmins) + count($pendingAdmins) + count($inactiveAdmins);
        $result   = $this->filter->byShowAll($showAll)->where('group_id', '=', $group->id)->get()->toArray();

        $this->assertCount($expected, $result);
    }
   
}
