<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Models\RegistrantRecord;
use App\Filters\RegistrantRecordFilter;


class RegistrantRecordFilterTest extends TestCase
{
    use DatabaseTransactions;

    private $filter;

    /**
     * @inheritdoc
     */
    public function setUp() {
        parent::setUp();
        
        $request = $this->createMock(Request::class);
        
        $filter = new RegistrantRecordFilter($request);

        $builder = new Builder($this->app->make('db')->connection());
        $builder->from('registrant_records');
        
        $property = (new \ReflectionClass(RegistrantRecordFilter::class))->getProperty('builder');
        $property->setAccessible(true);
        $property->setValue($filter, $builder);
        
        $this->filter = $filter;
    }

    /**
     * Test that the byFirstName method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByFirstName()
    {
        $firstName = $this->faker->firstName();
        $registrantRecord = factory(RegistrantRecord::class)->create(['first_name' => $firstName]);

        $result = $this->filter->byFirstName($firstName)->first();

        $this->assertEquals($registrantRecord->first_name, $result->first_name);
    }

    /**
     * Test that the byLastName method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByLastName()
    {
        $lastName = $this->faker->lastName();
        $registrantRecord = factory(RegistrantRecord::class)->create(['last_name' => $lastName]);

        $result = $this->filter->bylastName($lastName)->first();

        $this->assertEquals($registrantRecord->last_name, $result->last_name);
    }

    /**
     * Test that the bySex method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testBySex()
    {
        $sex = 'male';
        $registrantRecord = factory(RegistrantRecord::class)->create(['sex' => $sex]);

        $result = $this->filter->bySex($sex)->first();

        $this->assertEquals(strtolower($registrantRecord->sex), $result->sex);
    }

    /**
     * Test that the byProvinceId method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByProvinceId()
    {
        $province = 1;
        $registrantRecord = factory(RegistrantRecord::class)->create(['province_id' => $province]);

        $result = $this->filter->byProvinceId($province)->first();

        $this->assertEquals($registrantRecord->province_id, $result->province_id);
    }

    /**
     * Test that the byDateOfBirth method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByDateOfBirth()
    {
        $dateOfBirth = $this->faker->date();
        $registrantRecord = factory(RegistrantRecord::class)->create(['date_of_birth' => $dateOfBirth]);

        $result = $this->filter->byDateOfBirth($dateOfBirth)->first();

        $this->assertEquals($registrantRecord->date_of_birth, $result->date_of_birth);
    }

    /**
     * Test that the byHealthCare method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByHealthCare()
    {
        $healthCare = str_random(12);
        $registrantRecord = factory(RegistrantRecord::class)->create(['health_care' => $healthCare]);

        $result = $this->filter->byHealthCare($healthCare)->first();

        $this->assertEquals($registrantRecord->health_care, $result->health_care);
        $this->assertEquals($registrantRecord->id, $result->id);
    }

    /**
     * Test that the byDriversLicense method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByDriversLicense()
    {
        $driversLicense = str_random(12);
        $registrantRecord = factory(RegistrantRecord::class)->create(['drivers_license' => $driversLicense]);

        $result = $this->filter->byDriversLicense($driversLicense)->first();

        $this->assertEquals($registrantRecord->drivers_license, $result->drivers_license);
        $this->assertEquals($registrantRecord->id, $result->id);
    }

    /**
     * Test that the byMilitaryServices method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByMilitaryServices()
    {
        $militaryServices = str_random(12);
        $registrantRecord = factory(RegistrantRecord::class)->create(['military_services' => $militaryServices]);

        $result = $this->filter->byMilitaryServices($militaryServices)->first();

        $this->assertEquals($registrantRecord->military_services, $result->military_services);
        $this->assertEquals($registrantRecord->id, $result->id);
    }
   
}
