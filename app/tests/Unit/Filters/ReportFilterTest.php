<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Models\RegistrantRecord;
use App\Filters\ReportFilter;


class ReportFilterTest extends TestCase
{
    use DatabaseTransactions;

    private $filter;

    /**
     * @inheritdoc
     */
    public function setUp() {
        parent::setUp();
        
        $request = $this->createMock(Request::class);
        
        $filter = new ReportFilter($request);

        $builder = new Builder($this->app->make('db')->connection());
        $builder->from('registrant_records');
        
        $property = (new \ReflectionClass(ReportFilter::class))->getProperty('builder');
        $property->setAccessible(true);
        $property->setValue($filter, $builder);
        
        $this->filter = $filter;
    }

    /**
     * Test that the byStartDate method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByStartDate()
    {
        $startDate = Carbon::now()->subDay(rand(3, 9));
        $registrantRecords = factory(RegistrantRecord::class, rand(3, 9))->create(['created_at' => $startDate]);

        $result = $this->filter->byStartDate($startDate->toDateTimeString())->get();

        $this->assertTrue($result[0]->created_at >= $startDate);
    }

    /**
     * Test that the byStartDate method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByEndDate()
    {
        $endDate = Carbon::now()->subDay(rand(3, 9));
        $registrantRecords = factory(RegistrantRecord::class, rand(3, 9))->create(['created_at' => $endDate]);

        $result = $this->filter->byEndDate($endDate->toDateTimeString())->get();

        $this->assertTrue($result[0]->created_at <= $endDate);
    }

    /**
     * Test that the bySex method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testBySex()
    {
        $sex = 'male';
        $registrantRecord = factory(RegistrantRecord::class)->create(['sex' => $sex]);

        $result = $this->filter->bySex($sex)->first();

        $this->assertEquals(strtolower($registrantRecord->sex), $result->sex);
    }

    /**
     * Test that the byProvinceId method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testByProvinceId()
    {
        $province = 1;
        $registrantRecord = factory(RegistrantRecord::class)->create(['province_id' => $province]);

        $result = $this->filter->byProvinceId($province)->first();

        $this->assertEquals($registrantRecord->province_id, $result->province_id);
    }
   
}
