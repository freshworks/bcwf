<?php

use Illuminate\Database\Query\Builder;
use App\Filters\Filters;


class FiltersTest extends TestCase
{

    /**
     * Test that apply method returns Illuminate\Database\Query\Builder
     *
     * @group Unit
     * @group Filters
     */
    public function testFiltersApply()
    {
        $builderMock = $this->createMock(Builder::class);
        $stub = $this->getMockBuilder(Filters::class)
                    ->disableOriginalConstructor()
                    ->getMockForAbstractClass();
        
        $this->assertEquals($builderMock, $stub->apply($builderMock));
    }

    /**
     * Test that parseFilter returns a formatted string
     *
     * @group Unit
     * @group Filters
     */
    public function testParseFilter()
    {
        $stub = $this->getMockBuilder(Filters::class)
                ->disableOriginalConstructor()
                ->getMockForAbstractClass();
        $method = (new \ReflectionClass(Filters::class))->getMethod('parseFilter');
        $method->setAccessible(true);

        $expectedFilterOne = [
            'input' => 'filter',
            'output' => 'byFilter',
        ];
        $expectedFilterTwo = [
            'input' => 'filter_two',
            'output' => 'byFilterTwo',
        ];
        $expectedFilterThree = [
            'input' => 'filter_test_three',
            'output' => 'byFilterTestThree',
        ];      

        $this->assertEquals(
            $expectedFilterOne['output'], 
            $method->invokeArgs($stub, array($expectedFilterOne['input']))
        );
        $this->assertEquals(
            $expectedFilterTwo['output'], 
            $method->invokeArgs($stub, array($expectedFilterTwo['input']))
        );
        $this->assertEquals(
            $expectedFilterThree['output'], 
            $method->invokeArgs($stub, array($expectedFilterThree['input']))
        );
    }
   
}
