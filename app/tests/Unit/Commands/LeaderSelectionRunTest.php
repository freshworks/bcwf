<?php
namespace Tests\Unit\Commands;

use TestCase;
use App\Facades\AmazonInstanceHelper;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LeaderSelectionRunTest extends TestCase
{
    public static $functions;


    /**
     * Test LeaderselectionRun Command works as expected as a Leader instance
     *
     * @group Unit
     * @group Commands
     */
    public function testWhenCronEnabledAsLeader()
    {
        $instanceId = "testInstanceId";
        $environmentName = "testEnvName";

        $this->config = \Mockery::mock('Illuminate\Config\Repository')
            ->shouldReceive('offsetGet')
            ->shouldReceive('get')->with('project.elasticbeanstalk.cron.enable')->andReturn(true)
            ->getMock()
        ;

        \Config::swap($this->config);

        AmazonInstanceHelper::shouldReceive('getCurrentInstanceId')->once()->andReturn($instanceId);
        AmazonInstanceHelper::shouldReceive('getEnvName')->once()->with($instanceId)->andReturn($environmentName);
        AmazonInstanceHelper::shouldReceive('isLeaderInstance')->once()->andReturn(true);

        $command = \Mockery::mock("\App\Console\Commands\LeaderSelectionRun")->makePartial();
        $command->shouldReceive('info')->once()->with('Initializing Leader Selection...');
        $command->shouldReceive('info')->once()->with('Instance ID: ' . $instanceId);
        $command->shouldReceive('info')->once()->with('Environment: ' . $environmentName);
        $command->shouldReceive('info')->once()->with('Getting Instances with Environment: ' . $environmentName);
        $command->shouldReceive('info')->once()->with('We are the Leader! Initiating Cron Setup');
        $command->shouldReceive('call')->once()->with('freshworks:cron-setup:scheduler');
        $command->shouldReceive('info')->once()->with('Leader Selection Done!');
        $command->handle();
    }

    /**
     * Test LeaderselectionRun Command works as expected when cron is disabled 
     *
     * @group Unit
     * @group Commands
     */
    public function testWhenCronDisalbled()
    {
        $this->config = \Mockery::mock('Illuminate\Config\Repository')
            ->shouldReceive('offsetGet')
            ->shouldReceive('get')->with('project.elasticbeanstalk.cron.enable')->andReturn(false)
            ->getMock()
        ;

        \Config::swap($this->config);

        $command = \Mockery::mock("\App\Console\Commands\LeaderSelectionRun")->makePartial();
        $command->shouldReceive('info')->once()->with('Initializing Leader Selection...');
        $command->shouldReceive('info')->once()->with('USE_CRON env var not set. Exiting.');
        $command->handle();
    }

    /**
     * Test LeaderselectionRun Command works as expected when cannot detect instance 
     *
     * @group Unit
     * @group Commands
     */
    public function testWhenCannotDetectInstanceId()
    {
        $instanceId = "testInstanceId";
        $environmentName = "testEnvName";

        $this->config = \Mockery::mock('Illuminate\Config\Repository')
            ->shouldReceive('offsetGet')
            ->shouldReceive('get')->with('project.elasticbeanstalk.cron.enable')->andReturn(true)
            ->getMock()
        ;

        \Config::swap($this->config);

        AmazonInstanceHelper::shouldReceive('getCurrentInstanceId')->once()->andReturn(null);

        $command = \Mockery::mock("\App\Console\Commands\LeaderSelectionRun")->makePartial();
        $command->shouldReceive('info')->once()->with('Initializing Leader Selection...');
        $command->shouldReceive('error')->once()->with('Did not detect an ec2 environment. Exiting.');
        $command->handle();
    }

    /**
     * Test LeaderselectionRun Command works as expected as not a leader 
     *
     * @group Unit
     * @group Commands
     */
    public function testWhenCronEnalbledAsNotLeader()
    {
        $instanceId = "testInstanceId";
        $environmentName = "testEnvName";

        $this->config = \Mockery::mock('Illuminate\Config\Repository')
            ->shouldReceive('offsetGet')
            ->shouldReceive('get')->with('project.elasticbeanstalk.cron.enable')->andReturn(true)
            ->getMock()
        ;

        \Config::swap($this->config);

        AmazonInstanceHelper::shouldReceive('getCurrentInstanceId')->once()->andReturn($instanceId);
        AmazonInstanceHelper::shouldReceive('getEnvName')->once()->with($instanceId)->andReturn($environmentName);
        AmazonInstanceHelper::shouldReceive('isLeaderInstance')->once()->andReturn(false);

        $command = \Mockery::mock("\App\Console\Commands\LeaderSelectionRun")->makePartial();
        $command->shouldReceive('info')->once()->with('Initializing Leader Selection...');
        $command->shouldReceive('info')->once()->with('Instance ID: ' . $instanceId);
        $command->shouldReceive('info')->once()->with('Environment: ' . $environmentName);
        $command->shouldReceive('info')->once()->with('Getting Instances with Environment: ' . $environmentName);
        $command->shouldReceive('info')->once()->with('We are not a leader instance :( Maybe next time...');
        $command->shouldReceive('info')->once()->with('Leader Selection Done!');
        $command->handle();
    }
}
