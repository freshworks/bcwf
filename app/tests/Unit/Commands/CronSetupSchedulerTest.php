<?php
namespace Tests\Unit\Commands;

use TestCase;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CronSetupSchedulerTest extends TestCase
{
    public static $functions;

    /**
     * Test CronSetupScheduler Command works as expected without overwrite option
     *
     * @group Unit
     * @group Commands
     */
    public function testCronSchedulerWithoutOverwrite()
    {
        $runCommand = 'schedule:run';
        $testCron = "*/5 * * * * $runCommand";

        // No Cron is running
        $command = \Mockery::mock('\App\Console\Commands\CronSetupScheduler')->makePartial()->shouldAllowMockingProtectedMethods();
        $command->shouldReceive('info')->once()->with('Initializing CRON Schedule Setup...');
        $command->shouldReceive('option')->once()->with('overwrite')->andReturn(null);
        $command->shouldReceive('shell_exec')->once()->with('crontab -l')->andReturn(null);
        $command->shouldReceive('isCronRunning')->once()->with(null, $runCommand);
        $command->shouldReceive('file_put_contents')->once();
        $command->shouldReceive('exec')->once()->with('crontab /tmp/crontab.txt');
        $command->shouldReceive('info')->once()->with('Schedule Cron Done!');
        $command->handle();

        // Cron is running
        $command = \Mockery::mock('\App\Console\Commands\CronSetupScheduler')->makePartial()->shouldAllowMockingProtectedMethods();
        $command->shouldReceive('info')->once()->with('Initializing CRON Schedule Setup...');
        $command->shouldReceive('option')->once()->with('overwrite')->andReturn(null);
        $command->shouldReceive('shell_exec')->once()->with('crontab -l')->andReturn($testCron);
        $command->shouldReceive('isCronRunning')->once()->with($testCron, $runCommand)->andReturn(true);
        $command->shouldReceive('info')->once()->with('Already found Scheduler entry! Not adding.');
        $command->shouldReceive('info')->once()->with('Schedule Cron Done!');
        $command->handle();        
    }

    /**
     * Test CronSetupScheduler Command works as expected with overwrite option
     *
     * @group Unit
     * @group Commands
     */
    public function testCronSchedulerWithOverwrite()
    {
        $runCommand = 'schedule:run';
        $testCron = "*/5 * * * * $runCommand";

        $command = \Mockery::mock('\App\Console\Commands\CronSetupScheduler')->makePartial()->shouldAllowMockingProtectedMethods();
        $command->shouldReceive('info')->once()->with('Initializing CRON Schedule Setup...');
        $command->shouldReceive('option')->once()->with('overwrite')->andReturn(true);
        $command->shouldReceive('info')->once()->with('Overwriting previous CRON contents...');
        $command->shouldReceive('isCronRunning')->once()->with(null, $runCommand);
        $command->shouldReceive('file_put_contents')->once();
        $command->shouldReceive('exec')->once()->with('crontab /tmp/crontab.txt');
        $command->shouldReceive('info')->once()->with('Schedule Cron Done!');
        $command->handle();
    }
}
