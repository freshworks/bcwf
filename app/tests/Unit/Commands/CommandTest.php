<?php
namespace Tests\Unit\Commands;

use TestCase;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Console\Commands\Command;

class CommandTest extends TestCase
{
    /**
     * Test isCronRunning returns true
     *
     * @group Unit
     * @group Commands
     */
    public function testIsCronRunning()
    {
        $command = new Command();
        $crons = 'test';
        $commandArg = 'test';

        $this->assertEquals($command->isCronRunning($crons, $commandArg), true);
    }
}
