<?php

namespace Tests\Unit\Enums;

use PHPUnit\Framework\TestCase;
use App\Enums\Enum;
use Tests\Unit\Enums\{FooEnum, BarEnum};


class EnumTest extends TestCase
{
    /**
     * Test that an enum has a value
     *
     * @group Unit
     * @group Enum
     */
    public function testEnumValues()
    {
        $this->assertEquals(1, FooEnum::FOO_ONE);
        $this->assertEquals('bar-one', BarEnum::BAR_ONE);
    }

    /**
     * Test that an enum can get it's keys
     *
     * @group Unit
     * @group Enum
     */
    public function testEnumGetKeys()
    {
        $keys = BarEnum::getKeys();
        $expectedKeys = ['BAR_ONE', 'BAR_TWO', 'BAR_THREE'];
        $this->assertEquals($expectedKeys, $keys);
    }

    /**
     * Test that an enum can get it's values
     *
     * @group Unit
     * @group Enum
     */
    public function testEnumGetValues()
    {
        $values = FooEnum::getValues();
        $expectedValues = [1, 2, 3];
        $this->assertEquals($expectedValues, $values);
    }

    /**
     * Test that an enum can get a specific key
     *
     * @group Unit
     * @group Enum
     */
    public function testEnumGetKey()
    {
        $this->assertEquals('FOO_TWO', FooEnum::getKey(2));
        $this->assertEquals('BAR_TWO', BarEnum::getKey('bar-two'));
    }

    /**
     * Test that an enum can get a specific value
     *
     * @group Unit
     * @group Enum
     */
    public function testEnumGetValue()
    {
        $this->assertEquals(3, FooEnum::getValue('FOO_THREE'));
        $this->assertEquals('bar-three', BarEnum::getValue('BAR_THREE'));
    }

    /**
     * Test that an enum can convert to an array
     *
     * @group Unit
     * @group Enum
     */
    public function testEnumToArray()
    {
        $array = FooEnum::toArray();
        $expectedArray = [
            'FOO_ONE' => 1,
            'FOO_TWO' => 2,
            'FOO_THREE' => 3,
        ];
        $this->assertEquals($expectedArray, $array);
    }

    /**
     * Test that an enum has a specific key
     *
     * @group Unit
     * @group Enum
     */
    public function testEnumHasKey()
    {
        $this->assertTrue(FooEnum::hasKey('FOO_ONE'));
        $this->assertFalse(FooEnum::hasKey('BAR_ONE'));
    }

    /**
     * Test that an enum has a specific value
     *
     * @group Unit
     * @group Enum
     */
    public function testEnumHasValue()
    {
        $this->assertTrue(FooEnum::hasValue(2));
        $this->assertFalse(BarEnum::hasValue('foo-two'));
    }
}
