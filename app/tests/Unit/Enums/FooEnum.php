<?php

namespace Tests\Unit\Enums;

use App\Enums\Enum;

final class FooEnum extends Enum
{
    const FOO_ONE   = 1;
    const FOO_TWO   = 2;
    const FOO_THREE = 3;
}
