<?php

namespace Tests\Unit\Enums;

use App\Enums\Enum;

final class BarEnum extends Enum
{
    const BAR_ONE   = 'bar-one';
    const BAR_TWO   = 'bar-two';
    const BAR_THREE = 'bar-three';
}
