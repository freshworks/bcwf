<?php

namespace Tests\Unit\Pdf;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Pdf\PdfGenerator;

/**
 * PdfGenerator Test
 *
 * @author Freshworks <info@freshworks.io>
 */

class PdfGeneratorTest extends TestCase
{

    use DatabaseTransactions;


    public function setUp()
    {
        parent::setUp();

        $this->stack = [
            "className" => "\App\Pdf\PdfGenerator"
        ];
    }


    /**
     * Test that the constructor calls the expected internal methods
     *
     * @group Unit
     * @group PdfGenerator
     */
    public function testConstructorCallsInternalMethods()
    {
        $classname = '\App\Pdf\PdfGenerator';

        // Get mock, without the constructor being called
        $mock = $this->getMockBuilder($this->stack['className'])
            ->disableOriginalConstructor()
            ->setMethods(['setFileName', 'setTemplateName'])
            ->getMockForAbstractClass();

         // set expectations for constructor calls
        $mock->expects($this->once())->method('setFileName');
        $mock->expects($this->once())->method('setTemplateName');

        // now call the constructor
        $reflectedClass = new \ReflectionClass($this->stack['className']);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($mock);
    }

    /**
     * Undocumented function
     *
     * @group Walrus
     */
    public function testSetDataCorrectlySetsProperty()
    {
        $reflectedClass = new \ReflectionClass($this->stack['className']);
        $property = $reflectedClass->getProperty('data');
        $property->setAccessible(true);

        $this->assertNull($property->getValue($reflectedClass));


    }


    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * getPrivateProperty
     *
     * @param 	string $className
     * @param 	string $propertyName
     * @return	ReflectionProperty
     */
    public function getPrivateProperty($className, $propertyName)
    {
        $reflector = new \ReflectionClass($className);
        $property = $reflector->getProperty($propertyName);
        $property->setAccessible(true);

        return $property;
    }


}
