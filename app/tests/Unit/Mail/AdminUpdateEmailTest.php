<?php

namespace Tests\Unit\Mail;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Mail\AdminUpdateEmail;
use App\Models \{
    Admin, User
};
use App\Enums \{
    RolesEnum, UserStatusEnum
};
use Carbon\Carbon;

/**
 * AdminUpdate Email Test
 *
 * @author Freshworks <info@freshworks.io>
 */
class AdminUpdateEmailTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that the Admin Update Email is generated correctly
     *
     * @group Unit
     * @group Mail
     * @group AdminUpdateEmail
     */
    public function testAdminUpdateEmail()
    {
        $actor = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create();

        $event = new \App\Events\AdminUpdateEvent($admin, $actor);
        $message = new \App\Mail\AdminUpdateEmail($event);
        $expected = [
            'user.email' => $admin->userRecord->email,
            'user.role' => trans('acl.' . RolesEnum::STAFF, [], 'en'),
            'user.role_fr' => trans('acl.' . RolesEnum::STAFF, [], 'fr'),
            'user.company_name' => $admin->group->name,
            'actor.name' => "{$actor->first_name} {$actor->last_name}",
            'actor.role' => trans('acl.' . RolesEnum::SYSTEM_ADMIN, [], 'en'),
            'actor.role_fr' => trans('acl.' . RolesEnum::SYSTEM_ADMIN, [], 'fr'),
        ];

        $this->assertEquals($message->mailData->user->email, $expected['user.email']);
        $this->assertEquals($message->mailData->user->role, $expected['user.role']);
        $this->assertEquals($message->mailData->user->role_fr, $expected['user.role_fr']);
        $this->assertEquals($message->mailData->user->company_name, $expected['user.company_name']);
        $this->assertEquals($message->mailData->actor->name, $expected['actor.name']);
        $this->assertEquals($message->mailData->actor->role, $expected['actor.role']);
        $this->assertEquals($message->mailData->actor->role_fr, $expected['actor.role_fr']);

        \Mail::fake();
        \Mail::to($message->mailData->user->email)->send($message);
        \Mail::assertSent(AdminUpdateEmail::class, 1);
    }
}
