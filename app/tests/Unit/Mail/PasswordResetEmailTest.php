<?php

namespace Tests\Unit\Mail;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Mail\PasswordResetEmail;
use App\Models\ {PasswordResets, User};

/**
 * PasswordResetEmailTest
 *
 * @author Freshworks <info@freshworks.io>
 */
class PasswordResetEmailTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that the Password Reset Email is generated correctly
     *
     * @group Unit
     * @group Mail
     * @group PasswordResetEmail
     */
    public function testPasswordResetEmail()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        $reset = PasswordResets::create($data);

        $message = new PasswordResetEmail($reset);

        $this->assertEquals($message->mailData->reset->expireMinutes, PasswordResets::$expireMinutes);
        $this->assertEquals($message->mailData->reset->resetToken, $reset->token);
        $this->assertEquals($message->mailData->user->email, $user->email);

        \Mail::fake();
        \Mail::to($message->mailData->user->email)->send($message);
        \Mail::assertSent(PasswordResetEmail::class, 1);
    }
}
