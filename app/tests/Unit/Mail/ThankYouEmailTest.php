<?php

namespace Tests\Unit\Mail;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Mail\ThankYouEmail;
use App\Models\ {Registrant, User};

/**
 * ThankYouEmailTest
 *
 * @author Freshworks <info@freshworks.io>
 */
class ThankYouEmailTest extends TestCase 
{
    use DatabaseTransactions;

    /**
     * Test that the ThankYou Email is generated correctly
     * 
     * @group Unit
     * @group Mail
     * @group ThankYouEmail
     */
    public function testThankYouEmail()
    {

        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg  = Registrant::find($user->account_id);

        $keyData = ['id' => $user->id, 'user_name' => $user->user_name];
        $message = new ThankYouEmail($reg);
        
        $this->assertEquals($message->mailData->user->email, $reg->activeRecord()->email);
        $this->assertEquals(decrypt($message->mailData->user->key), $keyData);

        \Mail::fake();
        \Mail::to($message->mailData->user->email)->send($message);
        \Mail::assertSent(ThankYouEmail::class, 1);
        
    }
}
