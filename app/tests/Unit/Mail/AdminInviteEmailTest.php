<?php

namespace Tests\Unit\Mail;

use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Mail\AdminInviteEmail;
use App\Models\ {Admin, User};
use App\Enums\ {RolesEnum, UserStatusEnum};
use Carbon\Carbon;

/**
 * PasswordResetEmailTest
 *
 * @author Freshworks <info@freshworks.io>
 */
class AdminInviteEmailTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that the Password Reset Email is generated correctly
     *
     * @group Unit
     * @group Mail
     * @group AdminInviteEmail
     */
    public function testAdminInviteEmail()
    {
        $inviter = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $expires = Carbon::now()->addDay(\Config::get('project.admin.invite_expiration'))->toDateTimeString();
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_sender' => $inviter->id,
                'invite_token' => str_random(32),
                'invite_expiration' => $expires
            ]
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::PENDING]);
        $message = new AdminInviteEmail($admin);

        $expected = [
            'user.email' => $admin->userRecord->email,
            'user.role'  => trans('acl.'.RolesEnum::STAFF,[], 'en'),
            'user.role_fr' => trans('acl.' . RolesEnum::STAFF, [], 'fr'),
            'user.company_name' => $admin->group->name,
            'sender.name'   => "{$inviter->first_name} {$inviter->last_name}",
            'sender.role'    => trans('acl.'.RolesEnum::SYSTEM_ADMIN, [], 'en'),
            'sender.role_fr' => trans('acl.' . RolesEnum::SYSTEM_ADMIN, [], 'fr'),
            'token' => $admin->meta->invite_token
        ];


        $this->assertEquals($message->mailData->user->email, $expected['user.email']);
        $this->assertEquals($message->mailData->user->role,  $expected['user.role']);
        $this->assertEquals($message->mailData->user->role_fr, $expected['user.role_fr']);
        $this->assertEquals($message->mailData->user->company_name, $expected['user.company_name']);
        $this->assertEquals($message->mailData->invite->sender->name, $expected['sender.name']);
        $this->assertEquals($message->mailData->invite->sender->role, $expected['sender.role']);
        $this->assertEquals($message->mailData->invite->sender->role_fr, $expected['sender.role_fr']);
        $this->assertEquals($message->mailData->invite->token, $expected['token']);

        \Mail::fake();
        \Mail::to($message->mailData->user->email)->send($message);
        \Mail::assertSent(AdminInviteEmail::class, 1);
    }
}
