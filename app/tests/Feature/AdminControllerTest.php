<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;
use App\Models\ {Admin, Group, User, RegistrantRecord};
use App\Enums\ {RolesEnum, UserStatusEnum, AbilitiesEnum};
use App\Facades\AccessControlManager;

class AdminControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }

    /**
     * Test that an authenticated user can get their admin account
     *
     * @group Feature
     * @group Admin
     */
    public function testAccount()
    {
        $admin = factory(Admin::class)->states([RolesEnum::GROUP_ADMIN])->create();
        $this->actingAs($admin->userRecord);

        $response = $this->call('GET', '/api/admin/account');

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('Admin'));
        $this->seeJson(['id' => $admin->id]);
    }

    /**
     * Test that an permissioned user can get an admin account
     *
     * @group Feature
     * @group Admin
     */
    public function testShow()
    {
        $group = factory(Group::class)->create();
        $admin = factory(Admin::class)->create(['group_id' => $group->id]);

        $sysAdmin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create();
        $this->actingAs($sysAdmin->userRecord);
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP));

        $response = $this->call('GET', '/api/admin/show/' . $admin->id);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('Admin'));
        $this->seeJson(['id' => $admin->id]);

        $groupAdmin = factory(Admin::class)->states([RolesEnum::GROUP_ADMIN])->create(['group_id' => $group->id]);
        $this->actingAs($groupAdmin->userRecord);
        $this->assertTrue($groupAdmin->can(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP));
        $this->assertFalse($groupAdmin->can(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP));

        $response2 = $this->call('GET', '/api/admin/show/' . $admin->id);
        $this->assertEquals(ResponseCode::HTTP_OK, $response2->status());
        $this->seeJsonStructure(StructureFactory::resource('Admin'));
        $this->seeJson(['id' => $admin->id]);

        $staff = factory(Admin::class)->states([RolesEnum::STAFF])->create(['group_id' => $group->id]);
        $this->actingAs($staff->userRecord);
        $this->assertFalse($staff->can(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP));
        $this->assertFalse($staff->can(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP));

        $response3 = $this->call('GET', '/api/admin/show/' . $admin->id);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response3->status());
    }

    /**
     * Test creating an admin invitation
     *
     * @group Feature
     * @group Admin
     * @group AdminInvite
     */
    public function testAdminInvite()
    {
        \Mail::fake();
        $this->expectsEvents('App\Events\AdminInviteEvent');

        $admin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create();
        $this->actingAs($admin->userRecord);

        $input = [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'group_id' => 1,
            'role' => RolesEnum::STAFF,
        ];

        $response = $this->call('POST', '/api/admin/invite', $input);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);

        $newAdmin = Admin::where('email', '=', $input['email'])->first();

        $this->assertEquals($input['role'], $newAdmin->meta->role);
        $this->assertTrue($newAdmin->isA($input['role']));
        $this->assertEquals($admin->userRecord->account_id, $newAdmin->meta->invite_sender);
        $this->assertEquals(UserStatusEnum::PENDING, $newAdmin->userRecord->status_id);
    }

    /**
     * Test creating an admin invitation with an existing email
     *
     * @group Feature
     * @group Admin
     * @group AdminInvite
     */
    public function testAdminInviteWithExistingEmail()
    {
        $admin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create();
        $this->actingAs($admin->userRecord);

        $input = [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'email' => $admin->email,
            'group_id' => 1,
            'role' => RolesEnum::STAFF,
        ];

        $response = $this->call('POST', '/api/admin/invite', $input);

        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test verifying an admin activation token
     *
     * @group Feature
     * @group Admin
     */
    public function testAdminVerifyToken()
    {
        $input = ['token' => str_random(32)];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_token' => $input['token'],
                'invite_expiration' => Carbon::now()->addDay(10)->toDateTimeString(),
            ],
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::PENDING]);

        $response = $this->call('POST', '/api/admin/verify', $input);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('Admin'));
        $this->seeJson(['id' => $admin->id]);
    }

    /**
     * Test verifying an admin activation token with an wrong token
     *
     * @group Feature
     * @group Admin
     */
    public function testAdminVerifyTokenWithWrongToken()
    {
        $input = ['token' => 'bad_token'];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_token' => str_random(32),
                'invite_expiration' => Carbon::now()->addDay(10)->toDateTimeString(),
            ],
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::PENDING]);

        $response = $this->call('POST', '/api/admin/verify', $input);

        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test verifying an admin activation token with an expired token
     *
     * @group Feature
     * @group Admin
     */
    public function testAdminVerifyTokenWithExpiredToken()
    {
        $input = ['token' => str_random(32)];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_token' => $input['token'],
                'invite_expiration' => Carbon::now()->addDay(-10)->toDateTimeString(),
            ],
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::PENDING]);

        $response = $this->call('POST', '/api/admin/verify', $input);

        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }

    /**
     * Test verifying an admin activation token with a used token
     *
     * @group Feature
     * @group Admin
     */
    public function testAdminVerifyTokenWithUsedToken()
    {
        $input = ['token' => str_random(32)];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_token' => $input['token'],
                'invite_expiration' => Carbon::now()->addDay(10)->toDateTimeString(),
            ],
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::ACTIVE]);

        $response = $this->call('POST', '/api/admin/verify', $input);

        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }

    /**
     * Test that the activate process works as expected
     *
     * @group Feature
     * @group Admin
     * @group AdminActivate
     */
    public function testAdminActivate()
    {
        \Carbon::setTestNow(\Carbon::now());

        // Generate Invitation State
        $input = ['token' => str_random(32)];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_token' => $input['token'],
                'invite_expiration' => Carbon::now()->addDay(10)->toDateTimeString(),
            ],
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::PENDING]);

        $params = [
            'token' => $admin->meta->invite_token,
            'first_name' => 'Foo',
            'last_name'  => 'Bar',
            'password'   => 'FooBar123!'
        ];

        $this->assertNotEquals($admin->first_name, $params['first_name']);
        $this->assertNotEquals($admin->last_name, $params['last_name']);
        $this->assertEquals($admin->userRecord->status_id, UserStatusEnum::PENDING);
        $this->assertNull($admin->userRecord->activated_at);

        $response = $this->call('POST', '/api/admin/activate', $params);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $admin = Admin::find($admin->id);
        $this->assertEquals($admin->first_name, $params['first_name']);
        $this->assertEquals($admin->last_name, $params['last_name']);
        $this->assertEquals($admin->userRecord->status_id, UserStatusEnum::ACTIVE);
        $this->assertEquals($admin->userRecord->fresh()->activated_at, \Carbon::now());
    }

    /**
     * Test admin-activation fails with wrong-token
     *
     * @group Feature
     * @group Admin
     * @group AdminActivate
     */
    public function testAdminActivateWithWrongToken()
    {
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_token' => str_random(32),
                'invite_expiration' => Carbon::now()->addDay(10)->toDateTimeString(),
            ],
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::PENDING]);

        $params = [
            'token' => str_random(32)."badToken",
            'first_name' => 'Foo',
            'last_name' => 'Bar',
            'password' => 'FooBar123!'
        ];
        $response = $this->call('POST', '/api/admin/activate', $params);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
        $admin = Admin::find($admin->id);
        $this->assertEquals($admin->userRecord->status_id, UserStatusEnum::PENDING);
    }

    /**
     * Test admin-activation fails with expired token
     *
     * @group Feature
     * @group Admin
     * @group AdminActivate
     */
    public function testAdminActivateWithExpiredToken()
    {
        $input = ['token' => str_random(32)];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_token' => $input['token'],
                'invite_expiration' => Carbon::now()->addDay(-10)->toDateTimeString(),
            ],
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::PENDING]);

        $params = [
            'token' => $admin->meta->invite_token,
            'first_name' => 'Foo',
            'last_name' => 'Bar',
            'password' => 'FooBar123!'
        ];
        $response = $this->call('POST', '/api/admin/activate', $params);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
        $admin = Admin::find($admin->id);
        $this->assertEquals($admin->userRecord->status_id, UserStatusEnum::PENDING);

    }

    /**
     * Test admin-activation fails with a used-token
     *
     * @group Feature
     * @group Admin
     * @group AdminActivate
     */
    public function testAdminActivateWithUsedToken()
    {
        $input = ['token' => str_random(32)];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create([
            'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_token' => $input['token'],
                'invite_expiration' => Carbon::now()->addDay(10)->toDateTimeString(),
            ],
        ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::ACTIVE]);

        $params = [
            'token' => $admin->meta->invite_token,
            'first_name' => 'Foo',
            'last_name' => 'Bar',
            'password' => 'FooBar123!'
        ];
        $response = $this->call('POST', '/api/admin/activate', $params);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }

    /**
     * Test that the admin change-password method works as expected
     *
     * @group Feature
     * @group Admin
     * @group ChangePassword
     */
    public function testAdminChangePassword()
    {
        $expected = [
            'original' => 'FooBarMcNancyPants',
            'updated'  => 'FooBar123!'
        ];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();
        $admin->userRecord->password = app('hash')->make($expected['original']);
        $admin->userRecord->save();
        $this->assertTrue(app('hash')->check($expected['original'], $admin->userRecord->password));

        $this->actingAs($admin->userRecord);
        $params = [
            'password_old' => $expected['original'],
            'password' => $expected['updated']
        ];
        $response = $this->call('POST', '/api/admin/change-password', $params);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $user = User::find($admin->userRecord->id);
        $this->assertTrue(app('hash')->check($expected['updated'], $user->password));
    }

    /**
     * Test that the admin change-password method fails when provided an incorrect password
     *
     * @group Feature
     * @group Admin
     * @group ChangePassword
     */
    public function testAdminChangePasswordFailsWithWrongPassword()
    {
        $expected = [
            'original' => 'FooBarMcNancyPants',
            'updated' => 'FooBar123!'
        ];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();
        $admin->userRecord->password = app('hash')->make($expected['original']);
        $admin->userRecord->save();
        $this->assertTrue(app('hash')->check($expected['original'], $admin->userRecord->password));

        $this->actingAs($admin->userRecord);
        $params = [
            'password_old' => $expected['original'].str_random(12),
            'password' => $expected['updated']
        ];
        $response = $this->call('POST', '/api/admin/change-password', $params);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $user = User::find($admin->userRecord->id);
        $this->assertTrue(app('hash')->check($expected['original'], $user->password));
    }

    /**
     * Test that the admin change-password method fails when provided a week password
     *
     * @group Feature
     * @group Admin
     * @group ChangePassword
     */
    public function testAdminChangePasswordFailsWithWeakPassword()
    {
        $expected = [
            'original' => 'FooBarMcNancyPants',
            'updated' => 'foobar!'
        ];
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();
        $admin->userRecord->password = app('hash')->make($expected['original']);
        $admin->userRecord->save();
        $this->assertTrue(app('hash')->check($expected['original'], $admin->userRecord->password));

        $this->actingAs($admin->userRecord);
        $params = [
            'password_old' => $expected['original'],
            'password' => $expected['updated']
        ];
        $response = $this->call('POST', '/api/admin/change-password', $params);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());

        $user = User::find($admin->userRecord->id);
        $this->assertTrue(app('hash')->check($expected['original'], $user->password));
    }

    /**
     * Test that the admin change-status endpoint works as expected
     *
     * @group Feature
     * @group Admin
     * @group ChangeStatus
     */
    public function testChangeStatus()
    {
        \Mail::fake();
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $user  = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $admin->group->id]);

        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertEquals($user->userRecord->status_id, UserStatusEnum::ACTIVE);

        $this->actingAs($admin->userRecord);
        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $user = Admin::find($user->id);
        $this->assertEquals($user->userRecord->status_id, UserStatusEnum::DISABLED);

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::ACTIVE]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $user = Admin::find($user->id);
        $this->assertEquals($user->userRecord->status_id, UserStatusEnum::ACTIVE);

        // Test changing status of a pending user
        $user->userRecord->status_id = UserStatusEnum::PENDING;
        $user->userRecord->save();

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::ACTIVE]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->missingFromDatabase('admins', ['id' => $user->id]);
        $this->missingFromDatabase('users', ['id' => $user->userRecord->id]);
    }

    /**
     * Test that the access-control/abilities for changing admin-status is applied correctly
     *
     * @group Feature
     * @group Admin
     * @group ChangeStatus
     * @group AccessControl
     */
    public function testChangeStatusGroupAccessControl()
    {
        \Mail::fake();
        $groups = factory(\App\Models\Group::class, 2)->create();
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $groups[0]->id]);
        $user  = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $groups[0]->id]);

        $this->actingAs($admin->userRecord);

        // Test same-group-editing
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_STAFF);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertEquals($admin->group->id, $user->group->id);
        $this->assertEquals($user->userRecord->status_id, UserStatusEnum::ACTIVE);

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::ACTIVE);

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::DISABLED);

        // Test different-group-editing
        $user = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $groups[1]->id]);

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_STAFF);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertNotEquals($admin->group->ud, $user->group->id);
        $this->assertEquals($user->userRecord->status_id, UserStatusEnum::ACTIVE);

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::ACTIVE);

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::DISABLED);

    }


    /**
     * Test that the access-control/abilities for changing admin-status is applied correctly
     *
     * @group Feature
     * @group Admin
     * @group ChangeStatus
     * @group AccessControl
     */
    public function testChangeStatusRoleAccessControl()
    {
        \Mail::fake();
        $groups = factory(\App\Models\Group::class, 2)->create();
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $groups[0]->id]);
        $user = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $groups[0]->id]);

        $this->actingAs($admin->userRecord);

        // Test staff-editing
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_STAFF);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_GROUP_ADMIN);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_SYSTEM_ADMIN);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertEquals($admin->group->id, $user->group->id);
        $this->assertEquals($user->userRecord->status_id, UserStatusEnum::ACTIVE);
        $this->assertTrue($user->isA(RolesEnum::STAFF));

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::ACTIVE);

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_STAFF);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::DISABLED);

        // Test group-editing
        $user = factory(Admin::class)->states(RolesEnum::GROUP_ADMIN)->create(['group_id' => $groups[0]->id]);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_STAFF);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_GROUP_ADMIN);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_SYSTEM_ADMIN);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertEquals($admin->group->id, $user->group->id);
        $this->assertEquals($user->userRecord->status_id, UserStatusEnum::ACTIVE);
        $this->assertTrue($user->isA(RolesEnum::GROUP_ADMIN));

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::ACTIVE);

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_GROUP_ADMIN);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::DISABLED);

        // Test system-admin-editing
        $user = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create(['group_id' => $groups[0]->id]);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_STAFF);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_GROUP_ADMIN);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_SYSTEM_ADMIN);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertEquals($admin->group->id, $user->group->id);
        $this->assertEquals($user->userRecord->status_id, UserStatusEnum::ACTIVE);
        $this->assertTrue($user->isA(RolesEnum::SYSTEM_ADMIN));

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::ACTIVE);

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_SYSTEM_ADMIN);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));

        $response = $this->call('POST', '/api/admin/change-status', ['admin_id' => $user->id, 'status_id' => UserStatusEnum::DISABLED]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertEquals($user->userRecord->fresh()->status_id, UserStatusEnum::DISABLED);

    }

    /**
     * Test than a permitted admin can edit an admin's account
     *
     * @group Feature
     * @group Admin
     */
    public function testAdminEditAccount()
    {
        $otherGroup = factory(Group::class)->create();
        $group = factory(Group::class)->create();
        $admins = factory(Admin::class, 3)->create(['group_id' => $group->id]);

        $sysAdmin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create();
        $this->actingAs($sysAdmin->userRecord);
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_SELF));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::CHANGE_USER_GROUP));

        $input = [
            'admin_id'   => $admins[0]->id,
            'first_name' => $this->faker->firstName(),
            'last_name'  => $this->faker->lastName(),
            'role'       => RolesEnum::GROUP_ADMIN,
            'group_id'   => $otherGroup->id,
        ];
        $response = $this->call('POST', '/api/admin/edit', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => true]);
        $this->assertEquals($otherGroup->id, $admins[0]->fresh()->group_id);
        $this->assertEquals($input['first_name'], $admins[0]->fresh()->first_name);
        $this->assertEquals($input['last_name'], $admins[0]->fresh()->last_name);
        $this->assertEquals($input['role'], $admins[0]->fresh()->meta->role);

        $groupAdmin = factory(Admin::class)->states([RolesEnum::GROUP_ADMIN])->create(['group_id' => $group->id]);
        $this->actingAs($groupAdmin->userRecord);
        $this->assertTrue($groupAdmin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($groupAdmin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertTrue($groupAdmin->can(AbilitiesEnum::MANAGE_SELF));
        $this->assertFalse($groupAdmin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($groupAdmin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertTrue($groupAdmin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertFalse($groupAdmin->can(AbilitiesEnum::CHANGE_USER_GROUP));

        $input = [
            'admin_id'   => $admins[1]->id,
            'first_name' => $this->faker->firstName(),
            'last_name'  => $this->faker->lastName(),
            'role'       => RolesEnum::GROUP_ADMIN,
            'group_id'   => $group->id,
        ];
        $response2 = $this->call('POST', '/api/admin/edit', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response2->status());
        $this->seeJson(['success' => true]);
        $this->assertEquals($input['first_name'], $admins[1]->fresh()->first_name);
        $this->assertEquals($input['last_name'], $admins[1]->fresh()->last_name);
        $this->assertEquals($input['role'], $admins[1]->fresh()->meta->role);

        $input = [
            'admin_id'   => $admins[1]->id,
            'first_name' => $this->faker->firstName(),
            'last_name'  => $this->faker->lastName(),
            'role'       => RolesEnum::GROUP_ADMIN,
            'group_id'   => $otherGroup->id,
        ];
        $response3 = $this->call('POST', '/api/admin/edit', $input);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response3->status());

        $staff = factory(Admin::class)->states([RolesEnum::STAFF])->create(['group_id' => $group->id]);
        $this->actingAs($staff->userRecord);
        $this->assertFalse($staff->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($staff->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertTrue($staff->can(AbilitiesEnum::MANAGE_SELF));
        $this->assertFalse($staff->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertFalse($staff->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertFalse($staff->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertFalse($staff->can(AbilitiesEnum::CHANGE_USER_GROUP));

        $input = [
            'admin_id'   => $admins[2]->id,
            'first_name' => $this->faker->firstName(),
            'last_name'  => $this->faker->lastName(),
            'role'       => RolesEnum::STAFF,
            'group_id'   => $group->id,
        ];
        $response4 = $this->call('POST', '/api/admin/edit', $input);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response4->status());
    }

    /**
     * Test admin editing their own account
     *
     * @group Feature
     * @group Admin
     */
    public function testAdminEditOwnAccount()
    {
        $group = factory(Group::class)->create();
        $sysAdmin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create([
            'group_id' => $group->id,
        ]);
        $this->actingAs($sysAdmin->userRecord);
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_SELF));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::CHANGE_USER_GROUP));

        $input = [
            'admin_id'   => $sysAdmin->id,
            'first_name' => $this->faker->firstName(),
            'last_name'  => $this->faker->lastName(),
            'role'       => $sysAdmin->getRole(),
            'group_id'   => $sysAdmin->group_id,
        ];
        $response = $this->call('POST', '/api/admin/edit', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => true]);

        // Changing different role
        $input = [
            'admin_id'   => $sysAdmin->id,
            'first_name' => $this->faker->firstName(),
            'last_name'  => $this->faker->lastName(),
            'role'       => RolesEnum::GROUP_ADMIN,
            'group_id'   => $sysAdmin->group_id,
        ];
        $response2 = $this->call('POST', '/api/admin/edit', $input);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response2->status());
    }

    /**
     * Test creating an admin invitation
     *
     * @group Feature
     * @group Admin
     * @group AdminInvite
     * @group AccessControl
     */
    public function testAdminInviteAccessControll()
    {
        \Mail::fake();
        $this->expectsEvents('App\Events\AdminInviteEvent');

        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();
        $this->actingAs($admin->userRecord);

        $input = [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'group_id' => $admin->group->id,
            'role' => RolesEnum::STAFF,
        ];

        // Should fail without perms
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_STAFF));

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());


        // Same Group
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_STAFF);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertEquals($admin->group->id, $input['group_id']);

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        // Different Group
        $group = factory(App\Models\Group::class)->create();
        $input['group_id'] = $group->id;
        $input['email'] = $this->faker->safeEmail;

        $this->assertNotEquals($admin->group->id, $input['group_id']);

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_STAFF);
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $group = factory(App\Models\Group::class)->create();

        // Staff Types
        $input['role'] = RolesEnum::STAFF;
        $input['email'] = $this->faker->safeEmail;
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_STAFF);
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_STAFF));

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_STAFF);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        // Group-Admin Types
        $input['role'] = RolesEnum::GROUP_ADMIN;
        $input['email'] = $this->faker->safeEmail;
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_GROUP_ADMIN);
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_GROUP_ADMIN);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());


        // System-Admin Types
        $input['role'] = RolesEnum::SYSTEM_ADMIN;
        $input['email'] = $this->faker->safeEmail;
        $admin->userRecord->disallow(AbilitiesEnum::MANAGE_SYSTEM_ADMIN);
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_SYSTEM_ADMIN);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

    }

    /**
     * Test that the resend-invitation works as expected
     *
     * @group Feature
     * @group Admin
     * @group AdminInvite
     */
    public function testResendInvite()
    {
        Mail::fake();
        $group = factory(Group::class)->create();
        $actor = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create(['group_id' => $group->id]);
        $this->actingAs($actor->userRecord);

        // Generate a 'natural' invite so we can record its original state.
        $input = [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'group_id' => $group->id,
            'role' => RolesEnum::STAFF,
        ];

        $response = $this->call('POST', '/api/admin/invite', $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);

        $admin = Admin::where('email', '=', $input['email'])->first();
        $expects['meta'] = $admin->meta;

        $this->expectsEvents('App\Events\AdminInviteEvent');

        $response = $this->call('POST', '/api/admin/resend-invite', ['admin_id' => $admin->id]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => true]);

        $admin->fresh();
        $this->assertEquals($expects['meta']->invite_token, $admin->meta->invite_token);
        $this->assertEquals($expects['meta']->invite_sender, $actor->id);
        $this->assertEquals(UserStatusEnum::PENDING, $admin->userRecord->status_id);

    }

    /**
     * Test that when resending and invite to a disasbled user that its status is updated
     *
     * @group Feature
     * @group Admin
     * @group AdminInvite
     */
    public function testResendInviteDisabledAdmin()
    {
        Carbon::setTestNow(Carbon::now());
        \Mail::fake();
        $this->expectsEvents('App\Events\AdminInviteEvent');

        $group = factory(Group::class)->create();
        $actor = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create(['group_id' => $group->id]);

        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create(
            ['group_id' => $group->id,
             'meta' => [
                'role' => RolesEnum::STAFF,
                'invite_sender' => $actor->id,
                'invite_token' => str_random(32),
                'invite_expiration' => Carbon::now()
                    ->addDay(\Config::get('project.admin.invite_expiration'))
                    ->subDay(10)
                    ->toDateTimeString(),
                ]
            ]);
        $admin->userRecord->update(['status_id' => UserStatusEnum::DISABLED]);

        $expected = [
            'sender' => $admin->meta->invite_sender,
            'invite_token' => $admin->meta->invite_token,
            'invite_expiration' => Carbon::now()->addDay(\Config::get('project.admin.invite_expiration'))->toDateTimeString(),
            'status_id' => UserStatusEnum::PENDING
        ];

        $this->actingAs($actor->userRecord);

        $response = $this->call('POST', '/api/admin/resend-invite', ['admin_id' => $admin->id]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $admin = Admin::find($admin->id);
        $this->assertEquals($expected['status_id'], $admin->userRecord->status_id);
        $this->assertEquals($expected['sender'], $admin->meta->invite_sender);
        $this->assertEquals($expected['invite_token'], $admin->meta->invite_token);
        $this->assertEquals($expected['invite_expiration'], $admin->meta->invite_expiration);

    }
    /**
     * Test that resending an invite fails for an active users
     *
     * @group Feature
     * @group Admin
     * @group AdminInvite
     * @group AccessControl
     */
    public function testResendInviteFailsIfActive()
    {
        $group = factory(Group::class)->create();
        $actor = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create(['group_id' => $group->id]);
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create(
            [
                'group_id' => $group->id,
                'meta' => [
                    'role' => RolesEnum::STAFF,
                    'invite_sender' => $actor->id,
                    'invite_token' => str_random(32),
                    'invite_expiration' => Carbon::now()
                        ->addDay(\Config::get('project.admin.invite_expiration'))
                        ->subDay(10)
                        ->toDateTimeString(),
                ]
            ]
        );
        $admin->userRecord->update(['status_id' => UserStatusEnum::ACTIVE]);

        $this->actingAs($actor->userRecord);
        $response = $this->call('POST', '/api/admin/resend-invite', ['admin_id' => $admin->id]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

    }


}
