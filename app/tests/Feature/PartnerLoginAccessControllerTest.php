<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;
use App\Models \{
    PartnerLoginAccessKey, User, Registrant, ClientType
};
use Ramsey\Uuid\Uuid;
use App\Enums\AccessKeyEnum;

class PartnerLoginAccessControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $stack;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->stack = [
            'url.generate' => '/api/partner/generate-access-key',
            'url.validate' => '/api/partner/validate-access-key',
            'client.apple' => ClientType::find(ClientType::APPLE_ID),
            'client.web'   => ClientType::find(ClientType::WEB_ID)
        ];
    }

    /**
     * Test that the create method works as expected
     *
     * @group Feature
     * @group PartnerLoginAccessKey
     */
    public function testCreate()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg = Registrant::find($user->account_id);
        $reg->client_type_id = ClientType::APPLE_ID;
        $reg->uuid = strtoupper(Uuid::uuid4()->toString());
        $reg->save();

        $clientType = ClientType::find(ClientType::APPLE_ID);
        $response = $this->call('POST', '/api/partner/generate-access-key', ['registrant_uuid' => $reg->uuid], [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);

        $this->assertEquals(ResponseCode::HTTP_CREATED, $response->status());
        $this->seeJsonStructure(StructureFactory::resource("PartnerLoginAccessKey"));

        $content = json_decode($response->getContent());
        $this->seeInDatabase('partner_login_access_keys', ['key' => $content->data->access_key, 'registrant_id' => $reg->id]);

     }

    /**
     * Test that generate-access0key fails with an invalid registrant
     *
     * @group Feature
     * @group PartnerLoginAccessKey
     */
    public function testCreateFailsWithInvalidRegistrantUuid()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg = Registrant::find($user->account_id);
        $reg->client_type_id = ClientType::WEB_ID;
        $reg->uuid = strtoupper(Uuid::uuid4()->toString());
        $reg->save();

        $clientType = ClientType::find(ClientType::APPLE_ID);
        $response = $this->call('POST', '/api/partner/generate-access-key', ['registrant_uuid' => $reg->uuid], [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);

        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
    }

    /**
     * Test that generate-access0key fails with a miss-matched client-id
     *
     * @group Feature
     * @group PartnerLoginAccessKey
     */
    public function testCreateFailsWithInvalidClientType()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg = Registrant::find($user->account_id);
        $reg->client_type_id = ClientType::APPLE_ID;
        $reg->uuid = strtoupper(Uuid::uuid4()->toString());
        $reg->save();

        $clientType = ClientType::find(ClientType::WEB_ID);
        $response = $this->call('POST', '/api/partner/generate-access-key', ['registrant_uuid' => $reg->uuid], [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);

        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
    }

    /**
     * Check that the validateKey method correctly validates and returns a token-set
     *
     * @group Feature
     * @group PartnerLoginAccessKey
     */
    public function testValidateKey()
    {
        Carbon::setTestNow(Carbon::now());
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg = Registrant::find($user->account_id);
        $reg->client_type_id = ClientType::APPLE_ID;
        $reg->uuid = strtoupper(Uuid::uuid4()->toString());
        $reg->save();

        $key = factory(PartnerLoginAccessKey::class)->create(['registrant_id' => $reg->id]);
        $this->assertFalse($key->isExpired());
        $clientType = ClientType::find(ClientType::WEB_ID);
        $response = $this->call('POST', '/api/partner/validate-access-key', ['access_key' => $key->key], [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource("Registration"));
        $this->seeInDatabase('partner_login_access_keys', ['key' => $key->key, 'status_id' => AccessKeyEnum::STATUS_USED]);
    }

    /**
     * Test that the validateKey method rejects keys that have been used
     *
     * @group Feature
     * @group PartnerLoginAccessKey
     */
    public function testValidateKeyFailsWithUsedKey()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg = Registrant::find($user->account_id);
        $reg->client_type_id = ClientType::APPLE_ID;
        $reg->uuid = strtoupper(Uuid::uuid4()->toString());
        $reg->save();

        $clientType = ClientType::find(ClientType::WEB_ID);
        $key = factory(PartnerLoginAccessKey::class)->create(['registrant_id' => $reg->id]);
        $key->tap();

        $response = $this->call('POST', '/api/partner/validate-access-key', ['access_key' => $key->key], [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
    }

    /**
     * Test that the validateKey method rejects keys that have expired
     *
     * @group Feature
     * @group PartnerLoginAccessKey
     */
    public function testValidateKeyFailsWithExpiredKey()
    {
        $user = factory(User::class)->states(['registrant', 'enabled'])->create();
        $reg = Registrant::find($user->account_id);
        $reg->client_type_id = ClientType::APPLE_ID;
        $reg->uuid = strtoupper(Uuid::uuid4()->toString());
        $reg->save();

        $clientType = ClientType::find(ClientType::WEB_ID);
        $key = factory(PartnerLoginAccessKey::class)->create(['registrant_id' => $reg->id]);
        $key->created_at = Carbon::parse($key->created_at)->subMinutes(AccessKeyEnum::PARTNER_LOGIN_ACCESS_EXPIRES_MINUTES + 10)->timestamp;
        $key->save();
        $key->fresh();
        $this->assertTrue($key->isExpired());

        $response = $this->call('POST', '/api/partner/validate-access-key', ['access_key' => $key->key], [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());

    }

}
