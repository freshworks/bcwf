<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;

class AccountControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }

    /**
     * Test that an authenticated user can get their account
     *
     * @group Feature
     * @group Account
     */
    public function testShow()
    {
        $user = factory(\App\Models\User::class)->states(['admin', 'enabled'])->create();
        $this->actingAs($user);

        $response = $this->call('GET', '/api/account');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure( StructureFactory::resource("Account") );
        $this->seeJson(['id' => $user->id, 'type_id' => $user->type_id]);
    }

}
