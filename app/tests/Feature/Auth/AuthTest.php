<?php

namespace Tests\Feature\Auth;

use TestCase;
use Symfony\Component\HttpFoundation\Response AS ResponseCode;
use Tests\Structure\StructureFactory;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Enums\ {RolesEnum, UserStatusEnum};
use App\Models\ {Admin, User};

/**
 * Test the access and data-quality of the Login/oAuthToken Controller and Data.
 */
class AuthTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $this->stack = [
            'user.admin' => factory(\App\Models\User::class)->states(['admin', 'enabled'])->create(),
            'user.registrant' => factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create(),
            'input.data' => [
                'username' => 'username',
                'password' => 'password',
                'scope'    => '*',
                'grant_type' => 'password',
                'client_id' => env('CLIENT_ID'),
                'client_secret' => env('CLIENT_SECRET')
            ],
            'input.registrant' => [
                'first_name'    => 'first_name',
                'last_name'     => 'last_name',
                'province_id'   => 2,
                'date_of_birth' => '1969-01-01',
                'health_care' =>    "1234567890ab",
                'drivers_license' => "1234567890abcd",
                'military_services' => "1234567890ab",
            ]
        ];
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);

    }

    /**
     * Tests getting an auth-token via the login process (Non-Registrants)
     *
     * @group Feature
     * @group Auth
     * @group NonRegistrantLogin
     */
    public function testLogin()
    {
        $input = [
            'username' => $this->stack['user.admin']->email,
            'password' => 'password'
        ];
        $response = $this->call("POST", "/oauth/login", $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::make('Token'));

    }

    /**
     * Test that the login-event correctly sets the last_login_at datestamp
     *
     * @group Feature
     * @group Auth
     * @group NonRegistrantLogin
     */
    public function testLoginSetsLoginDate()
    {
        \Carbon::setTestNow(\Carbon::now());
        $admin = factory(\App\Models\Admin::class)->states(\App\Enums\RolesEnum::SYSTEM_ADMIN)->create();
        $this->assertNull($admin->userRecord->last_login_at);
        $input = [
            'username' => $admin->userRecord->email,
            'password' => 'password'
        ];
        $response = $this->call("POST", "/oauth/login", $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::make('Token'));
        $this->assertEquals($admin->userRecord->fresh()->last_login_at, \Carbon::now());
    }

    /**
     * Tests getting an auth-token
     *
     * @group Feature
     * @group Auth
     */
    public function testToken()
    {
        $user = $this->stack['user.admin'];
        $input = $this->stack['input.data'];
        $input['username'] = $user->email;

        $response = $this->call("POST", "/oauth/token", $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::make('Token'));

    }

    /**
     * Test that login fails with bad oauth-data
     *
     * @group Feature
     * @group Auth
     * @group NonRegistrantLogin
     */
    public function testLoginWithBadOauthData()
    {
        $input = $this->stack['input.data'];
        $input['username'] = $this->stack['user.admin']->email;

        $inputBad = array_merge($input, [
            'grant_type' => "Foobar McNancy Pants"
        ]);
        $response = $this->call("POST", "/oauth/token", $inputBad);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());

        $inputBad = array_merge($input, [
            'client_secret' => 'foooooobarred'
        ]);
        $response = $this->call("POST", "/oauth/token", $inputBad);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());

    }

    /**
     * Test that login fails with missing data
     *
     * @group Feature
     * @group Auth
     * @group NonRegistrantLogin
     */
    public function testLoginFailsWithMissingData()
    {
        $input = $this->stack['input.data'];
        $input['username'] =$this->stack['user.admin']->email;
        unset($input['username']);
        unset($input['password']);
        $response = $this->call("POST", "/oauth/token", $input);
        $this->assertEquals(ResponseCode::HTTP_BAD_REQUEST, $response->status());
    }

    /**
     * Test that login fails with bad-data
     *
     * @group Feature
     * @group Auth
     * @group NonRegistrantLogin
     */
    public function testLoginFailsWithBadData()
    {
        $input = $this->stack['input.data'];
        $input['username'] =$this->stack['user.admin']->email;
        $inputBad = array_merge($input, [
            'password' => "Foobar McNancy Pants"
        ]);
        $response = $this->call("POST", "/oauth/token", $inputBad);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
    }

    /**
     * Test that login fails when the user is not found
     *
     * @group Feature
     * @group Auth
     * @group NonRegistrantLogin
     */
    public function testLoginFailsWithBadUserData()
    {
        $userEmail  = 'foobarmcnancypants'.uniqid().'@foobar.com';
        $this->missingFromDatabase('users', ['email' => $userEmail]);
        $input = [
            'username' => $userEmail,
            'password' => 'password'
        ];

        $response = $this->call("POST", "/oauth/login", $input);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
    }

    /**
     * Test that the registrant-login process works as expected
     *
     * @group Feature
     * @group Auth
     * @group RegistrantLogin
     * @group HealthCare
     * @group DriversLicense
     * @group MilitaryServices
     */
    public function testRegistrantLogin()
    {
        $user = $this->stack['user.registrant'];
        $reg  = \App\Models\Registrant::find($user->account_id)->activeRecord();

        $input = [
            'first_name'    => $reg->first_name,
            'last_name'     => $reg->last_name,
            'sex'           => $reg->sex,
            'province_id'   => $reg->province_id,
            'date_of_birth' => $reg->date_of_birth,
            'health_care'   => $reg->health_care,
            'drivers_license' => $reg->drivers_license,
            'military_services' => $reg->military_services
        ];
        $response = $this->call("POST", "/oauth/registrant-login", $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::make('Token'));
    }

    /**
     * Test that the registrant-login process works as expected with mixed input
     *
     * @group Feature
     * @group Auth
     * @group RegistrantLogin
     * @group HealthCare
     * @group DriversLicense
     * @group MilitaryServices
     */
    public function testRegistrantLoginWithMixedInput()
    {
        $data = $this->validMixedRegistrantLoginDataProvider();
        foreach($data AS $label => $src) {
            $response = $this->call("POST", "/oauth/registrant-login", $src['input']);
            $this->assertEquals($src['code'], $response->status(), $label);
        }
    }

    /**
     * Test that the registrant login fails when provided incorrect values
     *
     * @group Feature
     * @group Auth
     * @group RegistrantLogin
     * @group HealthCare
     * @group DriversLicense
     * @group MilitaryServices
     */
    public function testRegistrantLoginFailsWithIncorrectData()
    {
        config(['local.disable_throttle' => true]);
        $data = $this->invalidRegistrantLoginDataProvider();
        foreach($data AS $label => $src) {
            $response = $this->call("POST", "/oauth/registrant-login", $src['input']);
            $this->assertEquals($src['code'], $response->status(), $label);
        }
    }

    /**
     * Test that the logout functionality correctly invalidates a token.
     *
     * @group Feature
     * @group Auth
     * @group Logout
     */
    public function testLogout()
    {
        $user = $this->stack['user.registrant'];
        $reg  = \App\Models\Registrant::find($user->account_id)->activeRecord();

        $input = [
            'first_name'    => $reg->first_name,
            'last_name'     => $reg->last_name,
            'sex'           => $reg->sex,
            'province_id'   => $reg->province_id,
            'date_of_birth' => $reg->date_of_birth,
            'health_care'   => $reg->health_care,
            'drivers_license' => $reg->drivers_license,
            'military_services' => $reg->military_services
        ];
        $response = $this->call("POST", "/oauth/registrant-login", $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $token = json_decode($response->content())->access_token;

        $response = $this->call("GET", "/oauth/tokens", [], [], [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $response = $this->call("DELETE", "/oauth/logout", [], [], [], ['HTTP_Authorization' => 'Bearer ' . $token]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status(), "Failed Logging Out");

    }

    /**
     * Test that the registrantExists functionality correctly returns [success => true]
     *
     * @group Feature
     * @group Auth
     * @group RegistrantExists
     */
    public function testRegistrantExists()
    {
        $user = $this->stack['user.registrant'];
        $reg  = \App\Models\Registrant::find($user->account_id)->activeRecord();
        $input = [
            'first_name' => $reg->first_name,
            'last_name' => $reg->last_name,
            'health_care' => $reg->health_care,
            'drivers_license' => $reg->drivers_license,
            'military_services' => $reg->military_services
        ];

        $response = $this->call("POST", "/api/registrant-exists", $input);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson([
            'success' => true,
        ]);
    }

    /**
     * Test that the registrantExists functionality correctly returns [success => false]
     *
     * @group Feature
     * @group Auth
     * @group RegistrantExists
     */
    public function testRegistrantDoesNotExist()
    {
        $input = [
            'first_name' => 'first',
            'last_name' => 'last',
            'health_care' => '1111111111',
        ];
        $this->missingFromDatabase('registrant_records', ['first_name' => 'first', 'last_name' => 'last', 'sex' => 'other' ]);

        $response = $this->call("POST", "/api/registrant-exists", $input);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson([
            'success' => false,
        ]);
    }

    /**
     * Test that the registrantExists validation works as expected
     *
     * @group Feature
     * @group Auth
     * @group RegistrantExists
     */
    public function testRegistrantExistsWithInvalidInput()
    {
        $input = [
            'first_name' => 'first',
            'last_name' => 'last'
        ];
        $response = $this->call("POST", "/api/registrant-exists", $input);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());

        $input = [
            'first_name' => 'first',
            'last_name' => 'last',
            'health_care' => 111,
        ];
        $response = $this->call("POST", "/api/registrant-exists", $input);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test that the token-refresh method works as expected
     *
     * @group Feature
     * @group Auth
     */
    public function testRefresh()
    {
        $input = [
            'username' => $this->stack['user.admin']->email,
            'password' => 'password'
        ];
        $response = $this->call("POST", "/oauth/login", $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::make('Token'));
        $refresh = json_decode($response->content())->refresh_token;

        $newResponse = $this->call("POST", "/oauth/refresh", ['refresh_token' => $refresh]);
        $this->assertEquals(ResponseCode::HTTP_OK, $newResponse->status());
        $this->seeJsonStructure(StructureFactory::make('Token'));

    }

    /**
     * Test that the token-refresh method fails when the token has been used.
     *
     * @group Feature
     * @group Auth
     */
    public function testRefreshFailsWithBadToken()
    {
        $input = [
            'username' => $this->stack['user.admin']->email,
            'password' => 'password'
        ];
        $response = $this->call("POST", "/oauth/login", $input);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::make('Token'));
        $refresh = json_decode($response->content())->refresh_token;

        // Should work - invalidate the token
        $newResponse = $this->call("POST", "/oauth/refresh", ['refresh_token' => $refresh]);
        $this->assertEquals(ResponseCode::HTTP_OK, $newResponse->status());
        $this->seeJsonStructure(StructureFactory::make('Token'));

        // Should now fail with an expired token message
        $newResponse = $this->call("POST", "/oauth/refresh", ['refresh_token' => $refresh]);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $newResponse->status());
        $this->seeJsonContains(['hint' => "Token has been revoked"]);

        // Should now fail with an expired token message
        $newResponse = $this->call("POST", "/oauth/refresh", ['refresh_token' => $refresh . "foooobar"]);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $newResponse->status());
        $this->seeJsonContains(['hint' => 'Cannot decrypt the refresh token']);
    }

    /**
     * Test that a disabled user cannot login
     *
     * @group Feature
     * @group Auth
     */
    public function testLoginFailsIfUserIsDisabled()
    {
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create();
        $admin->userRecord->update(['status_id' => UserStatusEnum::DISABLED]);

        $input = [
            'username' => $admin->userRecord->email,
            'password' => 'password'
        ];
        $response = $this->call("POST", "/oauth/login", $input);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
    }

    /**
     * Test that a pending user cannot login
     *
     * @group Feature
     * @group Auth
     */
    public function testLoginFailsIfUserIsPending()
    {
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create();
        $admin->userRecord->update(['status_id' => UserStatusEnum::PENDING]);

        $input = [
            'username' => $admin->userRecord->email,
            'password' => 'password'
        ];
        $response = $this->call("POST", "/oauth/login", $input);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
    }






    /**
     * -=- Custom Data Providers ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     * -- Note: Traditional DataProviders within PHPUNIT run before setUp and do
     * not have access to stacks, etc. These custom-data providers not only
     * provide the data-constructs needed, but also are better performing, and
     * still utilize DBTransactions. This method, infact - is only run when
     * explicitly called - like a standard data-provider, but unlike a stack
     * setup in setUp()
     *
     */

    /**
     * CustomDataProvider - Valid (but mixed Registrant Login Data
     *
     * @return array
     */
    public function validMixedRegistrantLoginDataProvider()
    {
        $user = $this->stack['user.registrant'];
        $reg  = \App\Models\Registrant::find($user->account_id)->activeRecord();
        $base = [
            'first_name'    => $reg->first_name,
            'last_name'     => $reg->last_name,
            'sex'           => $reg->sex,
            'province_id'   => $reg->province_id,
            'date_of_birth' => $reg->date_of_birth,
        ];

        $test[] = ['health_care' => $reg->health_care];
        $test[] = ['health_care' => $reg->health_care, 'drivers_license' => $reg->drivers_license];
        $test[] = ['health_care' => $reg->health_care, 'military_services' => $reg->military_services];
        $test[] = ['drivers_license' => $reg->drivers_license];
        $test[] = ['drivers_license' => $reg->drivers_license, 'military_services' => $reg->military_services];
        $test[] = ['military_services' => $reg->military_services];

        foreach($test AS $key => $inStack) {
            $ret["Failed to login-registrant with params:".implode(", ", array_keys($inStack))] = [
                'code' => ResponseCode::HTTP_OK,
                'input'=> array_merge($base, $inStack)
            ];
        }
        return $ret;
    }

    /**
     * Data provider for invalid registrant data.
     *
     * @return array
     */
    public function invalidRegistrantLoginDataProvider()
    {
        $user = $this->stack['user.registrant'];
        $reg  = \App\Models\Registrant::find($user->account_id)->activeRecord();

        $base = [
            'first_name'    => $reg->first_name,
            'last_name'     => $reg->last_name,
            'sex'           => $reg->sex,
            'province_id'   => $reg->province_id,
            'date_of_birth' => $reg->date_of_birth,
            'sex'           => $reg->sex,
            'health_care'   => $reg->health_care,
            'drivers_license' => $reg->drivers_license,
            'military_services' => $reg->military_services
        ];

        // First Name Exceeds String Limit
        $ret['Invalid Param: first_name. Field Exceed Boundry'] = [
            'code' => ResponseCode::HTTP_UNPROCESSABLE_ENTITY,
            'input' => array_merge($base, ['first_name' => str_repeat("foobar", "100")]),
        ];

        // Last Name Exceeds String Limit
        $ret['Invalid Param: last_name. Field Exceed Boundry'] = [
            'code' => ResponseCode::HTTP_UNPROCESSABLE_ENTITY,
            'input' => array_merge($base, ['last_name' => str_repeat("foobar", "100")]),
        ];

        // Province Doesnt Exists in DB
        $ret['Invalid Param: province_id. Value not in database.'] = [
            'code' => ResponseCode::HTTP_UNPROCESSABLE_ENTITY,
            'input' => array_merge($base, ['province_id' => rand(100,999)]),
        ];

        // Province is not integer
        $ret['Invalid Param: province_id. Value not integer.'] = [
            'code' => ResponseCode::HTTP_UNPROCESSABLE_ENTITY,
            'input' => array_merge($base, ['province_id' => "foobar"]),
        ];

        // DoB Not a valid date
        $ret['Invalid Param: date_of_birth. Value not a valid date.'] = [
            'code' => ResponseCode::HTTP_UNPROCESSABLE_ENTITY,
            'input' => array_merge($base, ['date_of_birth' => "foobar-mc-nancy"]),
        ];

        // HealthCare Invalid Format
        $ret['Invalid Param: health_care. Value not a valid health_care..'] = [
            'code' => ResponseCode::HTTP_UNPROCESSABLE_ENTITY,
            'input' => array_merge($base, ['health_care' => str_repeat("foobar", "100")]),
        ];

        // Drivers License Invalid Format
        $ret['Invalid Param: drivers_license. Value not a valid drivers_license..'] = [
            'code' => ResponseCode::HTTP_UNPROCESSABLE_ENTITY,
            'input' => array_merge($base, ['drivers_license' => str_repeat("foobar", "100")]),
        ];

        // Military Service Invalid Format
        $ret['Invalid Param: military_services. Value not a valid military_services'] = [
            'code' => ResponseCode::HTTP_UNPROCESSABLE_ENTITY,
            'input' => array_merge($base, ['military_services' => str_repeat("foobar", "100")]),
        ];

        // First Name Incorrect
        $ret['Invalid Param: first_name. Unauthorized Value.'] = [
            'code' => ResponseCode::HTTP_UNAUTHORIZED,
            'input' => array_merge($base, ['first_name' => "foobar mcnancy-pants"]),
        ];

        // Last Name Incorrect
        $ret['Invalid Param: last_name. Unauthorized Value.'] = [
            'code' => ResponseCode::HTTP_UNAUTHORIZED,
            'input' => array_merge($base, ['last_name' => "foobar mcnancy-pants"]),
        ];

        // Province Retry Closure
        $func = function() use($base) {
            $new = \App\Models\Province::all()->random()->id;
            if($new == $base['province_id']) {
                throw new \Exception("Cannot use same province");
            }
            return $new;
        };

        // Province Incorrect
        $ret['Invalid Param: province_id. Unauthorized Value.'] = [
            'code' => ResponseCode::HTTP_UNAUTHORIZED,
            'input' => array_merge($base,
                    ['province_id' => \App\Facades\Retry::start($func, 12)]),
        ];


        // Sex Retry Closure
        $func = function() use($base) {
            $list = ['male', 'female', 'other'];
            $new  = \Arr::random($list);
            if($new == \Str::lower($base['sex'])) {
                throw new \Exception("Cannot use same sex");
            }
            return $new;
        };

        // Sex Incorrect
        $ret['Invalid Param: sex. Unauthorized Value.'] = [
            'code' => ResponseCode::HTTP_UNAUTHORIZED,
            'input' => array_merge($base,
                    ['sex' => \App\Facades\Retry::start($func, 10)]),
        ];



        // Health Care Incorrect
        $ret['Invalid Param: health_care. Unauthorized Value.'] = [
            'code' => ResponseCode::HTTP_UNAUTHORIZED,
            'input' => array_merge($base, ['health_care' => "foobar"]),
        ];

        // Driverse License Incorrect
        $ret['Invalid Param: drivers_license. Unauthorized Value.'] = [
            'code' => ResponseCode::HTTP_UNAUTHORIZED,
            'input' => array_merge($base, ['drivers_license' => "foobar"]),
        ];

        // Health Care Incorrect
        $ret['Invalid Param: military_services. Unauthorized Value.'] = [
            'code' => ResponseCode::HTTP_UNAUTHORIZED,
            'input' => array_merge($base, ['military_services' => "foobar"]),
        ];

        return $ret;

    }


}
