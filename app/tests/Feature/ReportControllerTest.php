<?php

use Carbon\Carbon;
use Silber\Bouncer\BouncerFacade;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;
use App\Models\ {Admin, RegistrantRecord, Province};
use App\Enums\ {RolesEnum, RegistrantRecordStatusEnum};

class ReportControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }

    /**
     * Test that an authenticated user can retrieve an overview of active registrant records for the current year
     *
     * @group Feature
     * @group Report
     */
    public function testOverview()
    {
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($admin->userRecord);

        $now = Carbon::now();
        $records = RegistrantRecord::where('status_id', '=', RegistrantRecordStatusEnum::ACTIVE);
        $oldCurrentYearRecords = (clone $records)->whereYear('created_at', '=', $now->year)->count();
        
        // Create more current year and previous year records
        $moreCurrentYearRecords = factory(RegistrantRecord::class, rand(3, 9))->states(['active'])->create();
        factory(RegistrantRecord::class, rand(3, 9))->states(['active'])->create([
            'created_at' => (clone $now)->subYear(rand(1, 3))
        ]);

        $expectedGrandTotal     = (clone $records)->count();
        $expectedYearTotal      = count($moreCurrentYearRecords) + $oldCurrentYearRecords;
        $expectedLast15Days     = (clone $records)->whereDate('created_at', '>=', date((clone $now)->subDays(15)))->count();
        $expectedLast30Days     = (clone $records)->whereDate('created_at', '>=', date((clone $now)->subDays(30)))->count();
        $expectedMonthlyAverage = round($expectedYearTotal / $now->month);

        $response = $this->call('GET', '/api/report/overview');
        $content = json_decode($response->getContent());

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('ReportOverview'));
        $this->assertEquals($expectedLast15Days, $content->data->last_15_days);
        $this->assertEquals($expectedLast30Days, $content->data->last_30_days);
        $this->assertEquals($expectedMonthlyAverage, $content->data->monthly_average);
        $this->assertEquals($expectedYearTotal, $content->data->year_total);
        $this->assertEquals($expectedGrandTotal, $content->data->grand_total);
    }

    /**
     * Test that an authenticated user can retrieve an overview of active registrant records for a given range, and optionally sex and/or province id
     *
     * @group Feature
     * @group Report
     */
    public function testRangeStatistics()
    {
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($admin->userRecord);
        
        $startDate = Carbon::now()->subMonth(rand(1, 3))->toDateString();
        $endDate = Carbon::now()->toDateString();
        $sex = 'male';
        $provinceId = Province::all()->random()->id;

        $records = RegistrantRecord::where('status_id', '=', RegistrantRecordStatusEnum::ACTIVE);
        $oldRangeRecords = (clone $records)->whereDate('created_at', '>=', date($startDate))
                            ->whereDate('created_at', '<=', date($endDate))
                            ->where('sex', '=', $sex)
                            ->where('province_id', '=', $provinceId)
                            ->count();

        $startDateRecords = factory(RegistrantRecord::class, rand(3, 9))->states(['active'])->create([
            'created_at' => $startDate,
            'sex' => $sex,
            'province_id' => $provinceId,
        ]);
        $endDateRecords = factory(RegistrantRecord::class, rand(3, 9))->states(['active'])->create([
            'created_at' => $endDate,
            'sex' => $sex,
            'province_id' => $provinceId,
        ]);

        // Create records outside of range
        factory(RegistrantRecord::class, rand(3, 9))->states(['active'])->create([
            'created_at' => (new Carbon($startDate))->subDay(1),
            'sex' => $sex,
            'province_id' => $provinceId,
        ]);
        factory(RegistrantRecord::class, rand(3, 9))->states(['active'])->create([
            'created_at' => (new Carbon($endDate))->addDay(1),
            'sex' => $sex,
            'province_id' => $provinceId,
        ]);

        $expectedRangeTotal = count($startDateRecords) + count($endDateRecords) + $oldRangeRecords;
        $expectedGrandTotal = (clone $records)->count();

        $input = [
            'start_date' => $startDate,
            'end_date' => $endDate,
            'sex' => $sex,
            'province_id' => $provinceId,
        ];
        $response = $this->call('POST', '/api/report/range-statistics', $input);
        $content = json_decode($response->getContent());

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('RangeStatistics'));
        $this->assertEquals($expectedRangeTotal, $content->data->range_total);
        $this->assertEquals($expectedGrandTotal, $content->data->grand_total);
    }

}
