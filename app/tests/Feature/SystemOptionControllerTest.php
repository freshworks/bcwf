<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;
use App\Models\Admin;
use App\Enums\ {AbilitiesEnum, RolesEnum};

class SystemOptionControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }

    /**
     * Test getting a list of provinces
     *
     * @group Feature
     * @group SystemOption
     */
    public function testGetProvinces()
    {
        $response = $this->call('GET', '/api/sysop/provinces');

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection("Province"));
    }

    /**
     * Test getting a list of user types
     *
     * @group Feature
     * @group SystemOption
     */
    public function testUserTypes()
    {
        $response = $this->call('GET', '/api/sysop/user-types');

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection("UserType"));
    }

    /**
     * Test getting a list of organ types
     *
     * @group Feature
     * @group SystemOption
     */
    public function testOrganTypes()
    {
        $response = $this->call('GET', '/api/sysop/organ-types');

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection("OrganType"));
    }

    /**
     * Test that the getRolesForUser correctly filters based on permissions
     *
     * @group Feature
     * @group SystemOption
     * @group AccessControl
     */
    public function testGetRolesForUser()
    {
        $admin = factory(App\Models\Admin::class)->create();
        $this->actingAs($admin->userRecord);

        // Ensure that no abilities are set
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_STAFF));

        $response = $this->call('GET', '/api/sysop/roles-for-user');
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        // Grant Staff-Only
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_STAFF);

        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));

        $response = $this->call('GET', '/api/sysop/roles-for-user');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection("Role"));
        $this->assertCount(1, json_decode($response->getContent())->data);


        // Grant Group-Admin
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_GROUP_ADMIN);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));

        $response = $this->call('GET', '/api/sysop/roles-for-user');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection("Role"));
        $this->assertCount(2, json_decode($response->getContent())->data);


        // Grant System-Admin
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_SYSTEM_ADMIN);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));

        $response = $this->call('GET', '/api/sysop/roles-for-user');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection("Role"));
        $this->assertCount(3, json_decode($response->getContent())->data);
    }

}
