<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;
use App\Models\{PasswordResets, User, Admin, Group};
use App\Mail\PasswordResetEmail;
use App\Enums\ {RolesEnum, AbilitiesEnum, PasswordResetStatusEnum};


class PasswordControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }

     /**
     * Test reset email with valid email address
     *
     * @group Feature
     * @group Password
     */
    public function testResetWithValidEmail()
    {
        \Mail::fake();
        $this->expectsEvents('App\Events\PasswordResetEvent');

        $user = factory(User::class)->create(['email' => 'brian+foobar@freshworks.io']);
        $input = ['email' => $user->email];
        $response = $this->call('POST', '/api/password/reset', $input);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);
    }

    /**
     * Test that you cannot initiate a reset-password process with an invalid email
     *
     * @group Feature
     * @group Password
     */
    public function testResetWithInvalidEmail()
    {
        \Mail::fake();
        $email = 'foobarmcnancypants@freshworks.com';
        $input = ['email' => $email];

        $this->missingFromDatabase('users', $input);

        $response = $this->call('POST', '/api/password/reset', $input);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);
        \Mail::assertSent(PasswordResetEmail::class, 0);
    }

    /**
     * Test token-verify with valid data
     *
     * @group Feature
     * @group Password
     */
    public function testTokenVerifyWithValidData()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        PasswordResets::expireMinutes(30);
        $reset = PasswordResets::create($data);
        $input = [
            'email' => $user->email,
            'token' => $data['token'],
        ];
        $response = $this->call('POST', '/api/password/verify', $input);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);
    }

    /**
     * Test verify-token with invalid data
     *
     * @group Feature
     * @group Password
     */
    public function testTokenVerifyWithInvalidData()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        $reset = PasswordResets::create($data);
        $inputRefresh = [
            'email' => $user->email,
            'token' => $data['token']
        ];

        $input = $inputRefresh;
        $input['email'] = "foobar{$input['email']}";
        $response = $this->call('POST', '/api/password/verify', $input);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());

        $input = $inputRefresh;
        $input['token'] = 'foobar';
        $response = $this->call('POST', '/api/password/verify', $input);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());

        $input = $inputRefresh;
        $input['email'] = "foobar{$input['email']}";
        $input['token'] = "foobar{$input['token']}";
        $response = $this->call('POST', '/api/password/verify', $input);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test that an expired token cannot be validated
     *
     * @group Feature
     * @group Password
     */
    public function testTokenVerifyWithExpiredToken()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        PasswordResets::expireMinutes(-30);
        $reset = PasswordResets::create($data);

        $this->assertTrue($reset->isExpired());

        $input = [
            'email' => $user->email,
            'token' => $data['token'],
        ];
        $response = $this->call('POST', '/api/password/verify', $input);

        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }

    /**
     * Test that a completed token cannot be validated
     *
     * @group Feature
     * @group Password
     */
    public function testTokenVerifyWithUsedToken()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        PasswordResets::expireMinutes(30);
        $reset = PasswordResets::create($data);

        $this->assertFalse($reset->isExpired());

        $reset = PasswordResets::find($reset->id);
        $reset->status_id = PasswordResetStatusEnum::COMPLETED;
        $reset->save();

        $input = [
            'email' => $user->email,
            'token' => $data['token']
        ];
        $response = $this->call('POST', '/api/password/verify', $input);

        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }

    /**
     * Test that the change-password requests works with a valid token.
     *
     * @group Feature
     * @group Password
     */
    public function testChangePasswordWithValidToken()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        $reset = PasswordResets::create($data);

        $this->assertEquals($reset->status_id, PasswordResetStatusEnum::PENDING);

        $input = [
            'email' => $user->email,
            'token' => $data['token'],
            'password' => 'Password1!'
        ];
        $response = $this->call('POST', '/api/password/change', $input);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertEquals($reset->fresh()->status_id, PasswordResetStatusEnum::COMPLETED);
        $this->seeJsonStructure(['success']);

        $userTest = $user->fresh();
        $match = (new \Illuminate\Hashing\BcryptHasher())->check($input['password'], $userTest->password);

        $this->assertTrue($match);
    }

    /**
     * Test that the change password request fails with an invalid token
     *
     * @group Feature
     * @group Password
     */
    public function testChangePasswordWithInvalidToken()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        $reset = PasswordResets::create($data);

        $input = [
            'email' => $user->email,
            'token' => $data['token'].'fooobar',
            'password' => 'Password1!'
        ];
        $response = $this->call('POST', '/api/password/change', $input);

        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test cannot change password with an expired token
     *
     * @group Feature
     * @group Password
     */
    public function testChangePasswordWithExpiredToken()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
            'status_id' => PasswordResetStatusEnum::PENDING,
        ];
        PasswordResets::expireMinutes(-30);
        $reset = PasswordResets::create($data);

        $this->assertTrue($reset->isExpired());

        $input = [
            'email' => $user->email,
            'token' => $data['token'],
            'password' => 'Password1!'
        ];
        $response = $this->call('POST', '/api/password/change', $input);

        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }


    /**
     * Test cannot change password with a used token
     *
     * @group Feature
     * @group Password
     */
    public function testChangePasswordWithUsedToken()
    {
        $user = factory(User::class)->create();
        $data = [
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(32),
        ];
        PasswordResets::expireMinutes(30);
        $reset = PasswordResets::create($data);

        $this->assertFalse($reset->isExpired());

        $reset = PasswordResets::find($reset->id);
        $reset->status_id = PasswordResetStatusEnum::COMPLETED;
        $reset->save();

        $input = [
            'email' => $user->email,
            'token' => $data['token'],
            'password' => 'Password1!',
        ];
        $response = $this->call('POST', '/api/password/change', $input);

        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }


    /**
     * Test that an admin can trigger a password-reset on another admin
     *
     * @group Feature
     * @group PasswordReset
     * @group Admin
     */
    public function testAdminResetPassword()
    {
        \Mail::fake();
        $this->expectsEvents('App\Events\PasswordResetEvent');

        $group = factory(Group::class)->create();
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create(['group_id' => $group->id]);
        $user  = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $group->id]);

        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(Abilitiesenum::MANAGE_STAFF));
        $this->assertEquals($admin->group->id, $user->group->id);

        $this->actingAs($admin->userRecord);

        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);
        $this->seeInDatabase('password_resets', ['email' => $user->email, 'admin_id' => $admin->id]);
    }

    /**
     * Test that an admin can trigger a password-reset on another admin
     *
     * @group Feature
     * @group PasswordReset
     * @group Admin
     */
    public function testAdminResetPasswordAccessControl()
    {
        \Mail::fake();
        $this->expectsEvents('App\Events\PasswordResetEvent');

        $groups = factory(Group::class,2)->create();
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $groups[0]->id]);
        $user = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $groups[0]->id]);

        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP));
        $this->assertFalse($admin->can(Abilitiesenum::MANAGE_SYSTEM_ADMIN));
        $this->assertFalse($admin->can(Abilitiesenum::MANAGE_GROUP_ADMIN));
        $this->assertFalse($admin->can(Abilitiesenum::MANAGE_STAFF));
        $this->assertEquals($admin->group->id, $user->group->id);

        $this->actingAs($admin->userRecord);

        // No Perms
        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        // Within Group
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP);
        $admin->userRecord->allow(AbilitiesEnum::MANAGE_STAFF);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertTrue($user->isA(RolesEnum::STAFF));

        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);

        // Outside Group
        $admin->group_id = $groups[1]->id;
        $admin->save();

        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_USERS_OUTSIDE_GROUP);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_STAFF));
        $this->assertTrue($user->isA(RolesEnum::STAFF));

        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);

        // Group Admin
        $user = factory(Admin::class)->states(RolesEnum::GROUP_ADMIN)->create(['group_id' => $groups[0]->id]);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));
        $this->assertTrue($user->isA(RolesEnum::GROUP_ADMIN));

        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_GROUP_ADMIN);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_GROUP_ADMIN));

        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);

        // System Admin
        $user = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create(['group_id' => $groups[0]->id]);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));
        $this->assertTrue($user->isA(RolesEnum::SYSTEM_ADMIN));

        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $admin->userRecord->allow(AbilitiesEnum::MANAGE_SYSTEM_ADMIN);
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_SYSTEM_ADMIN));

        $response = $this->call('POST', '/api/admin/reset-password', ['admin_id' => $user->id]);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);



    }




}
