<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;

class UnsubscribeControllerTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }

    /**
     * Test that the unsubscribe-validate endpoint works as expected
     * 
     * @group Feature
     * @group UnsubscribeController
     * @group Unsubscribe
     */
    public function testUnsubscribeValidate()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $data = ['key' => $user->getEncryptedIdentifier()];
        
        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => true]);
    }

    /**
     * Test that the unsubscribe-validate endpoint fails when provided an invalid key
     * 
     * @group Feature
     * @group UnsubscribeController
     * @group Unsubscribe
     */
    public function testUnsubscribeValidateFailsWithInvalidKey()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $data = ['key' => encrypt('foobar')];

        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => false]);
        
        $data = ['foobar' => encrypt('key')];
        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test that the unsubscribe-validate endpoint fails when provided an invalid registrant
     * 
     * @group Feature
     * @group UnsubscribeController
     * @group Unsubscribe
     */
    public function testUnsubscribeValidateFailsWithInvalidRegistrant()
    {
        $keyData = ['id' => rand(99999,9999999), 'user_name' => 'foobar'];
        $this->missingFromDatabase('users', ['id' => $keyData['id']]);
        $data = ['key' => encrypt($keyData)];
        
        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => false]);
        
    }

    /**
     * Test that the unsubscribe-validate endpoint fails when provided a non-subscribed registrant
     * 
     * @group Feature
     * @group UnsubscribeController
     * @group Unsubscribe
     */
    public function testUnsubscribeValidateFailsWithNonSubscribedRegistrant()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $data = ['key' => $user->getEncryptedIdentifier()];
        
        $reg = \App\Models\Registrant::find($user->account_id);
        $reg->subscriber = false;
        $reg->save();
        
        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => false]);
    }
    
    /**
     * Test that the unsubscribe-registrant endpoint works as expected
     * 
     * @group Feature
     * @group UnsubscribeController
     * @group Unsubscribe
     */
    public function testUnsubscribeRegistrant()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $data = ['key' => $user->getEncryptedIdentifier()];
        
        $response = $this->call('POST', '/api/unsubscribe/registrant', $data);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => true]);
        
        $this->seeInDatabase('registrants', ['id' => $user->account_id, 'subscriber' => false]);
    }

    /**
     * Test that the unsubscribe-registrant endpoint fails when provided an invalid key
     * 
     * @group Feature
     * @group UnsubscribeController
     * @group Unsubscribe
     */
    public function testUnsubscribeRegistrantFailsWithInvalidKey()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $data = ['key' => encrypt('foobar')];

        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => false]);
        $this->seeInDatabase('registrants', ['id' => $user->account_id, 'subscriber' => true]);
        
        $data = ['foobar' => encrypt('key')];
        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
        $this->seeInDatabase('registrants', ['id' => $user->account_id, 'subscriber' => true]);
       
    }

    /**
     * Test that the unsubscribe-registrant endpoint fails when provided an invalid registrant
     * 
     * @group Feature
     * @group UnsubscribeController
     * @group Unsubscribe
     */
    public function testUnsubscribeRegistrantFailsWithInvalidRegistrant()
    {
        $keyData = ['id' => rand(99999,9999999), 'user_name' => 'foobar'];
        $this->missingFromDatabase('users', ['id' => $keyData['id']]);
        $data = ['key' => encrypt($keyData)];
        
        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => false]);
        
    }

    /**
     * Test that the unsubscribe-registrant endpoint fails when provided a non-subscribed registrant
     * 
     * @group Feature
     * @group UnsubscribeController
     * @group Unsubscribe
     */
    public function testUnsubscribeRegistrantFailsWithNonSubscribedRegistrant()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $data = ['key' => $user->getEncryptedIdentifier()];
        
        $reg = \App\Models\Registrant::find($user->account_id);
        $reg->subscriber = false;
        $reg->save();
        
        $response = $this->call('POST', '/api/unsubscribe/validate', $data);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJson(['success' => false]);
        $this->seeInDatabase('registrants', ['id' => $user->account_id, 'subscriber' => false]);
    }

    
    
    
    


    
}
