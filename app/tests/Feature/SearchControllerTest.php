<?php

use Silber\Bouncer\BouncerFacade;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;
use App\Models\ {Admin, User, Group, Registrant, RegistrantRecord};
use App\Enums\{RolesEnum, UserStatusEnum, AbilitiesEnum};

class SearchControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }

    /**
     * Test that an authenticated user can search for registrant records
     *
     * @group Feature
     * @group Search
     */
    public function testSearchRegistrantRecords()
    {
        $filters = [
            'first_name' => 'First',
            'last_name' => 'Last',
            'sex' => 'male',
        ];
        $filters2 = [
            'first_name' => 'Last',
            'last_name' => 'First',
            'sex' => 'male',
        ];
        factory(RegistrantRecord::class, 10)->states(['active'])->create($filters2);
        $registrantRecords = factory(RegistrantRecord::class, 5)->states(['active'])->create($filters);
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();

        $this->actingAs($admin->userRecord);
        $response = $this->call('POST', '/api/search/registrant-records', $filters);
        $data = json_decode($response->getContent())->data;

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection('RegistrantRecord'));
        $this->assertCount($registrantRecords->count(), $data);
        $this->assertEquals($filters['first_name'], $data[0]->first_name);
        $this->assertNotEquals($filters2['first_name'], $data[0]->first_name);
    }

     /**
     * Test that an authenticated user can search for registrant's active record
     *
     * @group Feature
     * @group Search
     */
    public function testSearchRegistrantRecord()
    {
        $registrant = factory(Registrant::class)->create();
        $record = factory(RegistrantRecord::class)->states(['active'])->create([
            'registrant_id' => $registrant->id,
        ]);
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();

        $this->actingAs($admin->userRecord);
        $response = $this->call('GET', '/api/search/registrant-records/' . $registrant->id);
        $data = json_decode($response->getContent())->data;

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('RegistrantRecord'));
        $this->assertEquals($record->id, $data->id);
        $this->assertEquals($registrant->id, $data->registrant_id);
    }

    /**
     * Test that searching for a non-existent registrant will fail
     *
     * @group Feature
     * @group Search
     */
    public function testSearchRegistrantRecordWithIncorrectId()
    {
        $registrant = factory(Registrant::class)->create();
        $badId = $registrant->id;
        $registrant->delete();
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();

        $this->actingAs($admin->userRecord);
        $response = $this->call('GET', '/api/search/registrant-records/' . $badId);
        $message = json_decode($response->getContent())->message;

        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
        $this->assertEquals(trans('auth.no_registrant'), $message);
    }

    /**
     * Test that an authenticated user can view a registrant's history
     *
     * @group Feature
     * @group Search
     */
    public function testSearchRegistrantHistory()
    {
        $initial = [
            'province_id' => 1,
            'exclude_organ_types' => [1, 2, 3],
        ];
        $update = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->safeEmail(),
            'province_id' => 2,
            'exclude_organ_types' => null,
        ];

        // Create mulitple records
        $registrant = factory(Registrant::class)->create();
        $record = factory(RegistrantRecord::class)->states(['active'])->create(array_merge($initial, [
            'registrant_id' => $registrant->id,
        ]));

        $record2 = factory(RegistrantRecord::class)->states(['active'])->create(array_merge($initial, [
            'registrant_id' => $registrant->id,
            'first_name' => $update['first_name'],
            'last_name' => $update['last_name'],
            'email' => $update['email'],
        ]));
        $previousAttributes = $record2->getAttributes();
        unset($previousAttributes['id']);

        $record3 = factory(RegistrantRecord::class)->states(['active'])->create(array_merge($previousAttributes, [
            'province_id' => $update['province_id'],
            'exclude_organ_types' => $update['exclude_organ_types'],
        ]));

        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();

        $this->actingAs($admin->userRecord);
        $response = $this->call('GET', '/api/search/registrant-history/' . $registrant->id);
        $data = json_decode($response->getContent())->data;

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection('RegistrantHistory'));
        $this->assertCount(3, $data);
        $this->assertEquals('Registered to be a donor', $data[2]->message);
        $this->assertEquals('Updated their province, excluded organ types', $data[0]->message);
        $this->assertEquals($record->created_at, $data[2]->created_at);
        $this->assertEquals($record2->created_at, $data[1]->created_at);
        $this->assertEquals($record3->created_at, $data[0]->created_at);
    }

    /**
     * Test that a collection is still returned with only one record
     *
     * @group Feature
     * @group Search
     */
    public function testSearchRegistrantHistoryWithOneRecord()
    {
        $registrant = factory(Registrant::class)->create();
        $record = factory(RegistrantRecord::class)->states(['active'])->create([
            'registrant_id' => $registrant->id,
        ]);

        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();
        $this->actingAs($admin->userRecord);

        $response = $this->call('GET', '/api/search/registrant-history/' . $registrant->id);
        $data = json_decode($response->getContent())->data;

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection('RegistrantHistory'));
        $this->assertCount(1, $data);
        $this->assertEquals('Registered to be a donor', $data[0]->message);
    }

    /**
     * Test that an error is thrown if there are no records
     *
     * @group Feature
     * @group Search
     */
    public function testSearchRegistrantHistoryWithNoRecords()
    {
        $registrant = factory(Registrant::class)->create();
        $admin = factory(Admin::class)->states([RolesEnum::STAFF])->create();
        $this->actingAs($admin->userRecord);

        $response = $this->call('GET', '/api/search/registrant-history/' . $registrant->id);

        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test that a permitted user can search for admins
     *
     * @group Feature
     * @group Search
     */
    public function testSearchAdmins()
    {
        $admin = factory(Admin::class)->states([RolesEnum::SYSTEM_ADMIN])->create();
        $newGroup = factory(Group::class)->create();
        $newAdmin = factory(Admin::class)->create(['group_id' => $newGroup->id]);
        factory(Admin::class, rand(3, 9))->create(['group_id' => $newGroup->id]);

        $filters = [
            'group_id' => $newGroup->id,
            'search_term' => $newAdmin->first_name,
            'show_all' => false,
        ];
        $filters2 = [
            'search_term' => $newAdmin->email,
            'show_all' => false,
        ];
        $filters3 = [
            'search_term' => $newGroup->name,
            'show_all' => true,
        ];

        $this->actingAs($admin->userRecord);
        $response = $this->call('POST', '/api/search/admins', $filters);
        $data = json_decode($response->getContent())->data;

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection('Admin'));
        $this->assertCount(1, $data);
        $this->assertEquals($newAdmin->id, $data[0]->id);
        $this->assertEquals($newAdmin->first_name, $data[0]->first_name);

        $response2 = $this->call('POST', '/api/search/admins', $filters2);
        $data2 = json_decode($response2->getContent())->data;

        $this->assertEquals(ResponseCode::HTTP_OK, $response2->status());
        $this->seeJsonStructure(StructureFactory::collection('Admin'));
        $this->assertCount(1, $data2);
        $this->assertEquals($newAdmin->id, $data2[0]->id);
        $this->assertEquals($newAdmin->email, $data2[0]->email);

        $response3 = $this->call('POST', '/api/search/admins', $filters3);
        $data3 = json_decode($response3->getContent())->data;
        $expectedCount = Admin::where('group_id', '=', $newGroup->id)->count();

        $this->assertEquals(ResponseCode::HTTP_OK, $response3->status());
        $this->seeJsonStructure(StructureFactory::collection('Admin'));
        $this->assertCount($expectedCount, $data3);
        foreach ($data3 as $admin) {
            $this->assertEquals($newGroup->id, $admin->group->id);
        }
    }

    /**
     * Test that the AccessControlLayer correctly restricts the searchAdmins feature.
     *
     * @group Feature
     * @group Search
     * @group AccessControl
     */
    public function testSearchAdminsAccessControl()
    {
        $group = factory(Group::class)->create();
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $group->id]);
        $this->assertFalse($admin->can(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP));
        $this->actingAs($admin->userRecord);

        $params = [
            'none' => [
                'search_term' => 'FooBar',
                'show_all' => false,
            ],
            'other' => [
                'group_id'    => $group->id - 1,
                'search_term' => 'FooBar',
                'show_all' => false,
            ],
            'match' => [
                'group_id' => $group->id,
                'search_term' => 'FooBar',
                'show_all' => false,
            ],
        ];

        // ---- Test with no applicable perms
        // no groupId passed should fail.
        $response = $this->call('POST', '/api/search/admins', $params['none']);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        // other groupId should fail
        $response = $this->call('POST', '/api/search/admins', $params['other']);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        // matching groupId should fail
        $response = $this->call('POST', '/api/search/admins', $params['match']);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());


        // ---- Add the 'search-outside' ability
        $admin->userRecord->allow(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP);
        $admin->userRecord->disallow(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP);
        $this->assertFalse($admin->can(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP));
        $this->actingAs($admin->userRecord);

        // no groupId passed should fail.
        $response = $this->call('POST', '/api/search/admins', $params['none']);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        // other groupId should pass
        $response = $this->call('POST', '/api/search/admins', $params['other']);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        // matching groupId should fail
        $response = $this->call('POST', '/api/search/admins', $params['match']);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());


        // ---- Add the 'search-within' ability
        $admin->userRecord->allow(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP);
        $admin->userRecord->disallow(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP);
        $this->assertTrue($admin->can(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP));
        $this->assertFalse($admin->can(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP));
        $this->actingAs($admin->userRecord);

        // no groupId passed should fail.
        $response = $this->call('POST', '/api/search/admins', $params['none']);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        // other groupId should fail
        $response = $this->call('POST', '/api/search/admins', $params['other']);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        // matching groupId should pass
        $response = $this->call('POST', '/api/search/admins', $params['match']);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());


        // ---- And with both applicable perms and admin is a system-admin
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create(['group_id' => $group->id]);
        $this->assertTrue($admin->isA(RolesEnum::SYSTEM_ADMIN));
        $this->assertTrue($admin->can(AbilitiesEnum::SEARCH_USERS_WITHIN_GROUP));
        $this->assertTrue($admin->can(AbilitiesEnum::SEARCH_USERS_OUTSIDE_GROUP));
        $this->actingAs($admin->userRecord);

        // no GroupId passed should pass.
        $response = $this->call('POST', '/api/search/admins', $params['none']);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        // other groupId should pass
        $response = $this->call('POST', '/api/search/admins', $params['other']);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        // matching groupId should pass
        $response = $this->call('POST', '/api/search/admins', $params['match']);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
    }

}
