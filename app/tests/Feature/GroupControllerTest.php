<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use Tests\Structure\StructureFactory;
use App\Models\ {
    Admin, Group, User, Province
};
use App\Enums\ {RolesEnum, AbilitiesEnum, UserStatusEnum};

class GroupControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        config(['local.disable_throttle' => true, 'local.disable_client_enforcement' => true]);
    }

    /**
     * Test that an authenticated user can create a group
     *
     * @group Feature
     * @group Group
     */
    public function testCreate()
    {
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->assertTrue($admin->can(AbilitiesEnum::MANAGE_OTHER_GROUP));
        $this->actingAs($admin->userRecord);

        $payload = [
            'name' => $this->faker->company(),
            'province_id' => Province::all()->random()->id,
            'address' => $this->faker->address(),
            'website' => $this->faker->url(),
            'phone' => $this->faker->phoneNumber(),
        ];

        $response = $this->call('POST', '/api/group', $payload);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);
    }

    /**
     * Test that groups cannot be created by non-system-admins
     *
     * @group Feature
     * @group Group
     */
    public function testCreateFailsIfNotAuthorized()
    {
        $list = [
            RolesEnum::GROUP_ADMIN =>factory(Admin::class)->states(RolesEnum::GROUP_ADMIN)->create(),
            RolesEnum::STAFF => factory(Admin::class)->states(RolesEnum::STAFF)->create()
        ];
        $payload = [
            'name' => $this->faker->company(),
            'province_id' => Province::all()->random()->id,
            'address' => $this->faker->address(),
            'website' => $this->faker->url(),
            'phone' => $this->faker->phoneNumber(),
        ];

        foreach($list AS $role => $admin) {
            $this->actingAs($admin->userRecord);
            $response = $this->call('POST', '/api/group', $payload);
            $this->assertFalse($admin->can(AbilitiesEnum::MANAGE_OTHER_GROUP));
            $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
        }
    }

    /**
     * Test that groups can be shown
     *
     * @group Feature
     * @group Group
     */
    public function testShow()
    {
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($admin->userRecord);

        $id = Group::all()->random()->id;
        $response = $this->call('GET', '/api/group/'.$id);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('Group'));
        $this->seeJson(['id' => $id]);

    }

    /**
     * Test that non-existant groups fail when requested.
     *
     * @group Feature
     * @group Group
     */
    public function testShowFailsWithBadGroup()
    {
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($admin->userRecord);

        $id = rand(99999,9999999);
        $this->missingFromDatabase('groups', ['id' => $id]);

        $response = $this->call('GET', '/api/group/' . $id);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test that updates work as expected
     *
     * @group Feature
     * @group Group
     */
    public function testEdit()
    {
        $group = factory(Group::class)->create();

        $sysAdmin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($sysAdmin->userRecord);
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_OWN_GROUP));
        $this->assertTrue($sysAdmin->can(AbilitiesEnum::MANAGE_OTHER_GROUP));

        $payload = [
            'group_id' => $group->id,
            'name' => $this->faker->company(),
            'province_id' => Province::all()->random()->id,
        ];
        $response = $this->call('POST', '/api/group/edit', $payload);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);
        $this->assertEquals($payload['name'], $group->fresh()->name);

        $groupAdmin = factory(Admin::class)->states(RolesEnum::GROUP_ADMIN)->create(['group_id' => $group->id]);
        $this->actingAs($groupAdmin->userRecord);
        $this->assertTrue($groupAdmin->can(AbilitiesEnum::MANAGE_OWN_GROUP));
        $this->assertFalse($groupAdmin->can(AbilitiesEnum::MANAGE_OTHER_GROUP));

        $payload = [
            'group_id' => $group->id,
            'name' => $this->faker->company(),
            'province_id' => Province::all()->random()->id,
        ];
        $response = $this->call('POST', '/api/group/edit', $payload);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(['success']);
        $this->assertEquals($payload['name'], $group->fresh()->name);
    }

    /**
     * Test that groups cannot be updated by non-system-admins
     *
     * @group Feature
     * @group Group
     */
    public function testEditFailsIfNotAuthorized()
    {
        $list = [
            RolesEnum::GROUP_ADMIN => factory(Admin::class)->states(RolesEnum::GROUP_ADMIN)->create(),
            RolesEnum::STAFF => factory(Admin::class)->states(RolesEnum::STAFF)->create()
        ];
        $group = factory(Group::class)->create();
        $payload = [
            'group_id' => $group->id,
            'name' => $this->faker->company(),
            'province_id' => Province::all()->random()->id
        ];

        foreach ($list as $role => $admin) {
            $this->actingAs($admin->userRecord);
            $response = $this->call('POST', '/api/group/edit', $payload);
            $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
        }
    }

    /**
     * Test that update fails if the group doesn't exist
     *
     * @group Feature
     * @group Group
     */
    public function testEditFailsWithBadGroup()
    {
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($admin->userRecord);

        $id = rand(99999, 9999999);
        $this->missingFromDatabase('groups', ['id' => $id]);

        $payload = [
            'group_id' => $id,
            'name' => $this->faker->company(),
            'province_id' => Province::all()->random()->id
        ];
        $response = $this->call('POST', '/api/group/edit', $payload);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());
    }

    /**
     * Test that all the groups can be returned
     *
     * @group Feature
     * @group Group
     */
    public function testAll()
    {
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($admin->userRecord);

        $expects = Group::all()->count();
        $response = $this->call('GET', '/api/groups');

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection('Group'));

        $content = json_decode($response->getContent());
        $this->assertEquals(count($content->data), $expects);
    }

    /**
     * Test that the list-all-groups endpoint fails without the correct ability
     *
     * @group Feature
     * @group Group
     * @group AccessControl
     */
    public function testAllFailsWithoutAbility()
    {
        $admin = factory(Admin::Class)->states(RolesEnum::GROUP_ADMIN)->create();
        $this->assertFalse($admin->can(AbilitiesEnum::SEARCH_OTHER_GROUP));
        $this->actingAs($admin->userRecord);

        $response = $this->call('GET', '/api/groups');
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }

    /**
     * Test that all the group reports can be returned
     *
     * @group Feature
     * @group Group
     */
    public function testReportList()
    {
        $newGroup = factory(Group::class)->create();
        factory(Admin::class, rand(3, 9))->create(['group_id' => $newGroup->id]);
        $newGroupReport = [
            'roles' => [
                RolesEnum::STAFF => 0,
                RolesEnum::GROUP_ADMIN => 0,
            ],
            'status' => [
                UserStatusEnum::DISABLED => 0,
                UserStatusEnum::ACTIVE => 0,
                UserStatusEnum::PENDING => 0,
            ],
        ];
        foreach($newGroup->admins as $admin) {
            $role = rand(1, 2) == 1 ? RolesEnum::STAFF : RolesEnum::GROUP_ADMIN;
            $userRecord = $admin->userRecord;

            $userRecord->retract(RolesEnum::STAFF);
            $userRecord->assign($role);
            $admin->setRole($role);
            $admin->save();
            $newGroupReport['roles'][$admin->meta->role]++;

            $userRecord->status_id = rand(UserStatusEnum::DISABLED, UserStatusEnum::PENDING);
            $userRecord->save();
            $newGroupReport['status'][$userRecord->status_id]++;
        }

        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($admin->userRecord);

        $response = $this->call('GET', '/api/groups/report');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection('GroupReport'));

        $content = json_decode($response->getContent());
        $this->assertEquals(count($content->data), Group::all()->count());

        $responseGroup = current(array_filter($content->data, function($group) use($newGroup) {
            return $group->id == $newGroup->id;
        }));

        $this->assertEquals($newGroup->id, $responseGroup->id);
        $this->assertEquals($newGroup->name, $responseGroup->name);
        $this->assertEquals($newGroupReport['roles'][RolesEnum::GROUP_ADMIN], $responseGroup->admin_count);
        $this->assertEquals($newGroupReport['roles'][RolesEnum::STAFF], $responseGroup->staff_count);
        $this->assertEquals($newGroupReport['status'][UserStatusEnum::DISABLED], $responseGroup->inactive_count);
        $this->assertEquals($newGroupReport['status'][UserStatusEnum::ACTIVE], $responseGroup->active_count);
        $this->assertEquals($newGroupReport['status'][UserStatusEnum::PENDING], $responseGroup->pending_count);
    }

    /**
     * Test that the group-report endpoint fails without the correct permission
     *
     * @group Feature
     * @group Group
     * @group AccessControl
     */
    public function testReportListFailsWithoutAbility()
    {
        $admin = factory(Admin::class)->states(RolesEnum::GROUP_ADMIN)->create();
        $this->assertFalse($admin->can(AbilitiesEnum::SEARCH_OTHER_GROUP));
        $this->actingAs($admin->userRecord);

        $response = $this->call('GET', '/api/groups/report');
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }

    /**
     * Test that a permitted user can get a group report
     *
     * @group Feature
     * @group Group
     */
    public function testReport()
    {
        $newGroup = factory(Group::class)->create();
        factory(Admin::class, rand(3, 9))->create(['group_id' => $newGroup->id]);
        $newGroupReport = [
            'roles' => [
                RolesEnum::STAFF => 0,
                RolesEnum::GROUP_ADMIN => 0,
            ],
            'status' => [
                UserStatusEnum::DISABLED => 0,
                UserStatusEnum::ACTIVE => 0,
                UserStatusEnum::PENDING => 0,
            ],
        ];
        foreach($newGroup->admins as $admin) {
            $role = rand(1, 2) == 1 ? RolesEnum::STAFF : RolesEnum::GROUP_ADMIN;
            $userRecord = $admin->userRecord;

            $userRecord->retract(RolesEnum::STAFF);
            $userRecord->assign($role);
            $admin->setRole($role);
            $admin->save();
            $newGroupReport['roles'][$admin->meta->role]++;

            $userRecord->status_id = rand(UserStatusEnum::DISABLED, UserStatusEnum::PENDING);
            $userRecord->save();
            $newGroupReport['status'][$userRecord->status_id]++;
        }

        $sysAdmin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($sysAdmin->userRecord);

        $response = $this->call('GET', '/api/groups/report/' . $newGroup->id);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('GroupReport'));

        $content = json_decode($response->getContent())->data;
        $this->assertEquals($newGroup->id, $content->id);
        $this->assertEquals($newGroup->name, $content->name);
        $this->assertEquals($newGroupReport['roles'][RolesEnum::GROUP_ADMIN], $content->admin_count);
        $this->assertEquals($newGroupReport['roles'][RolesEnum::STAFF], $content->staff_count);
        $this->assertEquals($newGroupReport['status'][UserStatusEnum::DISABLED], $content->inactive_count);
        $this->assertEquals($newGroupReport['status'][UserStatusEnum::ACTIVE], $content->active_count);
        $this->assertEquals($newGroupReport['status'][UserStatusEnum::PENDING], $content->pending_count);

        $groupAdmin = factory(Admin::class)->states(RolesEnum::GROUP_ADMIN)->create(['group_id' => $newGroup->id]);
        $this->actingAs($groupAdmin->userRecord);

        $response = $this->call('GET', '/api/groups/report/' . $newGroup->id);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::resource('GroupReport'));

        $groupAdmin = factory(Admin::class)->states(RolesEnum::GROUP_ADMIN)->create();
        $this->actingAs($groupAdmin->userRecord);

        $response = $this->call('GET', '/api/groups/report/' . $newGroup->id);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());

        $staff = factory(Admin::class)->states(RolesEnum::STAFF)->create(['group_id' => $newGroup->id]);
        $this->actingAs($staff->userRecord);

        $response = $this->call('GET', '/api/groups/report/' . $newGroup->id);
        $this->assertEquals(ResponseCode::HTTP_FORBIDDEN, $response->status());
    }

    /**
     * Test that the 'listForUser' method returns the full list with the correct permissions
     *
     * @group Feature
     * @group Group
     * @group AccessControl
     */
    public function testListForUserReturnsAllWhenAuthorized()
    {
        $admin = factory(Admin::class)->states(RolesEnum::SYSTEM_ADMIN)->create();
        $this->actingAs($admin->userRecord);
        $this->assertTrue($admin->can(AbilitiesEnum::SEARCH_OTHER_GROUP));

        $response = $this->call('GET', '/api/groups/list-for-user');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection('Group'));

        $all = Group::all();
        $list = json_decode($response->getContent());
        $this->assertEquals($all->count(), collect($list->data)->count());

    }

    /**
     * Test that the 'listForUser' method returns the filtered list without the correct permissions
     *
     * @group Feature
     * @group Group
     * @group AccessControl
     */
    public function testListForUserReturnsFilteredWhenNotAuthorized()
    {
        $admin = factory(Admin::class)->states(RolesEnum::STAFF)->create();
        $this->actingAs($admin->userRecord);
        $this->assertFalse($admin->can(AbilitiesEnum::SEARCH_OTHER_GROUP));

        $response = $this->call('GET', '/api/groups/list-for-user');
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure(StructureFactory::collection('Group'));

        $list = json_decode($response->getContent());
        $this->assertEquals(1, collect($list->data)->count());

    }


}
