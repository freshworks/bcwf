<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response as ResponseCode;
use App\Models\Registrant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use Tests\Structure\StructureFactory;
use App\Enums\RegistrantRecordStatusEnum;

class RegistrationControllerTest extends TestCase
{
    use DatabaseTransactions;

    private $payload;


    /**
     * @inheritdoc
     */
    public function setUp() {
        parent::setUp();
        $this->payload = [
            'province_id' => 1,
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'sex' => 'Male',
            'date_of_birth' => $this->faker->date(),
            'health_care' => '123456789012',
            'drivers_license' => '12345678901234',
            'military_services' => '123456789012',
            'city' => $this->faker->city,
            'address' => $this->faker->address,
            'postal_code' => $this->faker->postcode,
            'email' => $this->faker->safeEmail,
            'exclude_organ_types' => null,
            'wishes' => $this->faker->sentence(12),
            'subscriber' => true,
        ];
        config(['local.disable_throttle' => true,
                'local.disable_client_enforcement' => true,
                'local.disable_rak_enforcement' => true
            ]);
    }

    /**
     * Test that anyone can post to /api/register and sends an email
     *
     * @group Feature
     * @group Register
     */
    public function testCreateRegistrant()
    {
        \Mail::fake();
        $response = $this->call('POST', '/api/register', $this->payload);
        $this->seeJsonStructure(StructureFactory::resource("Registration"));
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        \Mail::assertSent(App\Mail\ThankYouEmail::class, 1);
    }

    /**
     * Test that anyone can post to /api/register and sends an email
     *
     * @group Feature
     * @group Register
     * @group Partner
     */
    public function testCreateRegistrantFromPartner()
    {
        \Mail::fake();
        $payload = $this->payload;
        unset($payload['province_id']);
        $payload['province'] = 'bc';
        $response = $this->call('POST', '/api/partner/register', $payload);
        $this->seeJsonStructure(StructureFactory::resource('RegistrantUuid'));
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        \Mail::assertSent(App\Mail\ThankYouEmail::class, 1);
    }

    /**
     * Test that when calling partner registration again it returns the same UUID
     *
     * @group Feature
     * @group Register
     * @group Partner
     */
    public function testCreateRegistrantFromPartnerWithSameDataReturnsSameUuid()
    {
        \Mail::fake();
        $payload = $this->payload;
        unset($payload['province_id']);
        $payload['province'] = 'bc';
        $response = $this->call('POST', '/api/partner/register', $payload);
        $this->seeJsonStructure(StructureFactory::resource('RegistrantUuid'));
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        \Mail::assertSent(App\Mail\ThankYouEmail::class, 1);

        $expected = json_decode($response->getContent())->data->registrant_uuid;

        $response = $this->call('POST', '/api/partner/register', $payload);
        $this->seeJsonStructure(StructureFactory::resource('RegistrantUuid'));
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());

        $actual = json_decode($response->getContent())->data->registrant_uuid;
        $this->assertEquals($expected, $actual);

        // Should have 2 now - We may want to change this later ?
        \Mail::assertSent(App\Mail\ThankYouEmail::class, 2);
    }

    /**
     * Test that the PartnerRegister method fails when providing a non-partner app-key
     *
     * @group Feature
     * @group Register
     * @group Partner
     */
    public function testCreateRegistrationFromPartnerFailsWithWrongPartnerKey()
    {
        config(['local.disable_client_enforcement' => false]);
        \Mail::fake();

        $clientType = \App\Models\ClientType::find(\App\Models\ClientType::WEB_ID);
        $payload = $this->payload;
        unset($payload['province_id']);
        $payload['province'] = 'bc';

        $response = $this->call('POST', '/api/partner/register', $payload, [], [], ['HTTP_X-Application-Id' => $clientType->uuid]);
        $this->assertEquals(ResponseCode::HTTP_UNAUTHORIZED, $response->status());
        \Mail::assertSent(App\Mail\ThankYouEmail::class, 0);
    }

    /**
     * Test that anyone can post to /api/register and not receive an email
     *
     * @group Feature
     * @group Register
     */
    public function testCreateUnsubscribedRegistrantDoesNotSendEmail()
    {
        \Mail::fake();
        $payload = $this->payload;
        $payload['subscriber'] = false;
        $response = $this->call('POST', '/api/register', $payload);

        $this->seeJsonStructure(StructureFactory::resource("Registration"));
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        \Mail::assertSent(App\Mail\ThankYouEmail::class, 0);
    }

    /**
     * Test a database failure does not create a new Registrant and RegistrantRecord
     *
     * @group Feature
     * @group Register
     * @runInSeparateProcess
     * @preserveGlobalState disabled
   */
    public function testRegisterDBFailureRollsBack()
    {
        $registrantCount = Registrant::all()->count();
        $registrantRecordMock = \Mockery::mock('overload:\App\Models\RegistrantRecord')->makePartial();
        $registrantRecordMock
            ->shouldReceive('create')
            ->andThrow(new \Exception('Failed to create registrant'))
            ->once();
        $this->app->instance('\App\Models\RegistrantRecord', $registrantRecordMock);

        $response = $this->call('POST', '/api/register', $this->payload);

        $this->assertEquals(ResponseCode::HTTP_INTERNAL_SERVER_ERROR, $response->status());
        $this->assertEquals(Registrant::all()->count(), $registrantCount);

        $registrantRecord = DB::table('registrant_records')
            ->where('email', $this->payload['email'])
            ->first();
        $this->assertEquals($registrantRecord, null);
    }

    /**
     * Test that when registering with an existing account it updates as expected
     *
     * @group Feature
     * @group Register
     */
    public function testRegistrationWithExistingAccount()
    {
        \Mail::fake();
        // Remove subscriber key from payload
        $registrantRecord = array_diff_assoc($this->payload, ['subscriber' => true]);
        $this->assertNull(App\Facades\AccountManager::registrantExists($registrantRecord));
        $existing = factory(App\Models\RegistrantRecord::class)->states('active')->create($registrantRecord);
        $user   = factory(App\Models\User::class)->states(['registrant', 'enabled'])->create(['account_id' => $existing->registrant_id]);
        $this->assertInstanceOf(App\Models\RegistrantRecord::class, App\Facades\AccountManager::registrantExists($registrantRecord));

        // Alter the payload on non-auth fields
        $alter = [
            'city' => $this->faker->city,
            'address' => $this->faker->address,
            'postal_code' => $this->faker->postcode,
            'status' => RegistrantRecordStatusEnum::ACTIVE
        ];
        $payload = array_merge($this->payload, $alter);

        // Make sure we can register still
        $response = $this->call('POST', '/api/register', $payload);
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->seeJsonStructure( StructureFactory::resource("Registration") );

        // Validate the changes
        $updated = App\Models\RegistrantRecord::where([
            'registrant_id' => $existing->registrant_id,
            'status_id' => RegistrantRecordStatusEnum::ACTIVE
        ])->first();

        $this->assertEquals($updated->city, $payload['city']);
        $this->assertEquals($updated->address, $payload['address']);
        $this->assertEquals($updated->postal_code, $payload['postal_code']);
        \Mail::assertSent(App\Mail\ThankYouEmail::class, 1);
    }

     /**
     * Test updating a registrant record works as expected
     *
     * @group Feature
     * @group RegistrationUpdate
     */
    public function testUpdateRegistrantRecord()
    {
        $registrant = factory(\App\Models\Registrant::class)->create([
            'subscriber' => true,
        ]);
        $registrantRecord = factory(\App\Models\RegistrantRecord::class)->create([
            'registrant_id' => $registrant->id,
        ]);
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create([
            'account_id' => $registrant->id,
        ]);
        $this->actingAs($user);

        $payload = [
            'first_name' => $this->faker->firstName(),
            'last_name' => $registrantRecord->last_name,
            'province_id' => $registrantRecord->province_id,
            'sex' => $registrantRecord->sex,
            'date_of_birth' => $registrantRecord->date_of_birth,
            'health_care' => (string) $registrantRecord->health_care,
            'subscriber' => false,
        ];
        $response = $this->call('POST', '/api/registrant-update', $payload);

        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertEquals($registrant->activeRecord()->first_name, $payload['first_name']);
        $this->assertEquals($registrant->fresh()->isSubscriber(), $payload['subscriber']);
    }

     /**
     * Test updating a registrant record with identical information and an extra unique field will update all similar records
     *
     * @group Feature
     * @group RegistrationUpdate
     */
    public function testUpdateRegistrantRecordWithSimilarData()
    {
        // The "root" record which all subsequent records will base their fields on
        $similarRegistrant = factory(\App\Models\Registrant::class)->create();
        $similarRegistrantRecord = factory(\App\Models\RegistrantRecord::class)->states(['active'])->create([
            'registrant_id' => $similarRegistrant->id,
        ]);

        $similarRegistrantTwo = factory(\App\Models\Registrant::class)->create();
        $similarRegistrantRecordTwo = factory(\App\Models\RegistrantRecord::class)->states(['active'])->create([
            'registrant_id' => $similarRegistrantTwo->id,
            'first_name' => $similarRegistrantRecord->first_name,
            'last_name' => $similarRegistrantRecord->last_name,
            'province_id' => $similarRegistrantRecord->province_id,
            'sex' => $similarRegistrantRecord->sex,
            'date_of_birth' => $similarRegistrantRecord->date_of_birth,
        ]);

        $registrant = factory(\App\Models\Registrant::class)->create();
        $registrantRecord = factory(\App\Models\RegistrantRecord::class)->states(['active'])->create([
            'registrant_id' => $registrant->id,
            'first_name' => $similarRegistrantRecord->first_name,
            'last_name' => $similarRegistrantRecord->last_name,
            'province_id' => $similarRegistrantRecord->province_id,
            'sex' => $similarRegistrantRecord->sex,
            'date_of_birth' => $similarRegistrantRecord->date_of_birth,
        ]);
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create([
            'account_id' => $registrant->id,
        ]);
        $this->actingAs($user);

        // Payload with the same data except:
        //      health_care - unique to all other records
        //      drivers_license - matches with $similarRegistrantRecord
        $payload = [
            'first_name' => $registrantRecord->first_name,
            'last_name' => $registrantRecord->last_name,
            'province_id' => $registrantRecord->province_id,
            'sex' => $registrantRecord->sex,
            'date_of_birth' => $registrantRecord->date_of_birth,
            'health_care' => (string) $registrantRecord->health_care,
            'drivers_license' => (string) $similarRegistrantRecord->drivers_license,
            'subscriber' => false,
        ];
        $response = $this->call('POST', '/api/registrant-update', $payload);

        // Assert that any matching records with atleast one similar id field will merge
        // $similarRegistrantRecordTwo will stay unique, as there is no match with it's health_care, drivers_license, or military_services
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
        $this->assertEquals($registrant->activeRecord()->first_name, $payload['first_name']);
        $this->assertEquals($registrant->fresh()->isSubscriber(), $payload['subscriber']);
        $this->assertEquals($registrantRecord->fresh()->status_id, RegistrantRecordStatusEnum::ARCHIVED);
        $this->assertEquals($similarRegistrantRecord->fresh()->status_id, RegistrantRecordStatusEnum::ARCHIVED);
        $this->assertEquals($similarRegistrantRecordTwo->fresh()->status_id, RegistrantRecordStatusEnum::ACTIVE);
        $this->assertEquals($similarRegistrantRecord->fresh()->registrant_id, $registrantRecord->fresh()->registrant_id);
        $this->assertNotEquals($similarRegistrantRecordTwo->fresh()->registrant_id, $registrantRecord->fresh()->registrant_id);
    }

    /**
     * Test that the registrant-record endpoint works as expected
     *
     * @group Feature
     * @group RegistrantRecord
     */
    public function testGetRegistrantRecord()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $this->actingAs($user);

        $response = $this->call('GET', '/api/registrant-record');

        $this->seeJsonStructure(StructureFactory::resource("RegistrantRecord"));
        $this->assertEquals(ResponseCode::HTTP_OK, $response->status());
    }

    /**
     * Test that the registration-access-key endpoint works as expected
     *
     * @group Feature
     * @group RegistrationAccessKey
     */
    public function testRegistrationAccessKey()
    {
        $response = $this->call('GET', 'api/registration-access-key');
        $this->seeJsonStructure(StructureFactory::resource("RegistrationAccessKey"));
        $this->assertEquals(ResponseCode::HTTP_CREATED, $response->status());
    }


    /**
     * Test that the registrant can delete their account
     *
     * @group Feature
     * @group RegistrationDelete
     */
    public function testRegistrantDelete()
    {

        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $reg  = \App\Models\Registrant::find($user->account_id);
        $rec  = $reg->activeRecord();

        $this->seeInDatabase('users', ['id' => $user->id]);
        $this->seeInDatabase('registrants', ['id' => $reg->id]);
        $this->seeInDatabase('registrant_records', ['id' => $rec->id]);

        $this->actingAs($user);
        $response = $this->call('DELETE', '/api/registrant');
        $this->assertEquals(ResponseCode::HTTP_ACCEPTED, $response->status());

        $this->missingFromDatabase('users', ['id' => $user->id]);
        $this->missingFromDatabase('registrants', ['id' => $reg->id]);
        $this->missingFromDatabase('registrant_records', ['id' => $rec->id]);

    }

    /**
     * Test that the registrant can delete their account
     *
     * @group Feature
     * @group RegistrationDelete
     * @group Events
     */
    public function testRegistrationDeleteEvent()
    {

        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $reg  = \App\Models\Registrant::find($user->account_id);
        $rec  = $reg->activeRecord();

        $this->seeInDatabase('users', ['id' => $user->id]);
        $this->seeInDatabase('registrants', ['id' => $reg->id]);
        $this->seeInDatabase('registrant_records', ['id' => $rec->id]);

        $this->actingAs($user);
        $this->expectsEvents(\App\Events\RegistrantDeleteEvent::class);
        $response = $this->call('DELETE', '/api/registrant');

    }


    /**
     * Test that the subscriber rule validates as expected for registrations
     *
     * @group Feature
     * @group Register
     * @group Validation
     */
    public function testRegistrationSubscriberRule()
    {
        $regData = $this->payload;
        unset($regData['email']);
        $response = $this->call('POST', '/api/register', $regData);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());

        $message = [
            'expected' => trans('validation.required_if', ['attribute' => 'email', 'other' => 'subscriber', 'value' => 1]),
            'actual'   => json_decode($response->content())->errors->email[0]
        ];
        $this->assertEquals($message['expected'], $message['actual']);
    }

    /**
     * Test that the subscriber rule validates as expected for registrations
     *
     * @group Feature
     * @group RegistrationUpdate
     * @group Validation
     */
    public function testRegistrantUpdateSubscriberRule()
    {
        $user = factory(\App\Models\User::class)->states(['registrant', 'enabled'])->create();
        $reg  = Registrant::find($user->account_id);
        $rec  = $reg->activeRecord();

        $this->actingAs($user);

        $payload = [
            'first_name'    => $rec->first_name,
            'last_name'     => $rec->last_name,
            'province_id'   => $rec->province_id,
            'sex'           => $rec->sex,
            'date_of_birth' => $rec->date_of_birth,
            'health_care'   => (string) $rec->health_care,
            'subscriber'    => true,
            'email'         => null
        ];
        $response = $this->call('POST', '/api/registrant-update', $payload);
        $this->assertEquals(ResponseCode::HTTP_UNPROCESSABLE_ENTITY, $response->status());

        $message = [
            'expected' => trans('validation.required_if', ['attribute' => 'email', 'other' => 'subscriber', 'value' => 1]),
            'actual'   => json_decode($response->content())->errors->email[0]
        ];
        $this->assertEquals($message['expected'], $message['actual']);
    }




}
