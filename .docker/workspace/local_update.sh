#!/bin/bash

# Updates the local workspace 
composer install
php artisan passport:keys --force
php artisan migrate:refresh --seed
